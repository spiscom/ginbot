﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ginbot.General
{
    public static class Percentage
    {
        public static int  Progress(double Value,double MaxValue)
        {
            var percentage = Math.Abs((Value * 100) / MaxValue);
            return (int)percentage;
        }
    }
}
