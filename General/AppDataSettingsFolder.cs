﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ginbot.General
{
    public  class AppDataSettingsFolder
    {
        public AppDataSettingsFolder()
        {

            var appDataFolderPath = Environment.ExpandEnvironmentVariables("%AppData%");
            settingFolder = Path.Combine(appDataFolderPath, settingsFolderName);
            if (!Directory.Exists(settingFolder))
                Directory.CreateDirectory(settingFolder);
        }

        private string settingFolder;
        private string settingsFolderName = "Ginbot";

        public string SettingFolder 
        {
            get
            {
                return settingFolder;
            }
                
        }
    }
}
