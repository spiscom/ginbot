﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ginbot.General
{
    /// <summary>
    /// Klasa zawiera stos do przechowywania uruchamianych wątków
    /// </summary>
    public static class RunningThreads
    {

        public static Stack<Thread> Threads = new Stack<Thread>();

        /// <summary>
        /// Metoda zatrzymuje wszyskie wątki ze stosu
        /// </summary>
        public static void StopAllThreads()
        {
            while (Threads.Count>0)
            {
                var thread = Threads.Pop();
                thread.Abort();
            }
        }
    }


}
