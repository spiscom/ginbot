﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ginbot.General
{
    public static class Logger
    {
        /// <summary>
        /// Metoda zapisuje do pliku log
        /// </summary>
        /// <param name="name">Nazwa klasy itp aby zidentyfikować gdzie log powstał</param>
        /// <param name="message">Wiadomośc</param>
        /// <param name="fileName">Nazwa pliku (opcjonalnie domyślnie Log.txt) </param>
        /// <returns></returns>
        public static bool Log(string name,string message,string fileName="Log.txt")
        {
            try
            {
                var msg = $"{DateTime.Now.ToString()} {name} {message}{Environment.NewLine}";
                File.AppendAllText(fileName, msg);
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
    }
}
