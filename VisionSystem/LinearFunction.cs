﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ginbot.VisionSystem
{
    /// <summary>
    /// Klasa reprezentuje funkcję liniową y=ax+b
    /// </summary>
    public class LinearFunction
    {
        public double aFaktor { get; set; }
        public double bFaktor { get; set; }


        #region metody
        /// <summary>
        /// Metoda oblicza współczynniki a,b funkcji liniowej 
        /// przechodzącej przez dwa punkty
        /// </summary>
        /// <param name="pointA">Punkt A</param>
        /// <param name="pointB">Punkt B</param>
        /// <returns>Zwraca tą klasę</returns>
        public LinearFunction SetFactorsFromPoints(Point2D pointA, Point2D pointB)
        {
            aFaktor =(double) (pointA.Y - pointB.Y) / (pointA.X - pointB.X);
            bFaktor = SetBFactorFromPoint(pointA).bFaktor;
            return this;
        }
        /// <summary>
        /// Metoda oblicza punkt przecięcia dwóch prostych
        /// </summary>
        /// <param name="linearFunction"></param>
        /// <returns></returns>
        public Point2D GetCrossPoint(LinearFunction linearFunction)
        {
            double x = (linearFunction.bFaktor - bFaktor) / (aFaktor - linearFunction.aFaktor);
            double y = aFaktor * x + bFaktor;
            return new Point2D(x, y);
        }
        #endregion
        /// <summary>
        /// Metoda oblicza i ustawia współczynnik b funkcji używając już obliczonego współczynnika a
        /// i punktu przez który przechodzi prosta
        /// </summary>
        /// <param name="pointA"></param>
        /// <returns>Tą klasę</returns>
        public LinearFunction SetBFactorFromPoint(Point2D pointA)
        {
            bFaktor = pointA.Y - (aFaktor * pointA.X);
            return this;
        }

        public bool IsPointOnStraight(Point2D point)
        {
            double diffrence;
            //próg dokładności 
            double threshold = 0.5;
          
            var y = aFaktor * point.X + bFaktor;
            diffrence = y - point.Y;

            return (Math.Abs(diffrence) <= threshold);
        }
        /// <summary>
        /// Metoda oblicza kąt pomiędzy prostymi
        /// </summary>
        /// <param name="linearFunction">Instancja LinearFunction (funkcji liniowej) </param>
        /// <returns>Kąt w radianach</returns>
        public double GetAngleBetween (LinearFunction linearFunction)
        {
            double n = aFaktor - linearFunction.aFaktor;
            double m = 1 + (aFaktor * linearFunction.aFaktor);
            double angle = Math.Atan((n / m));
            return angle;
        }
        /// <summary>
        /// Metoda wyznacza współczynnik a funkcji, która tworzy wyznaczony kąt
        /// z bieżącą funkcją
        /// </summary>
        /// <param name="angle">Kąt w radianach</param>
        /// <returns>Współczynnik a prostej</returns>
        public double GetAFaktorWithAngle(double angle)
        {
            var tan = Math.Tan(angle);
            return (aFaktor-tan)/((tan*aFaktor)+1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Zwraca równanie prostej</returns>
        public override string ToString()
        {
            return $"{aFaktor}X+({bFaktor})";
        }

    }


}
