﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ginbot.VisionSystem
{
    public partial class CalibrationForm : Form
    {
        Calibration calibration;
        Bitmap bitmap;
        CalibrationSettingsRepo settingsRepo;
        public CalibrationForm(Calibration _calibration,Bitmap _bitmap)
        {
            InitializeComponent();
            calibration = _calibration;
            bitmap = _bitmap;

            //usunięcie przycisów z NumericDownUp
            num_p1x.Controls[0].Visible = num_p1y.Controls[0].Visible = false;
            num_p2x.Controls[0].Visible = num_p2y.Controls[0].Visible = false;
            num_p3x.Controls[0].Visible = num_p3y.Controls[0].Visible = false;

            //numerowanie punktów kalibracyjnych
            int i = 1;
            foreach (var point in calibration.ImageCalibrationPoints)
            {
                ObjectDrawer.DrawText(bitmap, i.ToString(), 25, point.ToPoint()) ;
                i++;
            }
            settingsRepo = new CalibrationSettingsRepo(calibration);
            settingsRepo.Open();
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            Point2D p1 = new Point2D((double)num_p1x.Value, (double)num_p1y.Value);
            Point2D p2 = new Point2D((double)num_p2x.Value, (double)num_p2y.Value);
            Point2D p3 = new Point2D((double)num_p3x.Value, (double)num_p3y.Value);
            
            try
            {
                calibration.SetCalibrationPoints(p1, p2, p3);
                settingsRepo.Save();
                calibration.ImageSize = new Size(bitmap.Width, bitmap.Height);
                this.Close();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message,"Neprawidłowe punkty kalibracji",MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CalibrationForm_Load(object sender, EventArgs e)
        {
            if (calibration.CalibrationPoints.Count==3)
            {
                num_p1x.Value = (decimal) calibration.CalibrationPoints[0].X;
                num_p1y.Value = (decimal) calibration.CalibrationPoints[0].Y;

                num_p2x.Value = (decimal)calibration.CalibrationPoints[1].X;
                num_p2y.Value = (decimal)calibration.CalibrationPoints[1].Y;

                num_p3x.Value = (decimal)calibration.CalibrationPoints[2].X;
                num_p3y.Value = (decimal)calibration.CalibrationPoints[2].Y;
            }
            pbFrame.BackgroundImage = bitmap;
        }
    }
}
