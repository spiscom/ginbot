﻿using ginbot.RoboArm;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ginbot.VisionSystem
{
    public static class Geometry
    {
        /// <summary>
        /// Metoda oblicza kat odchylenia od poziomu prostej przechodzącej przez 
        /// dwa punkty parametrów
        /// </summary>
        /// <param name="FirstPoint">Pierwszy punkt</param>
        /// <param name="SecondPoint">Drugi punkt</param>
        /// <returns>Kąt odchylenia w stopniach</returns>
        public static double GetDeviation(Point FirstPoint, Point SecondPoint)
        {
            double a = Math.Abs((double)FirstPoint.Y - SecondPoint.Y) / Math.Abs((double)FirstPoint.X - SecondPoint.X);
            double rotation = Math.Atan(a);
            return RadianToDegree(rotation);
        }

        /// <summary>
        /// Oblicza odległość pomiędzy dwoma punktami obrazu
        /// </summary>
        /// <param name="p">Bieżący punkt</param>
        /// <param name="point">Punkt do którego mierzymy odległość</param>
        /// <returns>Odległość pomiędzy punktami</returns>
        public static double GetDistance(this Point p, Point point)
        {
            Point p1 = p;
            Point p2 = point;
            double distance = Math.Sqrt(Math.Pow((p2.X - p1.X), 2) + Math.Pow((p2.Y - p1.Y), 2));
            return distance;
        }

        /// <summary>
        /// Zamienia radiany na stopnie
        /// </summary>
        /// <param name="radian">Kąt w radianach</param>
        /// <returns>Kąt w stopniach</returns>
        public static double RadianToDegree(double radian)
        {
            return radian * (180 / Math.PI);
        }


        /// <summary>
        /// Zamienia stopnie na radiany
        /// </summary>
        /// <param name="angle">Kąt w stopniach</param>
        /// <returns>Kąt w radianach</returns>
        public static double DegreeToRadian(double angle)
        {
            return Math.PI * angle / 180.0;
        }
    }
}
