﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace ginbot.VisionSystem
{
    public class VisionSystemNewFrameEventArgs : EventArgs
    {
        public Bitmap NewFrame;
        public List<DetectedObject> DetectedObjects;
    }
}