﻿using AForge;
using AForge.Imaging;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ginbot.VisionSystem
{
    public static class Helper
    {
        /// <summary>
        /// Metoda konwertuje AForge.Point na System.Drawing.Point
        /// </summary>
        /// <param name="AforgePoint">Punkt AForge.Point </param>
        /// <returns></returns>
        public static System.Drawing.Point ToDrawingPoints(this AForge.Point AforgePoint)
        {
            var point = new System.Drawing.Point((int)AforgePoint.X, (int)AforgePoint.Y);
            return point;
        }

        public static List<System.Drawing.Point> ToDrowingPointList(this List<IntPoint> intPoints)
        {
            var pointList = new List<System.Drawing.Point>();
            foreach (var intPoint in intPoints)
            {
                pointList.Add(new System.Drawing.Point(intPoint.X, intPoint.Y));
            }
            return pointList;
        }

        // Konwersja AForge.Point na System.Drawing.Point array
        public static System.Drawing.Point[] ToPointsArray(this List<IntPoint> points)
        {
            System.Drawing.Point[] array = new System.Drawing.Point[points.Count];

            for (int i = 0, n = points.Count; i < n; i++)
            {
                array[i] = new System.Drawing.Point(points[i].X, points[i].Y);
            }

            return array;
        }

        public static RGB ToAforgeRGB(this Color color)
        {
            AForge.Imaging.RGB rgb = new RGB();
            rgb.Alpha = color.A;
            rgb.Red = color.R;
            rgb.Green = color.G;
            rgb.Blue = color.B;
            return rgb;
        }

        public static Point2D ToPoint2D(this System.Drawing.Point point)
        {
            return new Point2D(point.X, point.Y);
        }

        public static System.Drawing.Point ToPoint(this Point2D point2D)
        {
            return new System.Drawing.Point((int)point2D.X, (int)point2D.Y);
        }
    }
}
