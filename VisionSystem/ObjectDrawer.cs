﻿using ginbot.RoboArm;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ginbot.VisionSystem
{
    public static class ObjectDrawer
    {
        /// <summary>
        /// Metoda obrysowuje wykryte obiekty
        /// </summary>
        /// <param name="bitmap">Obraz o wykonania obrysu</param>
        /// <param name="detectedObjects">Lista wykrytych obiektów DetectedOject</param>
        public static void DrawObjectFrame(Bitmap bitmap,List<DetectedObject> detectedObjects)
        {
            //BitmapData bitmapData = bitmap.LockBits(
            //    new Rectangle(0, 0, bitmap.Width, bitmap.Height),
            //    ImageLockMode.ReadWrite, bitmap.PixelFormat);
            if (detectedObjects==null)
            {
                return;
            }
            Graphics g = Graphics.FromImage(bitmap);
            foreach (var detectedObject in detectedObjects)
            {
                int penWidth = 5;
                var name = detectedObject.Name;
                var center = detectedObject.Center;
                Pen pen = new Pen(Color.YellowGreen, penWidth);
                switch (name)
                {
                    case "Koło":
                        {
                            var radius = detectedObject.Radius;
                            g.DrawEllipse(pen,
                            (float)(center.X - radius), (float)(center.Y - radius),
                            (float)(radius * 2), (float)(radius * 2));
                            break;
                        }
                    default:
                        {
                            var corners = detectedObject.Corners;
                            g.DrawPolygon(pen, corners.ToArray());
                            break;
                        }
                        
                }
                
            }
            g.Dispose();
            //bitmap.UnlockBits(bitmapData);
        }

        public static void DrawText(Bitmap bitmap,String text,int fontSize, Point point)
        {
            Graphics g = Graphics.FromImage(bitmap);
            SolidBrush redBrush = new SolidBrush(Color.Red);
            g.DrawString(text, new Font("Arial", fontSize), redBrush,point);
            g.Dispose();
            redBrush.Dispose();
        }

        public static void DrawArm(Bitmap bitmap,Calibration calibration,  Arm arm)
        {
            var p1 = calibration.RealPointToImage( new Point((int)arm.Parent.Position3D.X, (int)arm.Parent.Position3D.Y));
            var p2 = calibration.RealPointToImage(new Point((int)arm.Position3D.X, (int)arm.Position3D.Y));
            Graphics g = Graphics.FromImage(bitmap);
            var penWidth = 5;
            Pen pen = new Pen(Color.YellowGreen, penWidth);
            g.DrawLine(pen, p1, p2);

            g.Dispose();
            pen.Dispose();
        }
    }
}
