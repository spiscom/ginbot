﻿using System;
using System.Drawing;

namespace ginbot.VisionSystem
{
    /// <summary>
    /// Klasa reprezentuje punkt X,Y o wartościach double
    /// </summary>
    public class Point2D
    {
        public Point2D()
        {

        }
        public Point2D(double X,double Y)
        {
            this.X = X;
            this.Y = Y;
        }
        public double X { get; set; }
        public double Y { get; set; }

        /// <summary>
        /// Metoda oblicza odległość od punktów 2D;
        /// </summary>
        /// <param name="X">Współrzędna X</param>
        /// <param name="Y">Współrzędna Y</param>
        /// <returns>Odległość o punktu.</returns>
        public double GetDistance(double X, double Y)
        {
            return Math.Sqrt(Math.Pow(X - this.X, 2) + Math.Pow(Y - this.Y, 2));
        }

        /// <summary>
        /// Metoda oblicza odległość od punktów 2D;
        /// </summary>
        /// <param name="point2D">Punkt w przestzreni 2D</param>
        /// <returns>Odległość o punktu.</returns>
        public double GetDistance(Point2D point2D)
        {
            return GetDistance(point2D.X, point2D.Y);
        }


    }
}