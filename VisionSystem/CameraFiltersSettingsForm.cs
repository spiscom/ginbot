﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ginbot.VisionSystem
{
    public partial class CameraFiltersSettingsForm : Form
    {
        VisionSystem visionSystem;
        ImagingFilters filters;
        bool Loaded = false;
        public CameraFiltersSettingsForm(VisionSystem _visionSystem)
        {
            InitializeComponent();
            visionSystem = _visionSystem;
            filters = visionSystem.filters;
            visionSystem.ShowFilteredFrame = true;
        }

        private void numericLevelsLinearChanged(object sender, EventArgs e)
        {
            if (!Loaded) return;
            filters.LevelsLinearfilter.InBlue = new AForge.IntRange((int)nudLinearLevelsBlueFrom.Value, (int)nudLinearLevelsBlueTo.Value);
            filters.LevelsLinearfilter.InGreen = new AForge.IntRange((int)nudLinearLevelsGreenFrom.Value, (int)nudLinearLevelsGreenTo.Value);
            filters.LevelsLinearfilter.InRed = new AForge.IntRange((int)nudLinearLevelsRedFrom.Value, (int)nudLinearLevelsRedTo.Value);
            filters.LevelsLinearfilter.InGray = new AForge.IntRange((int)nudLinearLevelsGrayFrom.Value, (int)nudLinearLevelsGrayTo.Value);
        }

        private void CameraFiltersSettingsForm_Load(object sender, EventArgs e)
        {
            //ustawienie warości LevelsLinear
            nudLinearLevelsBlueFrom.Value = filters.LevelsLinearfilter.InBlue.Min;
            nudLinearLevelsBlueTo.Value = filters.LevelsLinearfilter.InBlue.Max;
            nudLinearLevelsGreenFrom.Value = filters.LevelsLinearfilter.InGreen.Min;
            nudLinearLevelsGreenTo.Value =  filters.LevelsLinearfilter.InGreen.Max;
            nudLinearLevelsRedFrom.Value = filters.LevelsLinearfilter.InRed.Min;
            nudLinearLevelsRedTo.Value =  filters.LevelsLinearfilter.InRed.Max;
            nudLinearLevelsGrayFrom.Value = filters.LevelsLinearfilter.InGray.Min;
            nudLinearLevelsGrayTo.Value = filters.LevelsLinearfilter.InGray.Max;

            //ustawienie warości ColoringFilter
            nudColorFilterRedMin.Value = filters.colorFilter.Red.Min;
            nudColorFilterRedMax.Value = filters.colorFilter.Red.Max;
            nudColorFilterBlueMin.Value = filters.colorFilter.Blue.Min;
            nudColorFilterBlueMax.Value = filters.colorFilter.Blue.Max;
            nudColorFilterGreenMin.Value = filters.colorFilter.Green.Min;
            nudColorFilterGreenMax.Value = filters.colorFilter.Green.Max;

            nudMedianSize.Value=filters.MedianFilter.Size;

            nudContrast.Value=filters.ContrastFilter.Factor;

            nudBrightness.Value=filters.BrightnessCorrectionFilter.AdjustValue;

            chbLinearLevelsEnable.Checked = filters.LevelsLinearfilterEnabled ;
            chbColoringFilterEnabled.Checked = filters.ColorFilterEnabled ;
            chBFillOutsideRange.Checked = filters.colorFilter.FillOutsideRange ;
            chbBrightnessEnabled.Checked = filters.BrightnessCorrectionFilterEnabled;
            chbMedianEnabled.Checked = filters.MedianFilterEnabled;
            chbContrastFilterEnabled.Checked = filters.ContrastFilterEnabled;            


            Loaded = true;
        }

        private void btnOpenCameraProperty_Click(object sender, EventArgs e)
        {
            //visionSystem.OpenPropertyWindow();
            IntPtr window = default(IntPtr);
            visionSystem.VideoDevice.DisplayPropertyPage(window);
        }

        private void filterCheckedChanged(object sender, EventArgs e)
        {
            if (!Loaded) return;
            filters.LevelsLinearfilterEnabled = chbLinearLevelsEnable.Checked;
            filters.ColorFilterEnabled = chbColoringFilterEnabled.Checked;
            filters.colorFilter.FillOutsideRange = chBFillOutsideRange.Checked;
            filters.BrightnessCorrectionFilterEnabled = chbBrightnessEnabled.Checked;
            filters.MedianFilterEnabled = chbMedianEnabled.Checked;
            filters.ContrastFilterEnabled = chbContrastFilterEnabled.Checked;
        }

        private void linkLabelClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var linkLabel = sender as LinkLabel;
            
            linkLabel.LinkVisited = true;
            System.Diagnostics.Process.Start(linkLabel.Tag.ToString());
        }

        private void numericColorFilterChanged(object sender, EventArgs e)
        {
            if (!Loaded) return;
            filters.colorFilter.Red = new AForge.IntRange((int)nudColorFilterRedMin.Value, (int)nudColorFilterRedMax.Value);
            filters.colorFilter.Green = new AForge.IntRange((int)nudColorFilterGreenMin.Value, (int)nudColorFilterGreenMax.Value);
            filters.colorFilter.Blue = new AForge.IntRange((int)nudColorFilterBlueMin.Value, (int)nudColorFilterBlueMax.Value);

        }

        private void nudBrightness_ValueChanged(object sender, EventArgs e)
        {
            if (!Loaded) return;
            filters.BrightnessCorrectionFilter.AdjustValue = (int) nudBrightness.Value;
             
        }

        private void CameraFiltersSettingsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            visionSystem.ShowFilteredFrame = false;
        }

        private void nudMedianSize_ValueChanged(object sender, EventArgs e)
        {
            if (!Loaded) return;
            filters.MedianFilter.Size = (int)nudMedianSize.Value;
        }

        private void nudContrast_ValueChanged(object sender, EventArgs e)
        {
            if (!Loaded) return;
            filters.ContrastFilter.Factor = (int)nudContrast.Value;
        }
    }
}
