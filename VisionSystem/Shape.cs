﻿namespace ginbot.VisionSystem
{
    public class Shape
    {
        public static string Circle { get; internal set; } = "Koło";
        public static string Unknown { get; internal set; } = "Nieokreślony";
        public static string Square { get; internal set; } = "Kwadrat";
        public static string Rectangle { get; internal set; } = "Prostokąt";
        public static string Triangle { get; internal set; } = "Trójkąt";
        public static string Polygon { get; internal set; } = "Wielokąt";
        public static string Other { get; internal set; } = "Inny";
    }
}