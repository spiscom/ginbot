﻿using AForge;
using AForge.Imaging.Filters;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ginbot.VisionSystem
{
    public class ImagingFilters
    {
        public ImagingFilters()
        {
            //wartości początkowe filtrów
            LevelsLinearfilter.InRed = new IntRange(125, 230);
            LevelsLinearfilter.InGreen = new IntRange(125, 210);
            LevelsLinearfilter.InBlue = new IntRange(125, 210);
            LevelsLinearfilter.InGray = new IntRange(125, 210);

            colorFilter.Red = new IntRange(0, 100);
            colorFilter.Green = new IntRange(0, 100);
            colorFilter.Blue = new IntRange(0, 100);
            colorFilter.FillOutsideRange = false;
        }
        //Filtry
        public LevelsLinear LevelsLinearfilter = new LevelsLinear();
        public bool LevelsLinearfilterEnabled { get; set; } = true;

        public ColorFiltering colorFilter = new ColorFiltering();
        public bool ColorFilterEnabled { get; set; } = false;
        public BrightnessCorrection BrightnessCorrectionFilter = new BrightnessCorrection(-50);
        public bool BrightnessCorrectionFilterEnabled { get; set; } = false;
        public Median MedianFilter = new Median();
        public bool MedianFilterEnabled { get; set; } = false;

        public ContrastCorrection ContrastFilter = new ContrastCorrection(15);
        public bool ContrastFilterEnabled { get; set; } = false;
        public void ApplyFilters(Bitmap bitmap)
        {           
            if (BrightnessCorrectionFilterEnabled)
                BrightnessCorrectionFilter.ApplyInPlace(bitmap);
            if (ContrastFilterEnabled)
                ContrastFilter.ApplyInPlace(bitmap);
            if (LevelsLinearfilterEnabled)
                LevelsLinearfilter.ApplyInPlace(bitmap);
            if (ColorFilterEnabled)
                colorFilter.ApplyInPlace(bitmap);
            if (MedianFilterEnabled)
                MedianFilter.ApplyInPlace(bitmap);         
        }
    }
}
