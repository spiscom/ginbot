﻿namespace ginbot.VisionSystem
{
    public class CalibrationProgressChangedEventArgs
    {
        public int ProgressPercentage { get; set; }
    }
}