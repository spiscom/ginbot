﻿using AForge;
using AForge.Imaging;
using AForge.Imaging.Filters;
using AForge.Math.Geometry;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ginbot.VisionSystem
{
    public class ObjectDetector
    {
        BlobCounter blobCounter;
        public ObjectDetector()
        {
            blobCounter = new BlobCounter();
            blobCounter.FilterBlobs = true;
            blobCounter.MinHeight = 40;
            blobCounter.MinWidth = 40;
            ShapeOutFilter = new List<string>();
            
        }

        public List<string> ShapeOutFilter { get; set; }

        public int MinHeight
        {
            get { return blobCounter.MinHeight; }
            set { blobCounter.MinHeight = value; }
        }


        public int MinWidth
        {
            get { return blobCounter.MinWidth; }
            set { blobCounter.MinWidth = value; }
        }

        public int MaxWidth
        {
            get { return blobCounter.MaxWidth; }
            set { blobCounter.MaxWidth = value; }
        }

        public int MaxHeight
        {
            get { return blobCounter.MaxHeight; }
            set { blobCounter.MaxHeight = value; }
        }


        private Blob[] GetBlobs(Bitmap bitmap )
        {
                       


            blobCounter.BackgroundThreshold = Color.FromArgb(40, 40, 40);
            blobCounter.CoupledSizeFiltering = true;
            //BlobFilter blobFilter = new BlobFilter();
            //blobCounter.BlobsFilter = blobFilter;

            blobCounter.ProcessImage(bitmap);
            Blob[] blobs = blobCounter.GetObjectsInformation();
            return blobs;
        }

        public List<DetectedObject> GetDetectedObjects(Bitmap bitmap)
        {
            
            SimpleShapeChecker shapeChecker = new SimpleShapeChecker();
            var blobs = GetBlobs(bitmap);
            //Size imageSize = new Size(bitmap.Width, bitmap.Height );
            List<DetectedObject> detectedObjects = new List<DetectedObject>();
            foreach (var blob in blobs)
            {
                List<IntPoint> edgePoints = blobCounter.GetBlobsEdgePoints(blob);
                AForge.Point center;
                float radius;
                List<IntPoint> corners = new List<IntPoint>();
                string name=String.Empty;
                if (shapeChecker.IsCircle(edgePoints, out center, out radius))
                {
                    name = Shape.Circle;
                    Console.WriteLine("circle");
                }
                else
                {
                    if (shapeChecker.IsConvexPolygon(edgePoints, out corners))
                    {
                        // sprawdzenie typu obiektu
                        PolygonSubType subType = shapeChecker.CheckPolygonSubType(corners);

                        if (subType == PolygonSubType.Unknown)
                        {                           
                            name = Shape.Unknown;
                                                       
                        }
                        else if (subType == PolygonSubType.Square)
                        {                           
                            name = Shape.Square;
                        }
                        else if (subType == PolygonSubType.Rectangle)
                        {                            
                            name = Shape.Rectangle;
                        }
                        else if (corners.Count == 3)
                        {                          
                            name = Shape.Triangle;
                        }
                        else if (corners.Count > 4)
                        {                           
                            name = Shape.Polygon;
                            
                        }
                    }
                    else
                    {
                        
                        name = Shape.Other; 
                    }

                    
                }
                if (ShapeOutFilter.Contains(name)) continue;
                if (blob.Area < (MinWidth * MinHeight)) continue;
                if (blob.Area > (MaxWidth * MaxHeight)) continue;
                DetectedObject detectedObject = new DetectedObject()
                {
                    Id = blob.ID,
                    Name = name,
                    Center = center.ToDrawingPoints(),
                    Corners = corners.ToDrowingPointList(),
                    Radius = radius,
                    ColorMean = blob.ColorMean,
                    //ImageSize = imageSize
                };

                detectedObjects.Add(detectedObject);
        }

            return detectedObjects;
        }


    }
}
