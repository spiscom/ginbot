﻿namespace ginbot.VisionSystem
{
    partial class CalibrationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CalibrationForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_save = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.num_p3y = new System.Windows.Forms.NumericUpDown();
            this.num_p3x = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.num_p2y = new System.Windows.Forms.NumericUpDown();
            this.num_p2x = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.num_p1y = new System.Windows.Forms.NumericUpDown();
            this.num_p1x = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pbFrame = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_p3y)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_p3x)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_p2y)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_p2x)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_p1y)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_p1x)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFrame)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_save);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(974, 568);
            this.panel1.TabIndex = 0;
            // 
            // btn_save
            // 
            this.btn_save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_save.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_save.Location = new System.Drawing.Point(845, 493);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(85, 30);
            this.btn_save.TabIndex = 6;
            this.btn_save.Text = "Zapisz";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.num_p3y);
            this.groupBox3.Controls.Add(this.num_p3x);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox3.Location = new System.Drawing.Point(786, 317);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(176, 132);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Punkt 3";
            // 
            // num_p3y
            // 
            this.num_p3y.DecimalPlaces = 2;
            this.num_p3y.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.num_p3y.Location = new System.Drawing.Point(59, 82);
            this.num_p3y.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.num_p3y.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.num_p3y.Name = "num_p3y";
            this.num_p3y.Size = new System.Drawing.Size(111, 26);
            this.num_p3y.TabIndex = 7;
            this.num_p3y.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // num_p3x
            // 
            this.num_p3x.DecimalPlaces = 2;
            this.num_p3x.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.num_p3x.Location = new System.Drawing.Point(59, 50);
            this.num_p3x.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.num_p3x.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.num_p3x.Name = "num_p3x";
            this.num_p3x.Size = new System.Drawing.Size(111, 26);
            this.num_p3x.TabIndex = 6;
            this.num_p3x.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(33, 90);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(20, 20);
            this.label7.TabIndex = 4;
            this.label7.Text = "Y";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(33, 56);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(20, 20);
            this.label8.TabIndex = 3;
            this.label8.Text = "X";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(24, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(111, 20);
            this.label9.TabIndex = 0;
            this.label9.Text = "Współrzędne :";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.num_p2y);
            this.groupBox2.Controls.Add(this.num_p2x);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox2.Location = new System.Drawing.Point(786, 163);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(176, 132);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Punkt 2";
            // 
            // num_p2y
            // 
            this.num_p2y.DecimalPlaces = 2;
            this.num_p2y.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.num_p2y.Location = new System.Drawing.Point(59, 88);
            this.num_p2y.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.num_p2y.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.num_p2y.Name = "num_p2y";
            this.num_p2y.Size = new System.Drawing.Size(111, 26);
            this.num_p2y.TabIndex = 7;
            this.num_p2y.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // num_p2x
            // 
            this.num_p2x.DecimalPlaces = 2;
            this.num_p2x.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.num_p2x.Location = new System.Drawing.Point(59, 56);
            this.num_p2x.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.num_p2x.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.num_p2x.Name = "num_p2x";
            this.num_p2x.Size = new System.Drawing.Size(111, 26);
            this.num_p2x.TabIndex = 6;
            this.num_p2x.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(33, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "Y";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(33, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 20);
            this.label5.TabIndex = 3;
            this.label5.Text = "X";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 20);
            this.label6.TabIndex = 0;
            this.label6.Text = "Współrzędne :";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.num_p1y);
            this.groupBox1.Controls.Add(this.num_p1x);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.Location = new System.Drawing.Point(786, 16);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(176, 132);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Punkt 1";
            // 
            // num_p1y
            // 
            this.num_p1y.DecimalPlaces = 2;
            this.num_p1y.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.num_p1y.Location = new System.Drawing.Point(59, 86);
            this.num_p1y.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.num_p1y.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.num_p1y.Name = "num_p1y";
            this.num_p1y.Size = new System.Drawing.Size(111, 26);
            this.num_p1y.TabIndex = 5;
            this.num_p1y.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // num_p1x
            // 
            this.num_p1x.DecimalPlaces = 2;
            this.num_p1x.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.num_p1x.Location = new System.Drawing.Point(59, 54);
            this.num_p1x.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.num_p1x.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.num_p1x.Name = "num_p1x";
            this.num_p1x.Size = new System.Drawing.Size(111, 26);
            this.num_p1x.TabIndex = 1;
            this.num_p1x.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Y";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "X";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Współrzędne :";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.panel2.Controls.Add(this.pbFrame);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(780, 568);
            this.panel2.TabIndex = 0;
            // 
            // pbFrame
            // 
            this.pbFrame.BackColor = System.Drawing.Color.White;
            this.pbFrame.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbFrame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbFrame.Location = new System.Drawing.Point(0, 0);
            this.pbFrame.Name = "pbFrame";
            this.pbFrame.Size = new System.Drawing.Size(780, 568);
            this.pbFrame.TabIndex = 0;
            this.pbFrame.TabStop = false;
            // 
            // CalibrationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(974, 568);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CalibrationForm";
            this.Text = "Kalibracja systemu wizyjnego";
            this.Load += new System.EventHandler(this.CalibrationForm_Load);
            this.panel1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_p3y)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_p3x)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_p2y)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_p2x)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_p1y)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_p1x)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbFrame)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pbFrame;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.NumericUpDown num_p1x;
        private System.Windows.Forms.NumericUpDown num_p3y;
        private System.Windows.Forms.NumericUpDown num_p3x;
        private System.Windows.Forms.NumericUpDown num_p2y;
        private System.Windows.Forms.NumericUpDown num_p2x;
        private System.Windows.Forms.NumericUpDown num_p1y;
    }
}