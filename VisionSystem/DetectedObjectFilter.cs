﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ginbot.VisionSystem
{
    public class DetectedObjectFilter
    {

        public string[] OutFilter { get; set; }
        public List<DetectedObject> GetFilteredObjects(List<DetectedObject> detectedObjects)
        {
            
            List<DetectedObject> newList = new List<DetectedObject>();
            if (OutFilter == null) return newList;
            foreach (var detectedObject in detectedObjects)
            {
                if (!OutFilter.Contains(detectedObject.Name))
                {
                    newList.Add(detectedObject);
                }
            }
            return newList;
        }
    }
}
