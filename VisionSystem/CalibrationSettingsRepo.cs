﻿using ginbot.General;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ginbot.VisionSystem
{
    public  class CalibrationSettingsRepo
    {

        public CalibrationSettingsRepo(Calibration _calibration)
        {
            calibration = _calibration;
            settingPath = Path.Combine(settingsFolder.SettingFolder, settingsFileName);
        }

        static AppDataSettingsFolder settingsFolder = new AppDataSettingsFolder();
        private string settingsFileName = "CalibrationSettings.txt";
        private Calibration calibration;
        private string settingPath = "";

        /// <summary>
        /// Zapisuje współrzędne rzeczywiste punktów kalibracyjnych
        /// </summary>
        public void Save()
        {
            var filetext = "";
            foreach (var point in calibration.CalibrationPoints)
            {
                filetext += $"{point.X};{point.Y}{Environment.NewLine}";
            }

            try
            { 
            File.WriteAllText(settingPath, filetext);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        /// <summary>
        /// Otwiera z pliku współrzędne rzeczywiste punktów kalibracyjnych
        /// </summary>
        public void Open()
        {
            string[] pointLines;
            if (!File.Exists(settingPath)) return;
            try
            {
                pointLines = File.ReadAllLines(settingPath);
            }
            catch (Exception)
            {

                throw;
            }
            calibration.CalibrationPoints.Clear();
            foreach (var line in pointLines)
            {
                var stringPoint = line.Split(';');
                var X = Convert.ToDouble(stringPoint[0]);
                var Y = Convert.ToDouble(stringPoint[1]);
                calibration.CalibrationPoints.Add(new Point2D(X, Y));
            }


        }
    }
}
