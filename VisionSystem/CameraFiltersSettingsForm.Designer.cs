﻿namespace ginbot.VisionSystem
{
    partial class CameraFiltersSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnOpenCameraProperty = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.linkLabel4 = new System.Windows.Forms.LinkLabel();
            this.label9 = new System.Windows.Forms.Label();
            this.nudContrast = new System.Windows.Forms.NumericUpDown();
            this.chbContrastFilterEnabled = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.label8 = new System.Windows.Forms.Label();
            this.nudMedianSize = new System.Windows.Forms.NumericUpDown();
            this.chbMedianEnabled = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.label10 = new System.Windows.Forms.Label();
            this.nudBrightness = new System.Windows.Forms.NumericUpDown();
            this.chbBrightnessEnabled = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chBFillOutsideRange = new System.Windows.Forms.CheckBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.nudColorFilterBlueMax = new System.Windows.Forms.NumericUpDown();
            this.nudColorFilterBlueMin = new System.Windows.Forms.NumericUpDown();
            this.nudColorFilterGreenMax = new System.Windows.Forms.NumericUpDown();
            this.nudColorFilterGreenMin = new System.Windows.Forms.NumericUpDown();
            this.nudColorFilterRedMax = new System.Windows.Forms.NumericUpDown();
            this.nudColorFilterRedMin = new System.Windows.Forms.NumericUpDown();
            this.chbColoringFilterEnabled = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.nudLinearLevelsGrayTo = new System.Windows.Forms.NumericUpDown();
            this.nudLinearLevelsGrayFrom = new System.Windows.Forms.NumericUpDown();
            this.linkLabelLevelsLinear = new System.Windows.Forms.LinkLabel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.nudLinearLevelsBlueTo = new System.Windows.Forms.NumericUpDown();
            this.nudLinearLevelsBlueFrom = new System.Windows.Forms.NumericUpDown();
            this.nudLinearLevelsGreenTo = new System.Windows.Forms.NumericUpDown();
            this.nudLinearLevelsGreenFrom = new System.Windows.Forms.NumericUpDown();
            this.nudLinearLevelsRedTo = new System.Windows.Forms.NumericUpDown();
            this.nudLinearLevelsRedFrom = new System.Windows.Forms.NumericUpDown();
            this.chbLinearLevelsEnable = new System.Windows.Forms.CheckBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudContrast)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMedianSize)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBrightness)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudColorFilterBlueMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudColorFilterBlueMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudColorFilterGreenMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudColorFilterGreenMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudColorFilterRedMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudColorFilterRedMin)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudLinearLevelsGrayTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLinearLevelsGrayFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLinearLevelsBlueTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLinearLevelsBlueFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLinearLevelsGreenTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLinearLevelsGreenFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLinearLevelsRedTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLinearLevelsRedFrom)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(415, 450);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnOpenCameraProperty);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(407, 421);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Ustawienia kamery";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnOpenCameraProperty
            // 
            this.btnOpenCameraProperty.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnOpenCameraProperty.Location = new System.Drawing.Point(114, 67);
            this.btnOpenCameraProperty.Name = "btnOpenCameraProperty";
            this.btnOpenCameraProperty.Size = new System.Drawing.Size(155, 31);
            this.btnOpenCameraProperty.TabIndex = 0;
            this.btnOpenCameraProperty.Text = "Ustawienia kamery";
            this.btnOpenCameraProperty.UseVisualStyleBackColor = true;
            this.btnOpenCameraProperty.Click += new System.EventHandler(this.btnOpenCameraProperty_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(407, 421);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Filtry";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.AutoScrollMinSize = new System.Drawing.Size(0, 700);
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(401, 415);
            this.panel1.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.groupBox5, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.groupBox3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupBox2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(378, 797);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.linkLabel4);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.nudContrast);
            this.groupBox5.Controls.Add(this.chbContrastFilterEnabled);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(372, 114);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "ContrastCorrection";
            // 
            // linkLabel4
            // 
            this.linkLabel4.AutoSize = true;
            this.linkLabel4.Location = new System.Drawing.Point(146, 21);
            this.linkLabel4.Name = "linkLabel4";
            this.linkLabel4.Size = new System.Drawing.Size(114, 16);
            this.linkLabel4.TabIndex = 10;
            this.linkLabel4.TabStop = true;
            this.linkLabel4.Tag = "http://www.aforgenet.com/framework/docs/html/63fb4582-5b21-3655-b306-c1bf5d3f8999" +
    ".htm";
            this.linkLabel4.Text = "Informacje o filtrze";
            this.linkLabel4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelClicked);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(22, 59);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 16);
            this.label9.TabIndex = 7;
            this.label9.Text = "Contrast";
            // 
            // nudContrast
            // 
            this.nudContrast.Location = new System.Drawing.Point(114, 57);
            this.nudContrast.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.nudContrast.Minimum = new decimal(new int[] {
            127,
            0,
            0,
            -2147483648});
            this.nudContrast.Name = "nudContrast";
            this.nudContrast.Size = new System.Drawing.Size(87, 22);
            this.nudContrast.TabIndex = 1;
            this.nudContrast.ValueChanged += new System.EventHandler(this.nudContrast_ValueChanged);
            // 
            // chbContrastFilterEnabled
            // 
            this.chbContrastFilterEnabled.AutoSize = true;
            this.chbContrastFilterEnabled.Location = new System.Drawing.Point(16, 21);
            this.chbContrastFilterEnabled.Name = "chbContrastFilterEnabled";
            this.chbContrastFilterEnabled.Size = new System.Drawing.Size(89, 20);
            this.chbContrastFilterEnabled.TabIndex = 0;
            this.chbContrastFilterEnabled.Text = "Włączony";
            this.chbContrastFilterEnabled.UseVisualStyleBackColor = true;
            this.chbContrastFilterEnabled.CheckedChanged += new System.EventHandler(this.filterCheckedChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.linkLabel3);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.nudMedianSize);
            this.groupBox4.Controls.Add(this.chbMedianEnabled);
            this.groupBox4.Location = new System.Drawing.Point(3, 647);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(372, 120);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Median";
            // 
            // linkLabel3
            // 
            this.linkLabel3.AutoSize = true;
            this.linkLabel3.Location = new System.Drawing.Point(146, 21);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(114, 16);
            this.linkLabel3.TabIndex = 10;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Tag = "http://www.aforgenet.com/framework/docs/html/af40e7dd-59d2-aa3f-b694-995a076a8c61" +
    ".htm";
            this.linkLabel3.Text = "Informacje o filtrze";
            this.linkLabel3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelClicked);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(22, 59);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 16);
            this.label8.TabIndex = 7;
            this.label8.Text = "Squer size";
            // 
            // nudMedianSize
            // 
            this.nudMedianSize.Location = new System.Drawing.Point(114, 57);
            this.nudMedianSize.Maximum = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.nudMedianSize.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.nudMedianSize.Name = "nudMedianSize";
            this.nudMedianSize.Size = new System.Drawing.Size(87, 22);
            this.nudMedianSize.TabIndex = 1;
            this.nudMedianSize.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.nudMedianSize.ValueChanged += new System.EventHandler(this.nudMedianSize_ValueChanged);
            // 
            // chbMedianEnabled
            // 
            this.chbMedianEnabled.AutoSize = true;
            this.chbMedianEnabled.Location = new System.Drawing.Point(16, 21);
            this.chbMedianEnabled.Name = "chbMedianEnabled";
            this.chbMedianEnabled.Size = new System.Drawing.Size(89, 20);
            this.chbMedianEnabled.TabIndex = 0;
            this.chbMedianEnabled.Text = "Włączony";
            this.chbMedianEnabled.UseVisualStyleBackColor = true;
            this.chbMedianEnabled.CheckedChanged += new System.EventHandler(this.filterCheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.linkLabel2);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.nudBrightness);
            this.groupBox3.Controls.Add(this.chbBrightnessEnabled);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 123);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(372, 120);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "BrightnessCorrection";
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(146, 21);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(114, 16);
            this.linkLabel2.TabIndex = 10;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Tag = "http://www.aforgenet.com/framework/docs/html/2d369c5f-1c11-5607-7127-fddff34e0dff" +
    ".htm";
            this.linkLabel2.Text = "Informacje o filtrze";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelClicked);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(22, 59);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 16);
            this.label10.TabIndex = 7;
            this.label10.Text = "Brightness";
            // 
            // nudBrightness
            // 
            this.nudBrightness.Location = new System.Drawing.Point(114, 57);
            this.nudBrightness.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudBrightness.Minimum = new decimal(new int[] {
            255,
            0,
            0,
            -2147483648});
            this.nudBrightness.Name = "nudBrightness";
            this.nudBrightness.Size = new System.Drawing.Size(87, 22);
            this.nudBrightness.TabIndex = 1;
            this.nudBrightness.Value = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.nudBrightness.ValueChanged += new System.EventHandler(this.nudBrightness_ValueChanged);
            // 
            // chbBrightnessEnabled
            // 
            this.chbBrightnessEnabled.AutoSize = true;
            this.chbBrightnessEnabled.Location = new System.Drawing.Point(16, 21);
            this.chbBrightnessEnabled.Name = "chbBrightnessEnabled";
            this.chbBrightnessEnabled.Size = new System.Drawing.Size(89, 20);
            this.chbBrightnessEnabled.TabIndex = 0;
            this.chbBrightnessEnabled.Text = "Włączony";
            this.chbBrightnessEnabled.UseVisualStyleBackColor = true;
            this.chbBrightnessEnabled.CheckedChanged += new System.EventHandler(this.filterCheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chBFillOutsideRange);
            this.groupBox2.Controls.Add(this.linkLabel1);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.nudColorFilterBlueMax);
            this.groupBox2.Controls.Add(this.nudColorFilterBlueMin);
            this.groupBox2.Controls.Add(this.nudColorFilterGreenMax);
            this.groupBox2.Controls.Add(this.nudColorFilterGreenMin);
            this.groupBox2.Controls.Add(this.nudColorFilterRedMax);
            this.groupBox2.Controls.Add(this.nudColorFilterRedMin);
            this.groupBox2.Controls.Add(this.chbColoringFilterEnabled);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 460);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(372, 181);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ColorFiltering";
            // 
            // chBFillOutsideRange
            // 
            this.chBFillOutsideRange.AutoSize = true;
            this.chBFillOutsideRange.Location = new System.Drawing.Point(85, 162);
            this.chBFillOutsideRange.Name = "chBFillOutsideRange";
            this.chBFillOutsideRange.Size = new System.Drawing.Size(131, 20);
            this.chBFillOutsideRange.TabIndex = 11;
            this.chBFillOutsideRange.Text = "FillOutsideRange";
            this.chBFillOutsideRange.UseVisualStyleBackColor = true;
            this.chBFillOutsideRange.CheckedChanged += new System.EventHandler(this.filterCheckedChanged);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(146, 21);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(114, 16);
            this.linkLabel1.TabIndex = 10;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Tag = "http://www.aforgenet.com/framework/docs/html/35bd90e3-4e35-8f5f-e255-26c5d8d4b927" +
    ".htm";
            this.linkLabel1.Text = "Informacje o filtrze";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelClicked);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 16);
            this.label4.TabIndex = 9;
            this.label4.Text = "blue";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 101);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 16);
            this.label5.TabIndex = 8;
            this.label5.Text = "green";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 16);
            this.label6.TabIndex = 7;
            this.label6.Text = "red";
            // 
            // nudColorFilterBlueMax
            // 
            this.nudColorFilterBlueMax.Location = new System.Drawing.Point(191, 134);
            this.nudColorFilterBlueMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudColorFilterBlueMax.Name = "nudColorFilterBlueMax";
            this.nudColorFilterBlueMax.Size = new System.Drawing.Size(87, 22);
            this.nudColorFilterBlueMax.TabIndex = 6;
            this.nudColorFilterBlueMax.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            this.nudColorFilterBlueMax.ValueChanged += new System.EventHandler(this.numericColorFilterChanged);
            // 
            // nudColorFilterBlueMin
            // 
            this.nudColorFilterBlueMin.Location = new System.Drawing.Point(85, 134);
            this.nudColorFilterBlueMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudColorFilterBlueMin.Name = "nudColorFilterBlueMin";
            this.nudColorFilterBlueMin.Size = new System.Drawing.Size(87, 22);
            this.nudColorFilterBlueMin.TabIndex = 5;
            this.nudColorFilterBlueMin.ValueChanged += new System.EventHandler(this.numericColorFilterChanged);
            // 
            // nudColorFilterGreenMax
            // 
            this.nudColorFilterGreenMax.Location = new System.Drawing.Point(191, 95);
            this.nudColorFilterGreenMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudColorFilterGreenMax.Name = "nudColorFilterGreenMax";
            this.nudColorFilterGreenMax.Size = new System.Drawing.Size(87, 22);
            this.nudColorFilterGreenMax.TabIndex = 4;
            this.nudColorFilterGreenMax.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            this.nudColorFilterGreenMax.ValueChanged += new System.EventHandler(this.numericColorFilterChanged);
            // 
            // nudColorFilterGreenMin
            // 
            this.nudColorFilterGreenMin.Location = new System.Drawing.Point(85, 95);
            this.nudColorFilterGreenMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudColorFilterGreenMin.Name = "nudColorFilterGreenMin";
            this.nudColorFilterGreenMin.Size = new System.Drawing.Size(87, 22);
            this.nudColorFilterGreenMin.TabIndex = 3;
            this.nudColorFilterGreenMin.ValueChanged += new System.EventHandler(this.numericColorFilterChanged);
            // 
            // nudColorFilterRedMax
            // 
            this.nudColorFilterRedMax.Location = new System.Drawing.Point(191, 57);
            this.nudColorFilterRedMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudColorFilterRedMax.Name = "nudColorFilterRedMax";
            this.nudColorFilterRedMax.Size = new System.Drawing.Size(87, 22);
            this.nudColorFilterRedMax.TabIndex = 2;
            this.nudColorFilterRedMax.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            this.nudColorFilterRedMax.ValueChanged += new System.EventHandler(this.numericColorFilterChanged);
            // 
            // nudColorFilterRedMin
            // 
            this.nudColorFilterRedMin.Location = new System.Drawing.Point(85, 57);
            this.nudColorFilterRedMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudColorFilterRedMin.Name = "nudColorFilterRedMin";
            this.nudColorFilterRedMin.Size = new System.Drawing.Size(87, 22);
            this.nudColorFilterRedMin.TabIndex = 1;
            this.nudColorFilterRedMin.ValueChanged += new System.EventHandler(this.numericColorFilterChanged);
            // 
            // chbColoringFilterEnabled
            // 
            this.chbColoringFilterEnabled.AutoSize = true;
            this.chbColoringFilterEnabled.Location = new System.Drawing.Point(16, 21);
            this.chbColoringFilterEnabled.Name = "chbColoringFilterEnabled";
            this.chbColoringFilterEnabled.Size = new System.Drawing.Size(89, 20);
            this.chbColoringFilterEnabled.TabIndex = 0;
            this.chbColoringFilterEnabled.Text = "Włączony";
            this.chbColoringFilterEnabled.UseVisualStyleBackColor = true;
            this.chbColoringFilterEnabled.CheckedChanged += new System.EventHandler(this.filterCheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.nudLinearLevelsGrayTo);
            this.groupBox1.Controls.Add(this.nudLinearLevelsGrayFrom);
            this.groupBox1.Controls.Add(this.linkLabelLevelsLinear);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.nudLinearLevelsBlueTo);
            this.groupBox1.Controls.Add(this.nudLinearLevelsBlueFrom);
            this.groupBox1.Controls.Add(this.nudLinearLevelsGreenTo);
            this.groupBox1.Controls.Add(this.nudLinearLevelsGreenFrom);
            this.groupBox1.Controls.Add(this.nudLinearLevelsRedTo);
            this.groupBox1.Controls.Add(this.nudLinearLevelsRedFrom);
            this.groupBox1.Controls.Add(this.chbLinearLevelsEnable);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 249);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(372, 205);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "LevelsLinear";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 177);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 16);
            this.label7.TabIndex = 13;
            this.label7.Text = "inGray";
            // 
            // nudLinearLevelsGrayTo
            // 
            this.nudLinearLevelsGrayTo.Location = new System.Drawing.Point(191, 171);
            this.nudLinearLevelsGrayTo.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudLinearLevelsGrayTo.Name = "nudLinearLevelsGrayTo";
            this.nudLinearLevelsGrayTo.Size = new System.Drawing.Size(87, 22);
            this.nudLinearLevelsGrayTo.TabIndex = 12;
            this.nudLinearLevelsGrayTo.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudLinearLevelsGrayTo.ValueChanged += new System.EventHandler(this.numericLevelsLinearChanged);
            // 
            // nudLinearLevelsGrayFrom
            // 
            this.nudLinearLevelsGrayFrom.Location = new System.Drawing.Point(85, 171);
            this.nudLinearLevelsGrayFrom.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudLinearLevelsGrayFrom.Name = "nudLinearLevelsGrayFrom";
            this.nudLinearLevelsGrayFrom.Size = new System.Drawing.Size(87, 22);
            this.nudLinearLevelsGrayFrom.TabIndex = 11;
            this.nudLinearLevelsGrayFrom.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            this.nudLinearLevelsGrayFrom.ValueChanged += new System.EventHandler(this.numericLevelsLinearChanged);
            // 
            // linkLabelLevelsLinear
            // 
            this.linkLabelLevelsLinear.AutoSize = true;
            this.linkLabelLevelsLinear.Location = new System.Drawing.Point(146, 21);
            this.linkLabelLevelsLinear.Name = "linkLabelLevelsLinear";
            this.linkLabelLevelsLinear.Size = new System.Drawing.Size(114, 16);
            this.linkLabelLevelsLinear.TabIndex = 10;
            this.linkLabelLevelsLinear.TabStop = true;
            this.linkLabelLevelsLinear.Tag = "http://www.aforgenet.com/framework/docs/html/29bf1191-47c9-314b-eb8d-bea3f903ac28" +
    ".htm";
            this.linkLabelLevelsLinear.Text = "Informacje o filtrze";
            this.linkLabelLevelsLinear.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelClicked);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 16);
            this.label3.TabIndex = 9;
            this.label3.Text = "inBlue";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 16);
            this.label2.TabIndex = 8;
            this.label2.Text = "inGreen";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "inRed";
            // 
            // nudLinearLevelsBlueTo
            // 
            this.nudLinearLevelsBlueTo.Location = new System.Drawing.Point(191, 134);
            this.nudLinearLevelsBlueTo.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudLinearLevelsBlueTo.Name = "nudLinearLevelsBlueTo";
            this.nudLinearLevelsBlueTo.Size = new System.Drawing.Size(87, 22);
            this.nudLinearLevelsBlueTo.TabIndex = 6;
            this.nudLinearLevelsBlueTo.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudLinearLevelsBlueTo.ValueChanged += new System.EventHandler(this.numericLevelsLinearChanged);
            // 
            // nudLinearLevelsBlueFrom
            // 
            this.nudLinearLevelsBlueFrom.Location = new System.Drawing.Point(85, 134);
            this.nudLinearLevelsBlueFrom.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudLinearLevelsBlueFrom.Name = "nudLinearLevelsBlueFrom";
            this.nudLinearLevelsBlueFrom.Size = new System.Drawing.Size(87, 22);
            this.nudLinearLevelsBlueFrom.TabIndex = 5;
            this.nudLinearLevelsBlueFrom.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            this.nudLinearLevelsBlueFrom.ValueChanged += new System.EventHandler(this.numericLevelsLinearChanged);
            // 
            // nudLinearLevelsGreenTo
            // 
            this.nudLinearLevelsGreenTo.Location = new System.Drawing.Point(191, 95);
            this.nudLinearLevelsGreenTo.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudLinearLevelsGreenTo.Name = "nudLinearLevelsGreenTo";
            this.nudLinearLevelsGreenTo.Size = new System.Drawing.Size(87, 22);
            this.nudLinearLevelsGreenTo.TabIndex = 4;
            this.nudLinearLevelsGreenTo.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudLinearLevelsGreenTo.ValueChanged += new System.EventHandler(this.numericLevelsLinearChanged);
            // 
            // nudLinearLevelsGreenFrom
            // 
            this.nudLinearLevelsGreenFrom.Location = new System.Drawing.Point(85, 95);
            this.nudLinearLevelsGreenFrom.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudLinearLevelsGreenFrom.Name = "nudLinearLevelsGreenFrom";
            this.nudLinearLevelsGreenFrom.Size = new System.Drawing.Size(87, 22);
            this.nudLinearLevelsGreenFrom.TabIndex = 3;
            this.nudLinearLevelsGreenFrom.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            this.nudLinearLevelsGreenFrom.ValueChanged += new System.EventHandler(this.numericLevelsLinearChanged);
            // 
            // nudLinearLevelsRedTo
            // 
            this.nudLinearLevelsRedTo.Location = new System.Drawing.Point(191, 57);
            this.nudLinearLevelsRedTo.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudLinearLevelsRedTo.Name = "nudLinearLevelsRedTo";
            this.nudLinearLevelsRedTo.Size = new System.Drawing.Size(87, 22);
            this.nudLinearLevelsRedTo.TabIndex = 2;
            this.nudLinearLevelsRedTo.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudLinearLevelsRedTo.ValueChanged += new System.EventHandler(this.numericLevelsLinearChanged);
            // 
            // nudLinearLevelsRedFrom
            // 
            this.nudLinearLevelsRedFrom.Location = new System.Drawing.Point(85, 57);
            this.nudLinearLevelsRedFrom.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudLinearLevelsRedFrom.Name = "nudLinearLevelsRedFrom";
            this.nudLinearLevelsRedFrom.Size = new System.Drawing.Size(87, 22);
            this.nudLinearLevelsRedFrom.TabIndex = 1;
            this.nudLinearLevelsRedFrom.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            this.nudLinearLevelsRedFrom.ValueChanged += new System.EventHandler(this.numericLevelsLinearChanged);
            // 
            // chbLinearLevelsEnable
            // 
            this.chbLinearLevelsEnable.AutoSize = true;
            this.chbLinearLevelsEnable.Location = new System.Drawing.Point(16, 21);
            this.chbLinearLevelsEnable.Name = "chbLinearLevelsEnable";
            this.chbLinearLevelsEnable.Size = new System.Drawing.Size(89, 20);
            this.chbLinearLevelsEnable.TabIndex = 0;
            this.chbLinearLevelsEnable.Text = "Włączony";
            this.chbLinearLevelsEnable.UseVisualStyleBackColor = true;
            this.chbLinearLevelsEnable.CheckedChanged += new System.EventHandler(this.filterCheckedChanged);
            // 
            // CameraFiltersSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 450);
            this.Controls.Add(this.tabControl1);
            this.Name = "CameraFiltersSettingsForm";
            this.Text = "CameraFiltersSettingsForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CameraFiltersSettingsForm_FormClosing);
            this.Load += new System.EventHandler(this.CameraFiltersSettingsForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudContrast)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMedianSize)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBrightness)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudColorFilterBlueMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudColorFilterBlueMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudColorFilterGreenMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudColorFilterGreenMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudColorFilterRedMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudColorFilterRedMin)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudLinearLevelsGrayTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLinearLevelsGrayFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLinearLevelsBlueTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLinearLevelsBlueFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLinearLevelsGreenTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLinearLevelsGreenFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLinearLevelsRedTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLinearLevelsRedFrom)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btnOpenCameraProperty;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown nudLinearLevelsRedTo;
        private System.Windows.Forms.NumericUpDown nudLinearLevelsRedFrom;
        private System.Windows.Forms.CheckBox chbLinearLevelsEnable;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudLinearLevelsBlueTo;
        private System.Windows.Forms.NumericUpDown nudLinearLevelsBlueFrom;
        private System.Windows.Forms.NumericUpDown nudLinearLevelsGreenTo;
        private System.Windows.Forms.NumericUpDown nudLinearLevelsGreenFrom;
        private System.Windows.Forms.LinkLabel linkLabelLevelsLinear;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nudColorFilterBlueMax;
        private System.Windows.Forms.NumericUpDown nudColorFilterBlueMin;
        private System.Windows.Forms.NumericUpDown nudColorFilterGreenMax;
        private System.Windows.Forms.NumericUpDown nudColorFilterGreenMin;
        private System.Windows.Forms.NumericUpDown nudColorFilterRedMax;
        private System.Windows.Forms.NumericUpDown nudColorFilterRedMin;
        private System.Windows.Forms.CheckBox chbColoringFilterEnabled;
        private System.Windows.Forms.CheckBox chBFillOutsideRange;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown nudLinearLevelsGrayTo;
        private System.Windows.Forms.NumericUpDown nudLinearLevelsGrayFrom;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown nudBrightness;
        private System.Windows.Forms.CheckBox chbBrightnessEnabled;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.LinkLabel linkLabel3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown nudMedianSize;
        private System.Windows.Forms.CheckBox chbMedianEnabled;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.LinkLabel linkLabel4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown nudContrast;
        private System.Windows.Forms.CheckBox chbContrastFilterEnabled;
    }
}