﻿using AForge.Imaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ginbot.VisionSystem
{
    public class BlobFilter : IBlobsFilter

    {
        public bool Check(Blob blob)
        {
            bool result = (blob.Area > 160 && blob.Area < 10000);
            return result;
        }
    }
}
