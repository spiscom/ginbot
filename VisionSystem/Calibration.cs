﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ginbot.VisionSystem
{
    public class Calibration
    {

        public Calibration()
        {
            CalibrationPoints = new List<Point2D>();
            ImageCalibrationPoints = new List<Point2D>();
        }
        /// <summary>
        /// Lista punktów (rzeczywistych współrzędnych) kalibracji
        /// punkty muszą znajdować sie na jednej linii w równych odległościach od siebie
        /// </summary>
        public List<Point2D> CalibrationPoints { get; set; }

        /// <summary>
        /// Lista punktów kalibracyjnych wykrytych w obrazie
        /// </summary>
        public List<Point2D> ImageCalibrationPoints { get; private set; }

        public Size ImageSize { get;  set; }

        
        /// <summary>
        /// Odległość pomiędzy punktami kalibracji
        /// </summary>
        public double CalibrationDistance { get; set; }

        //True gdy kalibracja
        public bool IsCalibrated 
        {
            get
            {
                return (CalibrationPoints.Count == 3 && ImageCalibrationPoints.Count == 3);
            }
            set
            {
                if (!value)
                {
                    CalibrationPoints.Clear();
                    ImageCalibrationPoints.Clear();
                }
            }
            
        }


        /// <summary>
        /// Metoda waliduje i zapisuje punkty kalibracji
        /// </summary>
        /// <param name="p1">Punkt 1</param>
        /// <param name="p2">Punkt 2</param>
        /// <param name="p3">Punkt 3</param>
        public void SetCalibrationPoints(Point2D p1,Point2D p2, Point2D p3)
        {
            if (ValidateCalibrationPoints(p1, p2,  p3))
            {
                CalibrationPoints.Clear();
                CalibrationPoints.Add(p1);
                CalibrationPoints.Add(p2);
                CalibrationPoints.Add(p3);
            }
            else
            {
                var msg = "Wprowadzone punkty kalibracji nie spełniają warunków:\n"+
                    "-Punkty muszą znajdować się na jednej prostej,\n"+
                    "-Odległość między punktami musi być równa.";
                throw new Exception(msg);
            }
        }

        /// <summary>
        /// Metoda sprawdza czy punkty kalibracyjne znajdują się na jednej prostej
        /// i czy odległość między nimi jest równa
        /// </summary>
        /// <param name="p1">Punkt 1</param>
        /// <param name="p2">Punkt 2</param>
        /// <param name="p3">Punkt 3</param>
        /// <returns></returns>
        public bool ValidateCalibrationPoints(Point2D p1, Point2D p2, Point2D p3)
        {
            bool isValid = false;

            //todo ustalić doświadczalnie jaki ma być próg tolerancji
            var distanceThreshold = 1d;

            //sprawdzenie czy punkty są na prostej
                LinearFunction lf = new LinearFunction();
                lf.SetFactorsFromPoints(p1, p2);
                isValid = lf.IsPointOnStraight(p3);


            isValid = isValid && (Math.Abs(p1.GetDistance(p2) - p2.GetDistance(p3)) <= distanceThreshold);
            return isValid;
        }
        /// <summary>
        /// Metoda waliduje i zapisuje punkty kalibracji wykryte w obrazie z kamery
        /// </summary>
        /// <param name="detectedObjects">Lista objektów wykrytych w obrazie kamery</param>
        /// <returns></returns>
        public bool GetCalibrationPointsFromDetectedObjects(List<DetectedObject> detectedObjects)
        {
            bool isValid = false;
            //tymczasowa lista
            List<DetectedObject> objects = new List<DetectedObject>();
            foreach (var detectedObject in detectedObjects)
            {
                //dodanie do listy tylko obiektów w kształcie koła
                if (detectedObject.Name == Shape.Circle) objects.Add(detectedObject);
            }
            if (objects.Count != 3) return isValid;
            var p1 = objects[0].Center.ToPoint2D();
            var p2 = objects[1].Center.ToPoint2D();
            var p3 = objects[2].Center.ToPoint2D();
            isValid = ValidateCalibrationPoints(p1, p2, p3);
            //jeśli walidacja zwróciła true zapisuje punkty kalibracji
            if (isValid)
            {
                ImageCalibrationPoints.Clear();
                ImageCalibrationPoints.Add(p1);
                ImageCalibrationPoints.Add(p2);
                ImageCalibrationPoints.Add(p3);
                //ImageSize = objects[0].ImageSize;
            }

            return isValid;
        }
        /// <summary>
        /// Metoda oblicza rzeczywiste położenie wykrytego w obrazie obiektu
        /// </summary>
        /// <param name="_detectedPoint">Współrzędne punktu w obrazie</param>
        /// <returns>Rzeczywisty punkt</returns>
        public Point2D GetRealPoint(Point _detectedPoint)
        {
            if (!IsCalibrated)
                throw new Exception("System wizyjny nie został skalibrowany");

            int p1Index = 0;
            int p2Index = 2;
            //przeliczenie  współrzędnej x z obrazu do kartezjańskiego układu
            var detectedPoint = CartesianCenter(_detectedPoint.ToPoint2D());

            //pobranie współrzędnych punktów kalibracyjnych w obrazie
            var ipoint1 = CartesianCenter(ImageCalibrationPoints[p1Index]);
            var ipoint2 = CartesianCenter(ImageCalibrationPoints[p2Index]);

            // jeśli wsprzdna X punktu kalib. jest równa wsprzdnej X wykrytego obiektu
            // to zmieniamy punkt kalibracyjny, bo x=c nie jest funkcj liniow
            if (ipoint1.X==detectedPoint.X)
            {
                p1Index = 1;
                ipoint1 = CartesianCenter(ImageCalibrationPoints[p1Index]);
            }
            if (ipoint2.X == detectedPoint.X)
            {
                p2Index = 1;
                ipoint2 = CartesianCenter(ImageCalibrationPoints[p2Index]);
            }

            //pobranie rzeczywistych współrzędnych punktów kalibracyjnych
            var rpoint1 = CalibrationPoints[p1Index];
            var rpoint2 = CalibrationPoints[p2Index];

            LinearFunction lf1 = new LinearFunction();
            LinearFunction lf2 = new LinearFunction();
            //tworzenie prostej przechodzącej przez pierwszy punkt kalibracyjny
            // i wykrty obiekt
            lf1.SetFactorsFromPoints(ipoint1, detectedPoint);

            //tworzenie prostej przechodzącej przez trzeci punkt kalibracyjny
            // i wykrty obiekt
            lf2.SetFactorsFromPoints(ipoint2, detectedPoint);

            
            
            //tworzenie funkcji prostej przechodzących przez punkty kalibracji rzeczywiste
            var realLf = new LinearFunction();
            realLf.SetFactorsFromPoints(rpoint1, rpoint2);
            //tworzenie funkcji prostej przechodzących przez punkty kalibracji w obrazie
            var imageLf = new LinearFunction();
            imageLf.SetFactorsFromPoints(ipoint1, ipoint2);


            //Console.WriteLine("calib image " + imageLf.ToString().Replace(',', '.'));
            //Console.WriteLine("pkt1:" + lf1.ToString().Replace(',', '.'));
            //Console.WriteLine("pkt2:" + lf2.ToString().Replace(',', '.'));
            

            //obliczanie kątów pomiędzy prostą punktów kalibracji w obrazie,a
            //prostymi przechodzącymi przez wykryty punkt i punkty kalibracji
            var angle1 = imageLf.GetAngleBetween(lf1);
            var angle2 = imageLf.GetAngleBetween(lf2);
            //wyznaczanie współczynników prostych o takich samych kątach do prostej rzeczywistych punktów kalibracji
            lf1.aFaktor = realLf.GetAFaktorWithAngle(angle1);
            lf2.aFaktor = realLf.GetAFaktorWithAngle(angle2);
            //Console.WriteLine("angle1: " + angle1);
            //Console.WriteLine("angle2: " + angle2);


            //obliczanie współczynnika b z rzeczywistych współrzędnych punktów kalibracyjnych
            lf1.SetBFactorFromPoint(rpoint1);
            lf2.SetBFactorFromPoint(rpoint2);

            //Console.WriteLine("calib real: " + realLf.ToString().Replace(',', '.'));
            //Console.WriteLine("pkt1:" + lf1.ToString().Replace(',', '.'));
            //Console.WriteLine("pkt2:" + lf2.ToString().Replace(',', '.'));


            //zwrócenie punktu przecięcia się prostych
            return lf1.GetCrossPoint(lf2);
        }
        //todo utowrzyć wspólną metodę GetRealPoint, aby nie powtarzać kodu
        /// <summary>
        /// Metoda oblicza współrzędna w obrazie dla rzeczywistego punktu
        /// </summary>
        /// <param name="_detectedPoint"></param>
        /// <returns></returns>
        public Point RealPointToImage(Point _realPoint)
        {
            if (!IsCalibrated)
                throw new Exception("System wizyjny nie został skalibrowany");

            int p1Index = 0;
            int p2Index = 2;
            //przeliczenie  współrzędnej x z obrazu do kartezjańskiego układu
            var realPoint = _realPoint.ToPoint2D();

            //pobranie rzeczywistych współrzędnych punktów kalibracyjnych
            var rpoint1 = CalibrationPoints[p1Index];
            var rpoint2 = CalibrationPoints[p2Index];

            // jeśli wsprzdna X punktu kalib. jest równa wsprzdnej X wykrytego obiektu
            // to zmieniamy punkt kalibracyjny, bo x=c nie jest funkcj liniow
            if (rpoint1.X == realPoint.X)
            {
                p1Index = 1;
                rpoint1 = CalibrationPoints[p1Index];
            }
            if (rpoint2.X == realPoint.X)
            {
                p2Index = 1;
                rpoint2 = CalibrationPoints[p2Index];
            }

           
            //pobranie współrzędnych punktów kalibracyjnych w obrazie
            var ipoint1 = CartesianCenter(ImageCalibrationPoints[p1Index]);
            var ipoint2 = CartesianCenter(ImageCalibrationPoints[p2Index]);

            LinearFunction lf1 = new LinearFunction();
            LinearFunction lf2 = new LinearFunction();
            //tworzenie prostej przechodzącej przez pierwszy punkt kalibracyjny
            // i wykrty obiekt
            lf1.SetFactorsFromPoints(rpoint1, realPoint);

            //tworzenie prostej przechodzącej przez trzeci punkt kalibracyjny
            // i wykrty obiekt
            lf2.SetFactorsFromPoints(rpoint2, realPoint);

            //tworzenie funkcji prostych przechodzących przez punkty kalibracji
            // rzeczywiste i w obrazie
            var realLf = new LinearFunction();
            realLf.SetFactorsFromPoints(rpoint1, rpoint2);
            var imageLf = new LinearFunction();
            imageLf.SetFactorsFromPoints(ipoint1, ipoint2);


            //Console.WriteLine("calib image " + imageLf.ToString().Replace(',', '.'));
            //Console.WriteLine("pkt1:" + lf1.ToString().Replace(',', '.'));
            //Console.WriteLine("pkt2:" + lf2.ToString().Replace(',', '.'));


            //obliczanie kątów pomiędzy prostą punktów kalibracji w rzeczywistych,a
            //prostymi przechodzącymi przez  punkt i punkty kalibracji 
            var angle1 = realLf.GetAngleBetween(lf1);
            var angle2 = realLf.GetAngleBetween(lf2);
            //wyznaczanie współczynników prostych o takich samych kątach do prostej  punktów kalibracji w obrazie
            lf1.aFaktor = imageLf.GetAFaktorWithAngle(angle1);
            lf2.aFaktor = imageLf.GetAFaktorWithAngle(angle2);
            //Console.WriteLine("angle1: " + angle1);
            //Console.WriteLine("angle2: " + angle2);


            //obliczanie współczynnika b z współrzędnych punktów kalibracyjnych w obrazie
            lf1.SetBFactorFromPoint(ipoint1);
            lf2.SetBFactorFromPoint(ipoint2);

            //Console.WriteLine("calib real: " + realLf.ToString().Replace(',', '.'));
            //Console.WriteLine("pkt1:" + lf1.ToString().Replace(',', '.'));
            //Console.WriteLine("pkt2:" + lf2.ToString().Replace(',', '.'));


            
            var imagePoint = lf1.GetCrossPoint(lf2);
            //odwrócenie współrzędnej Y i zwrócenie punktu przecięcia się prostych, 
            return new Point((int)imagePoint.X, (int)(ImageSize.Height - imagePoint.Y)); ;
        }
        /// <summary>
        /// Metoda przelicza współrzędne punktu obrazu na współrzędne
        /// układu kartezjańskiego (poczatek układu w obrazie jest w górnym lewym rogu)
        /// </summary>
        /// <param name="imagePoint">Punkt w obrazie</param>
        /// <returns>Punkt </returns>
        public Point2D CartesianCenter(Point2D imagePoint)
        {
            return new Point2D(imagePoint.X, ImageSize.Height- imagePoint.Y);
            //return imagePoint;
        }
    }
}
