﻿using AForge;
using AForge.Imaging.Filters;
using AForge.Video.DirectShow;
using ginbot.General;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ginbot.VisionSystem
{
    public class VisionSystem
    {
        public VisionSystem()
        {

            calibration = new Calibration();
            calibrationFrameQueue = new Queue<Bitmap>();
            frameQueue= new Queue<Bitmap>();



        }

        /// <summary>
        /// Żrodło wideo
        /// </summary>
        public VideoCaptureDevice VideoDevice { get; set; }
        /// <summary>
        /// Zdarzenie pojawienia sie nowego kadru video
        /// </summary>
        public event VisionSystemNewFrameEvent NewFrameEvent;

        /// <summary>
        /// Zdarzenie występuje przy zmianie postępu kalibracji
        /// </summary>
        public event CalibrationProgressChangedEvent calibrationProgressChangedEvent;

        /// <summary>
        /// Wartość określa, co którą klatkę wideo analizować 
        /// </summary>
        public int FrameOneOf { get; set; } = 1;
        /// <summary>
        /// True aby obrysowywać wykryte obiekty, false w przeciwny razie
        /// </summary>
        public bool DrawingObjectFrame { get; set; } = true;
        /// <summary>
        /// True aby wykrywać obiekty, false w przeciwny razie
        /// </summary>
        public bool ObjectDetection { get; set; } = true;
        /// <summary>
        /// Lista wykrytych obiektów
        /// </summary>
        public List<DetectedObject> DetectedObjects;

        public Calibration calibration { get; set; }

        public bool ShowFilteredFrame { get; set; } = false;



        //Filtry
        public ImagingFilters filters { get; set; } = new ImagingFilters();

        private int _frameOneOf = 1;
        //bufor klatek do analizy
        private Queue<Bitmap> frameQueue;
        //bufor klatek do analizy
        private Queue<Bitmap> calibrationFrameQueue;
        //ilość klatek / prób wykrycia punktów kalibracyjnych
        private int calibrationAttempts = 100;
        private int calibrationProgress;
        //nazwa przekazuwana go loggera
        private string logName = "VisionSystem";
        /// <summary>
        /// Uruchamia przechwytywanie wideo z kamery
        /// </summary>
        public void Start()
        {
            if (VideoDevice == null) return;
            frameQueue = new Queue<Bitmap>();
            VideoDevice.Start();

        }
        /// <summary>
        /// Zatrzymuje przechwytywanie obrazu z kamery
        /// </summary>
        public void Stop()
        {
            if (VideoDevice == null) return;
            VideoDevice.Stop();
        }

        /// <summary>
        /// Otwiera folmularz umożliwiajacy wybór urzadzenia video
        /// </summary>
        /// <returns>True jeśli wybrano urzadzenie false w przeciwnym razie</returns>
        public bool SelectVideoDevice()
        {
            if (VideoDevice!=null)
            {
                VideoDevice.Stop();
            }
            //tworzy instancję formularza do wyboru urzadzenia video
            var vform = new VideoCaptureDeviceForm();
            if (vform.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                
                VideoDevice = vform.VideoDevice;
                VideoDevice.NewFrame += VideoDevice_NewFrame;
                return true;
            }
            return false;

        }
        /// <summary>
        /// Otwiera okno konfiguracji kamery
        /// </summary>
        public void OpenPropertyWindow()
        {
            //IntPtr window = default(IntPtr);
            //VideoDevice.DisplayPropertyPage(window);
            CameraFiltersSettingsForm cameraFiltersSettingsForm = new CameraFiltersSettingsForm(this);
            cameraFiltersSettingsForm.ShowDialog();
        }
        /// <summary>
        /// Metoda wywoływana przy pojawieniu sie nowej ramki wideo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void VideoDevice_NewFrame(object sender, AForge.Video.NewFrameEventArgs eventArgs)
        {

            if (_frameOneOf == FrameOneOf)
            {
                if (frameQueue.Count > 0) return;
                frameQueue.Enqueue((Bitmap)eventArgs.Frame.Clone());
                Bitmap frame = frameQueue.Dequeue();
                Bitmap displayedFrame = (Bitmap) frame.Clone();
                if (ObjectDetection)
                {

                    filters.ApplyFilters(frame);
                                
                    ObjectDetector objectDetector = new ObjectDetector();
                    //todo przenieść na zewnątrz odfiltrowanie niechcianych kształtów
                    //odfiltrowanie niechcianych kształtów
                    //objectDetector.ShapeOutFilter.Add(Shape.Unknown);
                    objectDetector.ShapeOutFilter.Add(Shape.Triangle);
                    objectDetector.ShapeOutFilter.Add(Shape.Polygon);

                    //ustawienia max rozmiaru obiektu
                    objectDetector.MaxHeight = 85;
                    objectDetector.MaxWidth = 85;
                    if (!calibration.IsCalibrated)
                    {
                        objectDetector.MinHeight = 31;
                        objectDetector.MinWidth = 31;
                    }
                    else
                    {
                        objectDetector.MinHeight = 55;
                        objectDetector.MinWidth = 55;
                    }
                    

                    
                    DetectedObjects = objectDetector.GetDetectedObjects(frame);
                    if (ShowFilteredFrame)
                        displayedFrame = (Bitmap)frame.Clone(); ;

                    if (DrawingObjectFrame)
                    {
                        ObjectDrawer.DrawObjectFrame(displayedFrame, DetectedObjects);
                    }
                }


                VisionSystemNewFrameEventArgs args = new VisionSystemNewFrameEventArgs()
                {
                    NewFrame = (Bitmap)displayedFrame.Clone(),
                    DetectedObjects = DetectedObjects
                };
                NewFrameEvent?.Invoke(this, args);
                _frameOneOf = 0;
                displayedFrame.Dispose();
                frame.Dispose();
            }
            _frameOneOf++;
        }

        public void StartCalibration()
        {
            if (VideoDevice == null) return;
            if (!VideoDevice.IsRunning)
                VideoDevice.Start();
            calibrationAttempts = 100;
            calibrationProgress = calibrationAttempts;
            calibration.IsCalibrated = false;
            frameQueue = new Queue<Bitmap>();
            NewFrameEvent += VideoDevice_NewFrame_Calibration;
        }

        private void VideoDevice_NewFrame_Calibration(object sender, VisionSystemNewFrameEventArgs e)
        {
            //kolejkowanie klatek, jeśli kolejka nie jest pusta nie jest pobierana kolejna klatka
            if (calibrationFrameQueue.Count > 0) return;
            
            calibrationFrameQueue.Enqueue(e.NewFrame);
            var frame = calibrationFrameQueue.Dequeue();
                        var result = false;
            result=calibration.GetCalibrationPointsFromDetectedObjects(e.DetectedObjects);
            calibrationProgress--;

            CalibrationProgressChangedEventArgs args = new CalibrationProgressChangedEventArgs();
            args.ProgressPercentage = Percentage.Progress(calibrationAttempts - calibrationProgress, calibrationAttempts);
            calibrationProgressChangedEvent?.Invoke("Kalibracja",args);

            //Jeśli poprawnie wykryto punkty kalibracyjne 
            if (result)
            {
                NewFrameEvent -= VideoDevice_NewFrame_Calibration;
                CalibrationForm cf = new CalibrationForm(calibration, frame);               
                args.ProgressPercentage = 100;
                var msg = "Kalibracja powiodła się!";
                calibrationProgressChangedEvent?.Invoke(msg, args);
                Logger.Log(logName, msg);
                cf.ShowDialog();

            }
            if (calibrationProgress == 0)
            {
                NewFrameEvent -= VideoDevice_NewFrame_Calibration;
                args.ProgressPercentage = 100;
                var msg = "Kalibracja nie powiodła się!";
                calibrationProgressChangedEvent?.Invoke(msg, args);
                Logger.Log(logName, msg);
                msg = "Kalibracja nie powiodła się!\n" +
                    "Nie wykryto punktów kalibracyjnych\n" +
                    "lub nie spełniają one warunków.";
                MessageBox.Show(msg, "Kalibracja", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }




    }
}
