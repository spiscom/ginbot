﻿using ginbot.RoboArm;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ginbot.VisionSystem
{
    public class DetectedObject
    {
        public int Id{ get; set; }
        public string Name { get; set; }
        public Point Center { get; set; }
        public List<Point> Corners { get; set; }
        public float Radius { get; set; }
        public Color ColorMean { get; set; }
        //public Size ImageSize { get; set; }

        /// <summary>
        /// Metoda oblicza kąt odchylenia objektu od pionu
        /// </summary>
        /// <returns>Kąt odchylenia</returns>
        public double Rotation()
        {
            if (Corners.Count < 2)
                return 0;
            return Geometry.GetDeviation(Corners[1], Corners[0]);
        }

        public override string ToString()
        {
            var msg = $"Id: {Id}\n"+
                $"Name: {Name}\n"+
                $"Center: {Center}\n";
            return msg;
        }

    }
}
