﻿using System;
using ginbot.VisionSystem;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing;
using System.Collections.Generic;

namespace UnitTest
{
    [TestClass]
    public class ObjectDetectorTest
    {
        [TestMethod]
        public void GetDetectedObjectsTest()
        {
            //string[] imagePaths = new string[] { "test.png","15st.png", "30st.png","45st.png","60st.png" };
            string[] imagePaths = new string[] { "test.png" };
            foreach (var path in imagePaths)
            {

                ObjectDetector objectDetector = new ObjectDetector();
                Bitmap bitmap = (Bitmap)Image.FromFile(path);
                List<DetectedObject> detectedObjects = objectDetector.GetDetectedObjects(bitmap);
                foreach (var detectedObject in detectedObjects)
                {
                    Console.WriteLine($"Path: {path}");
                    Console.WriteLine($"Id: {detectedObject.Id}");
                    Console.WriteLine($"Name: {detectedObject.Name}");
                    Console.WriteLine($"Center: {detectedObject.Center}");
                    Console.WriteLine($"Radius: {detectedObject.Radius}");
                    Console.WriteLine($"Rotation: {detectedObject.Rotation()}");
                    Console.WriteLine($"ColorMean: {detectedObject.ColorMean}");
                    Console.WriteLine($"Corners:");

                    foreach (var corner in detectedObject.Corners)
                    {
                        Console.WriteLine($"{corner.X},{corner.Y}");
                    }
                    Console.WriteLine("\n");
                    
                }
                ObjectDrawer.DrawObjectFrame(bitmap, detectedObjects);
                bitmap.Save($"{path}_out1.png");
                Assert.AreEqual(5, detectedObjects.Count);
            }
        }

        [TestMethod]
        public void GetDetectedObjectsCalibrationTest()
        {
            string[] imagePaths = new string[] {  "kalib.bmp" };

            foreach (var path in imagePaths)
            {

                ObjectDetector objectDetector = new ObjectDetector();

                objectDetector.MinHeight = 20;
                objectDetector.MinWidth = 20;
                Bitmap bitmap = (Bitmap)Image.FromFile(path);
                var ImageCenter = new Point( bitmap.Width / 2, bitmap.Height / 2);
                List<DetectedObject> detectedObjects = objectDetector.GetDetectedObjects(bitmap);
                foreach (var detectedObject in detectedObjects)
                {
                    Console.WriteLine($"Path: {path}");
                    Console.WriteLine($"Id: {detectedObject.Id}");
                    Console.WriteLine($"Name: {detectedObject.Name}");
                    Console.WriteLine($"Center: {detectedObject.Center}");
                    Console.WriteLine($"Radius: {detectedObject.Radius}");
                    Console.WriteLine($"Rotation: {detectedObject.Rotation()}");
                    Console.WriteLine($"Image Size: {bitmap.Width}:{bitmap.Height}");
                    Console.WriteLine($"środek obrazu: {ImageCenter}");
                    Console.WriteLine($"Odległośc do środka obrazu: {detectedObject.Center.GetDistance(ImageCenter)}");
                    
                    foreach (var corner in detectedObject.Corners)
                    {
                        Console.WriteLine($"{corner.X},{corner.Y}");
                    }
                    Console.WriteLine("\n");

                }
                Assert.AreEqual(4, detectedObjects.Count);
            }
        }

        [TestMethod]
        public void GetDetectedObjectsCalibration2stTest()
        {
            string[] imagePaths = new string[] { "kalib2st.png" };

            foreach (var path in imagePaths)
            {

                ObjectDetector objectDetector = new ObjectDetector();


                Bitmap bitmap = (Bitmap)Image.FromFile(path);
                List<DetectedObject> detectedObjects = objectDetector.GetDetectedObjects(bitmap);
                var ImageCenter = new Point(bitmap.Width / 2, bitmap.Height / 2);
                foreach (var detectedObject in detectedObjects)
                {
                    Console.WriteLine($"Path: {path}");
                    Console.WriteLine($"Id: {detectedObject.Id}");
                    Console.WriteLine($"Name: {detectedObject.Name}");
                    Console.WriteLine($"Center: {detectedObject.Center}");
                    Console.WriteLine($"Radius: {detectedObject.Radius}");
                    Console.WriteLine($"Rotation: {detectedObject.Rotation()}");
                    Console.WriteLine($"Image Size: {bitmap.Width}:{bitmap.Height}");
                    Console.WriteLine($"środek obrazu: {ImageCenter}");
                    Console.WriteLine($"Odległośc do środeka obrazu: {detectedObject.Center.GetDistance(ImageCenter)}");

                    foreach (var corner in detectedObject.Corners)
                    {
                        Console.WriteLine($"{corner.X},{corner.Y}");
                    }
                    Console.WriteLine("\n");

                }
                Assert.AreEqual(3, detectedObjects.Count);
            }
        }
    }
}
