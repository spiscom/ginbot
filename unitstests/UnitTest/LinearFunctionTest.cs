﻿using System;
using System.Collections.Generic;
using System.Drawing;
using ginbot.VisionSystem;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest
{
    [TestClass]
    public class LinearFunctionTest
    {
        [TestMethod]
        public void getFactorsTest()
        {
            var pointA = new Point2D(5, 6);
            var pointB = new Point2D(7, 11);
            double expAFactor = 5d / 2;
            double expBFactor = -13d / 2;
            LinearFunction lf = new LinearFunction();
            lf.SetFactorsFromPoints(pointA, pointB);
            var aFactor = lf.aFaktor;
            var bFactor = lf.bFaktor;
            Assert.AreEqual(expAFactor, aFactor);
            Assert.AreEqual(expBFactor, bFactor);
        }

        [TestMethod]
        public void GetCrossPointTest()
        {

            LinearFunction lf = new LinearFunction();
            lf.aFaktor = 3;
            lf.bFaktor = -2;
            LinearFunction lf2 = new LinearFunction();
            lf2.aFaktor = -1;
            lf2.bFaktor = 4;
            var expCrosspoint = new Point2D(1.5, 2.5);
            var Crosspoint = lf.GetCrossPoint(lf2);
            Assert.AreEqual(expCrosspoint.X, Crosspoint.X);
            Assert.AreEqual(expCrosspoint.Y, Crosspoint.Y);
        }
        [TestMethod]
        public void getBFactorTest()
        {
            var pointA = new Point2D(5, 6);
            var pointB = new Point2D(7, 11);
            double expAFactor = 5d / 2;
            double expBFactor = -13d / 2;
            LinearFunction lf = new LinearFunction();
            lf.aFaktor = expAFactor;
            var aFactor = lf.aFaktor;
            var bFactor = lf.SetBFactorFromPoint(pointB).bFaktor;

            Assert.AreEqual(expBFactor, bFactor);
        }

        [TestMethod]
        public void isOnStraightTest()
        {
            var pointA = new Point2D(5, 6);
            var pointB = new Point2D(7, 11);
            LinearFunction lf = new LinearFunction();
            lf.SetFactorsFromPoints(pointA, pointB);
            var testPoint = new Point2D(7, 11);
            var isOnStraight = lf.IsPointOnStraight(testPoint);

            Assert.IsTrue(isOnStraight);
        }

        [TestMethod]
        public void IsPointOnStraightTest()
        {
            string[] imagePaths = new string[] { "kalib2st.png" };

            foreach (var path in imagePaths)
            {

                ObjectDetector objectDetector = new ObjectDetector();
                Bitmap bitmap = (Bitmap)Image.FromFile(path);
                List<DetectedObject> detectedObjects = objectDetector.GetDetectedObjects(bitmap);
                LinearFunction lf = new LinearFunction();
                bool[] isOnStraight = new bool[detectedObjects.Count];
                if (detectedObjects.Count >= 2)
                {
                    var p1 = detectedObjects[0].Center.ToPoint2D();
                    var p2= detectedObjects[1].Center.ToPoint2D();
                    
                    lf.SetFactorsFromPoints(p1, p2);
                }
                int i = 0;
                foreach (var detectedObject in detectedObjects)
                {
                    var center = detectedObject.Center.ToPoint2D();
                    isOnStraight[i] = lf.IsPointOnStraight(center);
                    Console.WriteLine($"({center.X},{center.Y}),{isOnStraight}");
                    i++;
                }
                //ObjectDrawer.DrawObjectFrame(bitmap, detectedObjects);

                Assert.AreEqual(3, detectedObjects.Count);
                Assert.IsTrue(isOnStraight[0]);
                Assert.IsTrue(isOnStraight[1]);
                Assert.IsTrue(isOnStraight[2]);

            }
        }
        [TestMethod]
        public void GetAngleBetweenStraightTest()
        {

            LinearFunction lf = new LinearFunction();
            lf.aFaktor = -2;
            lf.bFaktor = 6;
            LinearFunction lf2 = new LinearFunction();
            lf2.aFaktor = 3;
            lf2.bFaktor = 1;
            var expAngle = 45;
            var angle = Geometry.RadianToDegree(lf.GetAngleBetween(lf2));

            Assert.AreEqual(expAngle,angle);
        }

    }
}
