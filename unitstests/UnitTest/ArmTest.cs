﻿using System;
using System.IO.Ports;
using System.Windows.Forms;
using ginbot.RoboArm;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest
{
    [TestClass]
    public class ArmTest
    {
        [TestMethod]
        public void SetPositionTest()
        {
            //Arrange
            SerialPort port = new SerialPort("COM1");
            port.Open();
            Arm arm = new Arm(100,0,0 );
            bool isPositionSet;
            bool actual=false;
            //Action
            try
            {
                isPositionSet = true;
                actual = arm.SetPosition(-180);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                isPositionSet = false;
            }
            finally
            { 
                port.Close(); 
            }
            port.Close();
            //asserts
            Assert.AreEqual(isPositionSet, actual);
        }

        [TestMethod]
        public void roundToStepTest()
        {
            //Arrange
            SerialPort port = new SerialPort("COM1");
            
            Arm arm = new Arm(100, 0, 0);
            double expected = 45.99;
            double actual = Arm.RoundToStep(46);
            //Action

            //asserts
            Assert.AreEqual(expected, actual);
        }
    }
}
