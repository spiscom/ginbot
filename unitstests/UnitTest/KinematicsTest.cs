﻿using System;
using ginbot.RoboArm;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest
{
    [TestClass]
    public class KinematicsTest
    {
        [TestMethod]
        public void ForwordKinematicsTest()
        {
            RoboArm rb = new RoboArm();
            rb.basis.ArmAngle.Angle = 90;
            rb.arm1.ArmAngle.Angle = 90;
            rb.arm2.ArmAngle.Angle = -90;
            rb.clampArm.ArmAngle.Angle = -90;
            var p1 = rb.Position3D;
            var p2 = Kinematics.ForwardKinematics(rb.roboAngles);
            Assert.AreEqual(p1.ToString(), p2.ToString());
        }

        [TestMethod]
        public void InverseKinematicsTest()
        {
            //Arrange


            var roboArm = new RoboArm();
            roboArm.basis.ArmAngle.Angle = 10;
            Point3D target = new Point3D(-100, 50, 10);
            //Act

            Console.WriteLine(roboArm.Position3D.GetDistance(target));
            Console.WriteLine($"{roboArm.Position3D.X}, {roboArm.Position3D.Y}, {roboArm.Position3D.Z}");
            Console.WriteLine();

            var angles = Kinematics.InverseKinematics(target, roboArm.roboAngles);
            var point = Kinematics.ForwardKinematics(angles);
            var dist = point.GetDistance(target);
            Console.WriteLine(point.ToString());
            Console.WriteLine(dist);
            Console.WriteLine();



            //Assert
            Assert.IsTrue(dist < 0.5);

        }
    }
}
