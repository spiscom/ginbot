﻿using System;
using ginbot.General;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest
{
    [TestClass]
    public class GenaralTest
    {
        [TestMethod]
        public void PercentageTest()
        {
            int value = 10;
            int max = 200;
            Assert.AreEqual(5, Percentage.Progress(value, max));
        }
    }
}
