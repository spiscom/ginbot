﻿using System;
using System.Collections.Generic;
using System.Drawing;
using ginbot.VisionSystem;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest
{
    [TestClass]
    public class GeometryTest
    {
        [TestMethod]
        public void GetDeviationTest()
        {
            var point1 = new Point(0, 0);
            var point2 = new Point(1, 1);
            double expectedAngle = 45;
            double actualAngle = Geometry.GetDeviation(point1, point2);
            Assert.AreEqual(expectedAngle, actualAngle);
        }

        [TestMethod]
        public void GetDeviationImageRotatedTest()
        {
            var path = "kalib2st.png";
            ObjectDetector objectDetector = new ObjectDetector();
            Bitmap bitmap = (Bitmap)Image.FromFile(path);
            
            List<DetectedObject> detectedObjects = objectDetector.GetDetectedObjects(bitmap);
            var point1 = detectedObjects[0].Center;
            var point2 = detectedObjects[1].Center;
            double expectedAngle = 2;
            double actualAngle = Geometry.GetDeviation(point1, point2);
            Console.WriteLine($"Odchylenie obrazu: {actualAngle}");

            Assert.AreEqual(expectedAngle, Math.Round(actualAngle,0));
        }
    }
}
