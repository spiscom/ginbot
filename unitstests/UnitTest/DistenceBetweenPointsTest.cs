﻿using System;
using System.Drawing;
using ginbot.RoboArm;
using ginbot.VisionSystem;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest
{
    [TestClass]
    public class DistenceBetweenPointsTest
    {
        [TestMethod]
        public void DistancePoint2DTest()
        {
            var p1 = new Point2D(2, 5);
            var p2 = new Point2D(5, 9);
            var expDistance = 5d;
            var distance = p1.GetDistance(p2);
            Assert.AreEqual(expDistance, distance);
        }

        [TestMethod]
        public void DistancePoint3DTest()
        {
            var p1 = new Point3D(2, 5, 7);
            var p2 = new Point3D(5, 9, 1);
            var expDistance = 7.81d;
            var distance = Math.Round(p1.GetDistance(p2),2);
            Assert.AreEqual(expDistance, distance);
        }

        [TestMethod]
        public void DistancePointTest()
        {
            var p1 = new Point(2, 5);
            var p2 = new Point(5, 9);
            var expDistance = 5d;
            var distance = p1.GetDistance(p2);
            Assert.AreEqual(expDistance, distance);
        }
    }
}
