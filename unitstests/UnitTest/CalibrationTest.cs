﻿using System;
using System.Collections.Generic;
using System.Drawing;
using ginbot.VisionSystem;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest
{
    [TestClass]
    public class CalibrationTest
    {
        [TestMethod]
        public void ValidateCalibrationPointsTest()
        {
            Calibration calib = new Calibration();
            var p1 = new Point2D(0, 23);
            var p2 = new Point2D(5, 23);
            var p3 = new Point2D(10, 23);

            var isValid = calib.ValidateCalibrationPoints(p1, p2, p3);
            var isValid2 = calib.ValidateCalibrationPoints(p1, p2, new Point2D(10,25));

            Assert.IsTrue(isValid);
            Assert.IsFalse(isValid2);
        }

        [TestMethod]
        public void GetCalibratIONPointSFromDetectedObjectsTest()
        {
            string path = "kalib2st_valid.png";
            ObjectDetector objectDetector = new ObjectDetector();
            objectDetector.MinHeight = 20;
            objectDetector.MinWidth = 20;
            Bitmap bitmap = (Bitmap)Image.FromFile(path);

            List<DetectedObject> detectedObjects = objectDetector.GetDetectedObjects(bitmap);

            Calibration calib = new Calibration();

            var isValid = calib.GetCalibrationPointsFromDetectedObjects(detectedObjects);

            Assert.IsTrue(isValid);


        }

        [TestMethod]
        public void CalibrationFormTest()
        {
            string path = "kalib2st_valid.png";
            ObjectDetector objectDetector = new ObjectDetector();
            objectDetector.MinHeight = 10;
            objectDetector.MinWidth = 10;
            Bitmap bitmap = (Bitmap)Image.FromFile(path);

            List<DetectedObject> detectedObjects = objectDetector.GetDetectedObjects(bitmap);

            Calibration calib = new Calibration();

            var isValid = calib.GetCalibrationPointsFromDetectedObjects(detectedObjects);

            Point detectedpoint2D = new Point();
            foreach (var detectedObject in detectedObjects)
            {
                if (detectedObject.Name!=Shape.Circle)
                {
                    detectedpoint2D = detectedObject.Center;
                }
            }

            CalibrationForm cf = new CalibrationForm(calib, bitmap);
            cf.ShowDialog();
            var realpoint = calib.GetRealPoint(detectedpoint2D);
            Console.WriteLine(detectedpoint2D+"- ("+ realpoint.X+" ; "+ realpoint.Y+")");

            Assert.AreEqual(3, calib.CalibrationPoints.Count);
            Assert.IsTrue(isValid);


        }
        [TestMethod]
        public void CalibrationFormTest2()
        {
            string path = "kalib18st_valid.bmp";
            ObjectDetector objectDetector = new ObjectDetector();
            objectDetector.MinHeight = 10;
            objectDetector.MinWidth = 10;
            Bitmap bitmap = (Bitmap)Image.FromFile(path);

            List<DetectedObject> detectedObjects = objectDetector.GetDetectedObjects(bitmap);

            Calibration calib = new Calibration();

            var isValid = calib.GetCalibrationPointsFromDetectedObjects(detectedObjects);

            Point detectedpoint2D = new Point();
            foreach (var detectedObject in detectedObjects)
            {
                if (detectedObject.Name != Shape.Circle)
                {
                    detectedpoint2D = detectedObject.Center;
                }
            }

            CalibrationForm cf = new CalibrationForm(calib, bitmap);
            cf.ShowDialog();
            var realpoint = calib.GetRealPoint(detectedpoint2D);
            
            Console.WriteLine(detectedpoint2D + "- (" + realpoint.X + " ; " + realpoint.Y + ")");

            Assert.AreEqual(3, calib.CalibrationPoints.Count);
            Assert.IsTrue(isValid);


        }

        [TestMethod]
        public void RealPointToImageTest()
        {
            string path = "kalib18st_valid.bmp";
            ObjectDetector objectDetector = new ObjectDetector();
            objectDetector.MinHeight = 10;
            objectDetector.MinWidth = 10;
            Bitmap bitmap = (Bitmap)Image.FromFile(path);

            List<DetectedObject> detectedObjects = objectDetector.GetDetectedObjects(bitmap);

            Calibration calib = new Calibration();

            var isValid = calib.GetCalibrationPointsFromDetectedObjects(detectedObjects);

            Point detectedpoint2D = new Point();
            foreach (var detectedObject in detectedObjects)
            {
                if (detectedObject.Name != Shape.Circle)
                {
                    detectedpoint2D = detectedObject.Center;
                }
            }

            CalibrationForm cf = new CalibrationForm(calib, bitmap);
            cf.ShowDialog();
            var realPoint = calib.GetRealPoint(detectedpoint2D);
            var imagePoint = calib.RealPointToImage(realPoint.ToPoint());
            ObjectDrawer.DrawText(bitmap, "(" + imagePoint.X + " ; " + imagePoint.Y + ")", 15, detectedpoint2D);
            bitmap.Save("CalibrationFormTest.png");
            Console.WriteLine(detectedpoint2D + "- (" + imagePoint.X + " ; " + imagePoint.Y + ")");

            Assert.AreEqual(3, calib.CalibrationPoints.Count);
            Assert.IsTrue(isValid);


        }

    }
}
