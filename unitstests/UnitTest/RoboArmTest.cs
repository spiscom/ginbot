﻿using System;
using System.IO.Ports;
using ginbot.RoboArm;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest
{
    [TestClass]
    public class RoboArmTest
    {
       
        [TestMethod]
        public void isPositionAllowedTest()
        {
            //Arrange
            
            
            var roboArm = new RoboArm();

            //Act


            //Assert
            Assert.IsTrue(roboArm.IsPositionAllowed(90, 90, -80, -90,45));
            
        }
        [TestMethod]
        public void RoboAnglesTest()
        {
            //Arrange
            RoboArm roboArm = new RoboArm();
            Console.WriteLine(roboArm.basis.BasisAngle.Angle);
            
            var roboAngles = roboArm.roboAngles.Copy();
            Console.WriteLine(roboAngles.BasisAngle.Angle);
            roboAngles.Array[0].Angle = 5;
            Console.WriteLine(roboAngles.BasisAngle.Angle);
  
                roboAngles.Array[0].Angle=15;
 
            Console.WriteLine(roboAngles.Array[0].Angle);
            Console.WriteLine(roboArm.basis.BasisAngle.Angle);
            //Act


            //Assert
            Assert.AreEqual(Arm.RoundToStep(15), roboAngles.BasisAngle.Angle);
            
        }













    }


}
