﻿using System;
using ginbot.RoboArm;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest
{
    [TestClass]
    public class ClampTest
    {
        [TestMethod]
        public void GetWidthTest()
        {
            RoboArm roboArm = new RoboArm();
            double expWidth = Math.Round(28.9877747106096,2);
            double witdh = Math.Round(roboArm.clampArm.clamp.GetWidth(147.15),2);

            Assert.AreEqual(expWidth,witdh);
        }
        

                    [TestMethod]
        public void GetAngleFromWidthTest()
        {
            RoboArm roboArm = new RoboArm();
            double expAngle = 147.15;
            double witdh = Math.Round(28.9877747106096, 2);
            double Angle = Math.Round(roboArm.clampArm.clamp.GetAngleFromWidth(witdh), 2);

            Assert.AreEqual(expAngle, Angle);
        }
    }
}
