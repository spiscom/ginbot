﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UsbLibrary;

namespace ginbot.Connection
{
   
    public class WitUsb: HIDDevice
    {
        
        public WitUsb()
        {
            usbHidPort = new UsbHidPort();

            usbHidPort.ProductId = ProductId;
            usbHidPort.VendorId = VendorId;
            dev = SpecifiedDevice.FindSpecifiedDevice(VendorId, ProductId);
            
        }
        UsbHidPort usbHidPort;
        //product ID
        private const int ProductId = 0x0100;

        private const int VendorId = 0x1920;

        public SpecifiedDevice dev;
    }
}
