﻿using ginbot.VisionSystem;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ginbot.RoboArm
{
    public class Clamp : Arm
    {
        

        /// <summary>
        /// Klasa reprezentuje zacisk robota
        /// </summary>
        /// <param name="Length">Długość w mm przy zamkniętych szczękach</param>
        /// <param name="ZeroPositionAngle">Kąt ramienia względem rodzica przy zerowym położeniu serwa.</param>
        /// <param name="Channel">Kanał kontrolera, do którego podłączone jest serwo podstawy.</param>
        /// <param name="ComPort">Port com, do którego jest podłączony kontroler.</param>
        public Clamp(double ZeroPositionAngle, int Channel) : base(0, ZeroPositionAngle, Channel)
        {

        }

        //dł. elementu, który otwiera chwytak
        double l = 30;
        /// <summary>
        /// Zwraca różnicę (o ile się skraca) długości przy zadanym kącie 
        /// </summary>
        public  double LengthDifference
        { 
            get
            {              
                //różnica długości przy danym otwarciu
                double diff = l - (l * Math.Cos(Geometry.DegreeToRadian(ArmAngle.MaxAngle - ArmAngle.Angle - zeroPositionAngle)));
                return diff;
            }

        }

        /// <summary>
        /// Metoda zwraca szerokość otwarcia szczeęki przy zadanym kącie serwa
        /// </summary>
        /// <param name="_ArmAngle"></param>
        /// <returns></returns>
        public double GetWidth(double _ArmAngle)
        {          
            {
                //szerokość otwarcia szczęk (mnożymy x2, bo otwiera się na dwie strony)
                double width = 2 * (l * Math.Sin(Geometry.DegreeToRadian(ArmAngle.MaxAngle - _ArmAngle - zeroPositionAngle )));
                return width;
            }
        }

        /// <summary>
        /// Metoda oblicza kąt potrzeby do otwarcia zacisku do zadanej szerokości
        /// </summary>
        /// <param name="Width"></param>
        /// <returns></returns>
        public double GetAngleFromWidth(double Width)
        {
            double angle = ArmAngle.MaxAngle- Geometry.RadianToDegree( Math.Asin((Width / 2) / l));
            return angle;
        }


        /// <summary>
        /// Otwiera szczękę do zadanej szerokości
        /// </summary>
        /// <param name="Width">Szerokość otwarcia szczęki</param>
        /// <returns></returns>
        public  bool SetWidth(float Width)
        {
            double angle = GetAngleFromWidth(Width);
            return base.SetPosition(angle);
        }

        /// <summary>
        /// Otwiera szczękę do maksymalnej szerokości
        /// </summary>
        /// <returns></returns>
        public bool Open()
        {
           
            return base.SetPosition(ArmAngle.MinAngle);
        }
        /// <summary>
        /// Zamyka szczękę do minimalnej szerokości
        /// </summary>
        /// <returns></returns>
        public bool Close()
        {

            return base.SetPosition(ArmAngle.MaxAngle);
        }

        public double GetMaxWidth()
        {
            return GetWidth(ArmAngle.MinAngle);
        }

    }
    }
