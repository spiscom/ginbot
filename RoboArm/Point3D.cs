﻿using System;

namespace ginbot.RoboArm
{
    public class Point3D
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        public Point3D()
        {

        }

        public Point3D(double X, double Y, double Z)
        {
            this.X = X;
            this.Y = Y;
            this.Z = Z;
        }
        /// <summary>
        /// Metoda ustawia punkt o podanych współrzędnych;
        /// </summary>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="Z"></param>
        public void Set(double X, double Y, double Z)
        {
            this.X = X;
            this.Y = Y;
            this.Z = Z;
        }

        /// <summary>
        /// Metoda oblicza odległość od punktów 3D;
        /// </summary>
        /// <param name="X">Współrzędna X</param>
        /// <param name="Y">Współrzędna Y</param>
        /// <param name="Z">Współrzędna Z</param>
        /// <returns>Odległość o punktu.</returns>
        public double GetDistance(double X, double Y, double Z)
        {
            return Math.Sqrt(Math.Pow(X - this.X, 2) + Math.Pow(Y - this.Y, 2) + Math.Pow(Z - this.Z, 2));
        }

        /// <summary>
        /// Metoda oblicza odległość od punktów 3D;
        /// </summary>
        /// <param name="point3D">Punkt w przestzreni 3D</param>
        /// <returns>Odległość o punktu.</returns>
        public double GetDistance(Point3D point3D)
        {
            return GetDistance(point3D.X, point3D.Y, point3D.Z);
        }
        /// <summary>
        /// Zwraca pozycję 3Djako string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"({X.ToString("N2")} ; {Y.ToString("N2")} ; {Z.ToString("N2")})";
        }


    }
}