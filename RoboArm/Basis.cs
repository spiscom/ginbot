﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ginbot.RoboArm
{
    public class Basis : Arm
    {

        /// <summary>
        /// Klasa reprezentuje podstawę robota
        /// </summary>
        /// <param name="Length">Wysokość podstawy w mm</param>
        /// <param name="ZeroPositionAngle">Kąt ramienia względem rodzica przy zerowym położeniu serwa.</param>
        /// <param name="Channel">Kanał kontrolera, do którego podłączone jest serwo podstawy.</param>
        /// <param name="ComPort">Port com, do którego jest podłączony kontroler.</param>
        public Basis(int Length,double ZeroPositionAngle, int Channel) : base(Length, ZeroPositionAngle, Channel)
        {
           zeroPositionAngle = ZeroPositionAngle;
        }

        /// <summary>
        /// Kąt obrotu podstawy;
        /// </summary>
        public override RoboJoint BasisAngle
        {
            get
            {
                return ArmAngle;
            }

        }

        /// <summary>
        /// Rzeczywisty bezwzględny kąt wychylenia (do osi X)
        /// </summary>
        public override double AbsoluteAngle
        {
            get
            {                
                return 0;
            }
        }

        /// <summary>
        /// Pozycja 3D
        /// </summary>
        public override Point3D Position3D
        {
            get
            {
                double x = 0;
                double y = 0;
                double z = Length;
                return new Point3D(x, y, z);
            }
        }


    }
}
