﻿using ginbot.General;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ginbot.RoboArm
{
    /// <summary>
    /// Klasa definiuje metody sterujące ruchem serv
    /// </summary>
    public class Servo
    {
        
        public int Channel { get; set; }
        public const double STEP = 0.09;
        const int MAX_SPEED = 20;
        const int MIN_SPEED = 1;
        const double MAX_POSITION = 180;
        const double MIN_POSITION = 0;
        


        /// <summary>
        /// Klasa zawiera metody sterujące ruchem serv
        /// </summary>
        /// <param name="Channel">Numer kanału do którego jest podłączone servo</param>
        public Servo(int Channel)
        {
            
            this.Channel = Channel;
        }


        

        /// <summary>
        /// Ustawia prędkość 
        /// </summary>
        /// <param name="Speed">Liczba z zakresu 1-20</param>
        /// <returns>True, jeśli pomyślnie wysłano do portu com</returns>
        public bool SetSpeed(int Speed, SerialPort ComPort)
        {
            if (Speed < MIN_SPEED || Speed > MAX_SPEED)
                throw new ArgumentException($"{this.ToString()}: Wartość {Speed} z poza dozwolonego zakresu {MIN_SPEED}-{MAX_SPEED}");
            return SendCommand(Cmd.SpeedControl, Speed, ComPort);
        }

        /// <summary>
        /// Ustawia pozycję (kąt) serva
        /// </summary>
        /// <param name="Position">Kąt wychylenia, liczba z zakresu 0-180 stopni</param>
        /// <returns>True, jeśli pomyślnie wysłano do portu com</returns>
        public bool SetPosition(double Position, SerialPort ComPort)
        {
            if (Position < MIN_POSITION || Position > MAX_POSITION)
                throw new ArgumentException($"{this.ToString()}: Wartość {Position} z poza dozwolonego zakresu {MIN_POSITION}-{MAX_POSITION}");
            int steps = AngleToSteps(Position);
            return SendCommand(Cmd.PositionControl, steps, ComPort);
        }

        /// <summary>
        /// Ustawia pozycję (kąt) i prędkość serva
        /// </summary>
        /// <param name="Position">Kąt wychylenia, liczba z zakresu 0-180 stopni</param>
        /// <param name="Speed">Prędkość, liczba z zakresu 1-20 </param>
        /// <returns>True, jeśli pomyślnie wysłano do portu com</returns>
        public bool SetPosition(double Position, int Speed, SerialPort ComPort)
        {
            if (Position < MIN_POSITION || Position > MAX_POSITION)
            {
                throw new ArgumentException($"{this.ToString()}: Wartość {Position} z poza dozwolonego zakresu {MIN_POSITION}-{MAX_POSITION}");
            }
            if (!SetSpeed(Speed, ComPort))
                return false;
            int steps = AngleToSteps(Position);
            return SendCommand(Cmd.PositionControl, steps, ComPort);
        }

        /// <summary>
        /// Awaryjne zatrzymanie
        /// </summary>
        /// <returns>True , jeśli poprawnie wysłano do portu com</returns>
        public bool EmergencyStop(SerialPort ComPort)
        {
            return SendCommand(Cmd.EmergencyStopRecovery, 1, ComPort);
        }

        /// <summary>
        /// Wznowienie pracy
        /// </summary>
        /// <returns>True , jeśli poprawnie wysłano do portu com</returns>
        public bool RecoveryStart(SerialPort ComPort)
        {
            
            return SendCommand(Cmd.EmergencyStopRecovery, 0, ComPort);
        }

        /// <summary>
        /// Metoda przelicza kąt na kroki (steps) serva
        /// </summary>
        /// <param name="Position">Zadany kąt wychylenia 0-180</param>
        /// <returns>int liczba kroków</returns>
        private int AngleToSteps(double Position)
        {
            int steps =(int) Math.Round((Position / STEP) + 500);
            return steps;
        }

        /// <summary>
        /// Wysyła komendy do sterownika
        /// </summary>
        /// <param name="Command">Typ komendy np. 0x001 lub zdefiniowane w klasie Cmd  etc</param>
        /// <param name="Value"></param>
        /// <returns>True , jeśli poprawnie wysłano do portu com</returns>
        public bool SendCommand(byte Command, int Value, SerialPort ComPort)
        {
            
            //TODO przetestować zabezpieczenie przed wyjątkiem
            byte ch = Convert.ToByte(Channel);
            byte[] cmd = new byte[] {Cmd.StartCode, Command, ch, Value.DataL(), Value.DataH()};
            bool exception;
            var logName = "SendCommand";
            do
            {
                try
                {

                    ComPort.Write(cmd, 0, 5);
                    exception = false;
                }
                //wyjątek "zasób jest  użyciu"
                catch (System.IO.IOException e)
                {
                    exception = true;                   
                    Thread.Sleep(200);
                    var msg = $"{e.Source}: {e.Message}";
                    Logger.Log(logName, msg);
                }
                catch (Exception e)
                {
                    var msg = $"{e.Source}: {e.Message}";
                    Logger.Log(logName,msg);
                    throw;
                }
            }
            //gdy występuje wyjątek zasób jest w użyciu próba ponownego wysłania komendy
            while(exception);

            return true;
        }
    }
}
