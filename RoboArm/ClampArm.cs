﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ginbot.RoboArm
{
    public class ClampArm:Arm
    {

        public Wrist wrist;
        public Clamp clamp;
        //SerialPort comPort;
        /// <summary>
        /// Klasa reprezentuje ramię z zaciskiem
        /// </summary>
        /// <param name="Length">Długość w mm</param>
        /// <param name="ZeroPositionAngle">Kąt ramienia względem rodzica przy zerowym położeniu serwa.</param>
        /// <param name="Channel">Kanał kontrolera, do którego podłączone jest serwo podstawy.</param>
        /// <param name="WristChannel">Kanał kontrolera, do którego podłączone jest serwo nadgarstka.</param>
        /// <param name="ClampChannel">Kanał kontrolera, do którego podłączone jest serwo zacisku.</param>
        /// <param name="ComPort">Port com, do którego jest podłączony kontroler.</param>
        public ClampArm(int Length, double ZeroPositionAngle, int Channel,int WristChannel,int ClampChannel   ) : base(Length, ZeroPositionAngle, Channel)
        {
            
            wrist = new Wrist(0, WristChannel);
            clamp = new Clamp(0, ClampChannel);
            wrist.Connection = Connection;
            clamp.Connection = Connection;

        }

        //public override SerialPort ComPort
        //{
        //    get
        //    {
        //        return comPort;
        //    }
        //    set
        //    {
        //        comPort = value;
        //        //wrist.ComPort = comPort;
        //        //clamp.ComPort = comPort;
        //        //base.ComPort = comPort;
        //    }
        //}


        public override Point3D Position3D
        {
            get
            {
                Point3D point3D = new Point3D();
                //obliczenie pozycji z korektą przesiunięcia o 9mm w prawo
                double l = 9;
                if (Parent != null)
                    point3D = Kinematics.GetPosition(Parent.Position3D, l, Parent.AbsoluteAngle, BasisAngle.Angle - 90);
                return Kinematics.GetPosition(point3D, Length, AbsoluteAngle, BasisAngle.Angle);
            }
        }

        public override double Length
        {
            get
            {
                //korekta długości ramienia
                return base.Length - clamp.LengthDifference;              
            }
        }

    }
}
