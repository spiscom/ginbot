﻿namespace ginbot.RoboArm
{
    public class RunFromListProgressChangedEventArgs
    {
        public int Total = 0;
        public int Done = 0;
        
    }
}