﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ginbot.RoboArm
{
    /// <summary>
    /// Klasa zawiera metody odczytu i zapisu listy pozycji do pliku.
    /// </summary>
    public static class PositionListRepo
    {
        /// <summary>
        /// Metaoda zapisuje listę pozycji (Point3D) robota do pliku.
        /// Jeśli plik istnieje zostanie nadpisany, 
        /// jeśli plik nie istnieje zostanie utworzony. 
        /// </summary>
        /// <param name="Point3DList">Lista pozycji (Point3D)</param>
        /// <param name="FilePath">Ścieżka do pliku</param>
        /// <returns></returns>
        public static bool Save(List<Point3D> Point3DList, string FilePath)
        {
            bool status = false;
            string pointsText = String.Empty;
            foreach (var point in Point3DList)
            {
                pointsText += $"{point.X};{point.Y};{point.Z}";
            }
            try
            {
                File.WriteAllText(pointsText, FilePath);
            }
            catch (Exception)
            {
                throw;
            }
            status = true;
            return status;
        }

        /// <summary>
        /// Metoda odczytuje z pliku listę punktów 3D (Point3D)
        /// </summary>
        /// <param name="FilePath">Ścieżka do pliku</param>
        /// <returns>Lista Point3D</returns>
        public static List<Point3D> Open(string FilePath)
        {
            var Point3DList = new List<Point3D>();
            string[] pointsLines;
            try
            {
                pointsLines = File.ReadAllLines(FilePath);
                foreach (var points in pointsLines)
                {
                    var pointArray = points.Split(';');
                    Point3D point3D = new Point3D()
                    {
                        X = Double.Parse(pointArray[0]),
                        Y = Double.Parse(pointArray[1]),
                        Z = Double.Parse(pointArray[2])
                    };
                    Point3DList.Add(point3D);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return Point3DList;
        }


    }
}
