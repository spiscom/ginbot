﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ginbot.RoboArm
{   
    /// <summary>
    /// Klasa przechowuje zestaw kątów ramion robota
    /// </summary>
    public class RoboAngles
    {
        public RoboAngles()
        {
            
            Array = new RoboJoint[6] { new RoboJoint(), new RoboJoint(), new RoboJoint(), new RoboJoint(), new RoboJoint(), new RoboJoint() };
            BasisAngle = new RoboJoint();
            Arm1Angle = new RoboJoint();
            Arm2Angle = new RoboJoint();
            ClampArmAngle = new RoboJoint();
            WristAngle = new RoboJoint();
            ClampAngle = new RoboJoint();
            //this.Add(BasisAngle);
            //this.Add(Arm1Angle);
            //this.Add(Arm2Angle);
            //this.Add(ClampArmAngle);
            //this.Add(new RoboJoint());
            //this.Add(new RoboJoint());
        }
        public RoboJoint BasisAngle 
        { 
            get
            {
                return Array[0];
            }
            set
            {
                Array[0] = value;
            }
        }
        public RoboJoint Arm1Angle
        {
            get
            {
                return Array[1];
            }
            set
            {
                Array[1] = value;
            }
        }
        public RoboJoint Arm2Angle
        {
            get
            {
                return Array[2];
            }
            set
            {
                Array[2] = value;
            }
        }
        public RoboJoint ClampArmAngle
        {
            get
            {
                return Array[3];
            }
            set
            {
                Array[3] = value;
            }
        }
        public RoboJoint WristAngle
        {
            get
            {
                return Array[4];
            }
            set
            {
                Array[4] = value;
            }
        }
        public RoboJoint ClampAngle
        {
            get
            {
                return Array[5];
            }
            set
            {
                Array[5] = value;
            }
        }
        /// <summary>
        /// Wartości kątów w tablicy 
        /// [0]:BasisAngle
        /// [1]:Arm1Angle
        /// [2]:Arm2Angle
        /// [3]:ClampArmAngle
        /// [4]:ClampAngle
        /// [5]:WristAngle
        /// </summary>
        public RoboJoint[] Array;

        /// <summary>
        /// Kopiuje wartości do nowej instancji

        /// </summary>
        /// <returns></returns>
        public RoboAngles Copy()
        {
            RoboAngles roboAngle = new RoboAngles();

            for (int i = 0; i < Array.Length; i++)
            {
                roboAngle.Array[i].Angle = Array[i].Angle;
                roboAngle.Array[i].MinAngle = Array[i].MinAngle;
                roboAngle.Array[i].MaxAngle = Array[i].MaxAngle;
                roboAngle.Array[i].MinMaxValidation = Array[i].MinMaxValidation;
            }
            return roboAngle;
        }
        public override string ToString()
        {
            string angles = "(";
            foreach (var angle in Array)
            {
                angles += angle.Angle.ToString() + ";";
            }
            angles = ")";
            return base.ToString();
        }
    }
}
