﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ginbot.RoboArm
{
    /// <summary>
    /// Klasa zawiera metody odczytu i zapisu listy kątów do pliku.
    /// </summary>
    public class AngleListRepo
    {

        /// <summary>
        /// Metaoda zapisuje listę kątów robota do pliku.
        /// Jeśli plik istnieje zostanie nadpisany, 
        /// jeśli plik nie istnieje zostanie utworzony. 
        /// </summary>
        /// <param name="RoboAnglesList">Lista kątów (RoboAngles)</param>
        /// <param name="FilePath">Ścieżka do pliku</param>
        /// <returns></returns>
        public static bool Save(List<RoboAngles> RoboAnglesList, string FilePath)
        {
            bool status = false;
            string textToFile = String.Empty;
                foreach (var angles in RoboAnglesList)
                {
                textToFile += $"{angles.BasisAngle.Angle};{angles.Arm1Angle.Angle};{angles.Arm2Angle.Angle};{angles.ClampArmAngle.Angle};{angles.WristAngle.Angle};{angles.ClampAngle.Angle}{Environment.NewLine}";
                }
            try
            {
                File.WriteAllText(FilePath, textToFile);
            }
            catch (Exception)
            {
                
                throw;
            }
            status = true;
            return status;
        }
        /// <summary>
        /// Metoda odczytuje z pliku listę kątów (RoboAngles).
        /// </summary>
        /// <param name="FilePath">Ścieżka do pliku.</param>
        /// <returns>Lista RoboAngles.</returns>
        public static List<RoboAngles> Open(string FilePath)
        {
            string[] anglesLines;
            List<RoboAngles> roboAnglesList = new List<RoboAngles>();
            try
            {
                anglesLines = File.ReadAllLines(FilePath);
                foreach (var angles in anglesLines)
                {
                    var anglesArray = angles.Split(';');
                    RoboAngles roboAngles = new RoboAngles();
                    {
                        roboAngles.BasisAngle.Angle = Double.Parse(anglesArray[0]);
                        roboAngles.Arm1Angle.Angle = Double.Parse(anglesArray[1]);
                        roboAngles.Arm2Angle.Angle = Double.Parse(anglesArray[2]);
                        roboAngles.ClampArmAngle.Angle = Double.Parse(anglesArray[3]);
                        roboAngles.WristAngle.Angle = Double.Parse(anglesArray[4]);
                        roboAngles.ClampAngle.Angle = Double.Parse(anglesArray[5]);

                    };
                    roboAnglesList.Add(roboAngles);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return roboAnglesList;
        }
    }
}
