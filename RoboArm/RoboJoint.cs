﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ginbot.RoboArm
{
    public class RoboJoint
    {
        public RoboJoint()
        {

        }
        public RoboJoint(double Angle,double MinAngle, double MaxAngle)
        {
            this.Angle = Angle;
            this.MinAngle = MinAngle;
            this.MaxAngle = MaxAngle;

        }
        private double angle;
        private double minAngle;
        private double maxAngle;
        public bool MinMaxValidation { get; set; } = false;
        public double Angle 
        {
            get
            {
                return angle;
            }
            set
            {
                if (value < MinAngle && MinMaxValidation)
                    angle = MinAngle;
                else if (value > MaxAngle && MinMaxValidation)
                    angle = MaxAngle;
                else
                    angle = Arm.RoundToStep(value);
            }
        } 
        public double MinAngle 
        { 
            get
            {
                return minAngle;
            }
            set
            {
                minAngle=Arm.RoundToStep(value);
            }
        }
        public double MaxAngle 
        {
            get
            {
                return maxAngle;
            }
            set
            {
                maxAngle = Arm.RoundToStep(value);
            }
        }
    }
}
