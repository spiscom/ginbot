﻿using System;

namespace ginbot.RoboArm
{
    public class RoboConnectionStatusChangedEventArgs:EventArgs
    {
        public bool IsConnected { get; set; }
    }
}
