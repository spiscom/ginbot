﻿using ginbot.General;
using System;
using System.Collections.Generic;

using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ginbot.RoboArm
{
    public class RoboArm
    {



        /// <summary>
        /// Klasa reprezentuje rzeczywiste ramię robota  6DOF
        /// </summary>

        public RoboArm()
        {
            Connection = new RoboConnection();
            serialPort = Connection.serialPort;
            basis = new Basis(BASIS_HEIGHT,-8,BASIS_CHANNEL);
            arm1 = new Arm(ARM1_LENGTH, -6.84, ARM1_CHANNEL);
            arm1.ArmAngle.MinAngle = 45;
            arm1.ArmAngle.MaxAngle = 150;
            
            arm2 = new Arm(ARM2_LENGTH, -190.35, ARM2_CHANNEL);
            arm2.ArmAngle.MinAngle = -130;
            arm2.ArmAngle.MaxAngle = -11;
            clampArm = new ClampArm(CLAMP_ARM_LENGTH, -98, CLAMP_ARM_CHANNEL, WRIST_CHANNEL, CLAMP_CHANNEL);
            clampArm.ArmAngle.MinAngle = -90;
            clampArm.clamp.ArmAngle.MaxAngle = 176.04;
            clampArm.clamp.ArmAngle.MinAngle = 115;
            clampArm.clamp.ArmAngle.Angle = clampArm.clamp.ArmAngle.MaxAngle;
            clampArm.wrist.ArmAngle.Angle = 90;
            clampArm.Parent = arm2;
            arm2.Parent = arm1;
            arm1.Parent = basis;

            basis.Connection = Connection;
            arm1.Connection = Connection; 
            arm2.Connection = Connection; 
            clampArm.Connection = Connection; 
            clampArm.clamp.Connection = Connection; 
            clampArm.wrist.Connection = Connection;


            Connection.roboConnectionStatusChangedEvent += Connection_roboConnectionStatusChangedEvent;
        }

        private void Connection_roboConnectionStatusChangedEvent(object sender, RoboConnectionStatusChangedEventArgs e)
        {
            //todo zrobić jako opcja w ustawieniach programu
            //ustawienie domyślej prędkości pracy robota
            if (e.IsConnected)
            {
                SetArmSpeed(2,2,2,2,4,2);
            }

        }
        /// <summary>
        /// Metoda ustawia prędkość ramienia robota
        /// </summary>
        /// <param name="basisSpeed"></param>
        /// <param name="arm1Speed"></param>
        /// <param name="arm2Speed"></param>
        /// <param name="clampArmSpeed"></param>
        /// <param name="clampSpeed"></param>
        /// <param name="wristSpeed"></param>
        private void SetArmSpeed(int basisSpeed,int arm1Speed,int arm2Speed, int clampArmSpeed,int  clampSpeed, int wristSpeed)
        {
            basis.SetSpeed(basisSpeed);
            arm1.SetSpeed(arm1Speed);
            arm2.SetSpeed(arm2Speed);
            clampArm.SetSpeed(clampArmSpeed);
            clampArm.clamp.SetSpeed(clampSpeed);
            clampArm.wrist.SetSpeed(wristSpeed);
        }








        #region zmienne
        public event RunFromListProgressChangedEvent runFromListProgressChangedEvent;
        public Arm basis { get; set; }
        public Arm arm1 { get; set; }
        public Arm arm2 { get; set; }
        public ClampArm clampArm { get; set; }

        const int BASIS_HEIGHT = 97;
        const int ARM1_LENGTH = 105;
        const int ARM2_LENGTH = 150;
        const int CLAMP_ARM_LENGTH = 184;
        const int BASIS_CHANNEL = 0;
        const int ARM1_CHANNEL = 1;
        const int ARM2_CHANNEL = 2;
        const int CLAMP_ARM_CHANNEL = 3;
        const int WRIST_CHANNEL = 4;
        const int CLAMP_CHANNEL = 5;
        public SerialPort serialPort { get; set; }
        public double MIN_X_POSITION { get; set; }= -500;
        public double MAX_X_POSITION { get; set; }= 500;
        public double MIN_Y_POSITION { get; set; }= 0;
        public double MAX_Y_POSITION { get; set; }= 500;
        public double MIN_Z_POSITION { get; set; }= 0;
        public double MAX_Z_POSITION { get; set; } = 450;
        public RoboConnection Connection { get; set; }
        /// <summary>
        /// Zwraca punkt w przestrzeni 3D grze znajduje się końcówka robota.
        /// </summary>
        public Point3D Position3D
        {
            get
            {
                return clampArm.Position3D;//Kinematics.ForwardKinematics(roboAngles);
            }
        }
        #endregion
        
        public RoboAngles roboAngles 
        { 
            get
            {
                RoboAngles roboAngles = new RoboAngles()
                {
                    BasisAngle = basis.ArmAngle,
                    Arm1Angle = arm1.ArmAngle,
                    Arm2Angle = arm2.ArmAngle,
                    ClampArmAngle = clampArm.ArmAngle,
                    ClampAngle = clampArm.clamp.ArmAngle,
                    WristAngle = clampArm.wrist.ArmAngle

                };
                return roboAngles.Copy();
            }
            private set
            {
                roboAngles = value;
            }
        }






        /// <summary>
        /// Ustawia kąty robota;
        /// </summary>
        /// <param name="arm1Angle">Kąt wychylenia ramienia 1 względem rodzica</param>
        /// <param name="arm2Angle">Kąt wychylenia ramienia 2 względem rodzica</param>
        /// <param name="clampArmAngle">Kąt wychylenia chwytaka względem rodzica</param>
        /// <returns></returns>
        public bool SetArmsAngles(double basisAngle, double arm1Angle, double arm2Angle, double clampArmAngle,double wristAngle, double clampAngle)
        {
            if (!Connection.IsConnected)
            {
                MessageBox.Show("Nie połaczono ze sterownikiem", "Uwaga", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }



            //sprawdzenie czy zadana pozycja jest dozwolona
            if (!IsPositionAllowed(basisAngle, arm1Angle, arm2Angle, clampArmAngle, clampAngle))
                return false;

            

            //tworzenie listy kątów z kluczem do sortowania
            List<KeyValuePair<int, double>> anglesList = new List<KeyValuePair<int, double>>
            {
                new KeyValuePair<int, double>(1,arm1Angle),
                new KeyValuePair<int, double>(2,arm2Angle),
                new KeyValuePair<int, double>(3,clampArmAngle),
            };

            ////sortowanie listy od największego kąta aby unikąć zderzenia z podłożem
            //anglesList.Sort(new AngleComparer());

            ////wysyła posortowane kąty do sterownika 
            //foreach (var angle in anglesList)
            //{

            //    switch (angle.Key)
            //    {
            //        case 1:
            //                {
            //                if (!arm1.SetPosition(angle.Value))
            //                    return false;
            //                break;
            //                }
            //        case 2:
            //            {
            //                if (!arm2.SetPosition(angle.Value))
            //                    return false;
            //                break;
            //            }
            //        case 3:
            //            {
            //                if (!clampArm.SetPosition(angle.Value))
            //                    return false;
            //                break;
            //            }

            //    }

            //}
            if (!arm1.SetPosition(arm1Angle))
                                   return false;
            if (!arm2.SetPosition(arm2Angle))
                                   return false;
            if (!clampArm.SetPosition(clampArmAngle))
                                   return false;
                // obrót podstawy
            if (!basis.SetPosition(basisAngle))
                return false;
            if (!clampArm.wrist.SetPosition(wristAngle))
                return false;
            if (!clampArm.clamp.SetPosition(clampAngle))
                return false;
            return true;
        }

        /// <summary>
        /// Ustawia kąty robota;
        /// </summary>
        /// <param name="roboAngles">Zestaw kątów</param>
        /// <returns></returns>
        public bool SetArmsAngles(RoboAngles roboAngles)
        {
            return SetArmsAngles(roboAngles.BasisAngle.Angle, roboAngles.Arm1Angle.Angle, roboAngles.Arm2Angle.Angle, roboAngles.ClampArmAngle.Angle, roboAngles.WristAngle.Angle, roboAngles.ClampAngle.Angle);
        }

        /// <summary>
        /// Sprawdza czy zadane kąty ustawią robota w dozwolonym obszarze.
        /// </summary>
        /// <param name="basisAngle">Kąt obrotu podstawy</param>
        /// <param name="arm1Angle">Kąt wychylenia ramienia 1 względem rodzica</param>
        /// <param name="arm2Angle">Kąt wychylenia ramienia 2 względem rodzica</param>
        /// <param name="clampArmAngle">Kąt wychylenia chwytaka względem rodzica</param>
        /// <param name="clampAngle">Kąt wychylenia/otwarcia szczęki</param>
        /// <returns></returns>
        public bool IsPositionAllowed(double basisAngle, double arm1Angle, double arm2Angle, double clampArmAngle,double clampAngle)
        {
            Point3D point = Kinematics.ForwardKinematics(basisAngle, arm1Angle, arm2Angle, clampArmAngle, clampAngle);
            if (point.X < MIN_X_POSITION || point.X > MAX_X_POSITION)
                return false;
            if (point.Y < MIN_Y_POSITION || point.Y > MAX_Y_POSITION)
                return false;
            if (point.Z < MIN_Z_POSITION || point.Z > MAX_Z_POSITION)
                return false;

            return true;

        }

        /// <summary>
        /// Sprawdza czy zadane kąty ustawią robota w dozwolonym obszarze.
        /// </summary>
        /// <param name="point3D">punkt w przestrzeni</param>
        /// <returns>True, jeśli punkt jest dozwolony, false w przeciwnym razie. </returns>
        public bool IsPositionAllowed(Point3D point3D)
        {
            Point3D point = point3D;
            if (point.X <= MIN_X_POSITION || point.X >= MAX_X_POSITION)
                return false;
            if (point.Y <= MIN_Y_POSITION || point.Y >= MAX_Y_POSITION)
                return false;
            if (point.Z <= MIN_Z_POSITION || point.Z >= MAX_Z_POSITION)
                return false;

            return true;

        }
        /// <summary>
        /// Sprawdza czy zadane kąty ustawią robota w dozwolonym obszarze.
        /// </summary>
        /// <param name="roboAngles">Zestaw kątów robota</param>
        /// <returns></returns>
        public bool IsPositionAllowed(RoboAngles roboAngles)
        {
            return IsPositionAllowed(roboAngles.BasisAngle.Angle, roboAngles.Arm1Angle.Angle, roboAngles.Arm2Angle.Angle, roboAngles.ClampArmAngle.Angle, roboAngles.ClampAngle.Angle);
        }




        /// <summary>
        /// Metoda wysyła do sterownika kąty z listy w osobnym wątku
        /// </summary>
        /// <param name="roboAngles"></param>
        public void Run(List<RoboAngles> roboAngles)
        {
            Thread thread = new Thread(() => { RunFromList(roboAngles); });
            RunningThreads.Threads.Push(thread);
            thread.Start();
            
        }


        /// <summary>
        /// Metoda wysyła do sterownika kąty z listy
        /// </summary>
        /// <param name="roboAngles"></param>
        /// <returns></returns>
        private void RunFromList(List<RoboAngles> roboAngles)
        {
            int i = 0;
            foreach (var angles in roboAngles)
            {
                int time = GetMaxRunTime(angles);
                
                
                SetArmsAngles(angles);
                Thread.Sleep(time);
                i++;
                RunFromListProgressChangedEventArgs args = new RunFromListProgressChangedEventArgs()
                {
                    Total = roboAngles.Count,
                    Done = i
                };
                runFromListProgressChangedEvent?.Invoke(this, args);
            }
            
            
        }
        /// <summary>
        /// Metoda oblicza czas trwania ruchu do zadanych kątów
        /// </summary>
        /// <param name="roboAngles"></param>
        /// <returns>Czas w milisekundach</returns>
        private int GetMaxRunTime(RoboAngles angles)
        {
            double time = 0;
            time = (Math.Abs(angles.BasisAngle.Angle - basis.ArmAngle.Angle) / basis.AngularSpeed > time) ? (Math.Abs(angles.BasisAngle.Angle - basis.ArmAngle.Angle) / basis.AngularSpeed) : time;
            time = (Math.Abs(angles.Arm1Angle.Angle - arm1.ArmAngle.Angle) / arm1.AngularSpeed > time) ? (Math.Abs(angles.Arm1Angle.Angle - arm1.ArmAngle.Angle) / arm1.AngularSpeed) : time;
            time = (Math.Abs(angles.Arm2Angle.Angle - arm2.ArmAngle.Angle) / arm2.AngularSpeed > time) ? (Math.Abs(angles.Arm2Angle.Angle - arm2.ArmAngle.Angle) / arm2.AngularSpeed) : time;
            time = (Math.Abs(angles.ClampArmAngle.Angle - clampArm.ArmAngle.Angle) / clampArm.AngularSpeed > time) ? (Math.Abs(angles.ClampArmAngle.Angle - clampArm.ArmAngle.Angle) / clampArm.AngularSpeed) : time;
            time = (Math.Abs(angles.ClampAngle.Angle - clampArm.clamp.ArmAngle.Angle) / clampArm.clamp.AngularSpeed > time) ? (Math.Abs(angles.ClampAngle.Angle - clampArm.clamp.ArmAngle.Angle) / clampArm.clamp.AngularSpeed) : time;
            time = (Math.Abs(angles.WristAngle.Angle - clampArm.wrist.ArmAngle.Angle) / clampArm.wrist.AngularSpeed > time) ? (Math.Abs(angles.WristAngle.Angle - clampArm.wrist.ArmAngle.Angle) / clampArm.wrist.AngularSpeed) : time;
            Console.WriteLine($"{time}:{basis.ArmAngle}");
            return (int)(time*1000);
        }
    }
}
