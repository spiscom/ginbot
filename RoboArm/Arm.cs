﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ginbot.RoboArm
{
    public  class Arm
    {
        /// <summary>
        /// Klasa opisuje pojedyncze ramię robota.
        /// </summary>
        /// <param name="Length">Długość ramienia w mm</param>
        /// <param name="ZeroPositionAngle">Kąt ramienia względem rodzica przy zerowym położeniu serwa.</param>
        /// <param name="Channel">Kanał kontrolera, do którego podłączone jest serwo ramienia.</param>

        public Arm(double Length, double ZeroPositionAngle, int Channel)
        {
            this.length = Length;
            this.channel = Channel;
           
            zeroPositionAngle = ZeroPositionAngle;
            ArmAngle = new RoboJoint();

            ArmAngle.MinMaxValidation = true;
            ArmAngle.MinAngle = RoundToStep(zeroPositionAngle);
            ArmAngle.MaxAngle = RoundToStep(zeroPositionAngle + 180);
            ArmAngle.Angle = ArmAngle.MinAngle;

            servo = new Servo( channel);
            
        }

        #region zmienne

        
        const double STEP = Servo.STEP;
        public double zeroPositionAngle { get; set; }
        private RoboJoint armAngle;
        /// <summary>
        /// długość ramienia w mm 
        /// </summary>
        double length;
        // kąt wychylenia servo (symulowany)
        double servoPosition = 0;
        //poziom prędkości 1-20
        int speed=1;
        
        

        Arm parent;
        //SerialPort comPort;
        
        private Servo servo;

        private int channel;
        //private double minPosition = 0;
        //private double maxPosition = 180;

        #endregion

        #region właściwości

        public RoboConnection Connection { get; set; }

        //kąt ramienia względem rodzica
        public RoboJoint ArmAngle
        {
            get
            {
                return armAngle;
            }
            set 
            {
                armAngle = value;


            }
        }

        /// <summary>
        /// Kąt obrotu podstawy;
        /// </summary>
        public virtual RoboJoint BasisAngle
        {
            get
            {
                RoboJoint angleY = new RoboJoint();
                if (Parent != null)
                    angleY = Parent.BasisAngle;
                return angleY;
            }
            
        }

        /// <summary>
        /// Pozycja 3D
        /// </summary>
        public virtual Point3D Position3D
        {
            get
            {
                Point3D point3D =new Point3D();
                if (Parent != null)
                    point3D = Parent.Position3D;
                return Kinematics.GetPosition(point3D,length,  AbsoluteAngle, BasisAngle.Angle);
            }
        }



        /// <summary>
        /// Rzeczywisty bezwzględny kąt wychylenia (do podłoża)
        /// </summary>
        public virtual double AbsoluteAngle
        {
            get
            {
                double parentAbsoluteAngle = 0;
                if (Parent != null)
                    parentAbsoluteAngle = parent.AbsoluteAngle;
                return ArmAngle.Angle + parentAbsoluteAngle;
            }              
        }



        public virtual double Length
        {
            get
            {
                return length;
            }               
        }
        /// <summary>
        /// Kąt wychylenia serwa (symulowany) w stopniach, -1 = nieokreślone.;
        /// </summary>
        public double ServoPosition
        {
            get
            {
                return servoPosition;
            }
        }
        /// <summary>
        /// Prędkość wyrażona wartością 1-20;
        /// </summary>
        public int Speed
        {
            get
            {
                return speed;
            }

        }

        /// <summary>
        /// Prędkość kątowa w stopniach/ na sekundę
        /// </summary>
        public double AngularSpeed
        {
            get
            {
                return speed * 100 * STEP;
            }
        }

        public Arm Parent
        {
            get
            {
                return parent;
            }
            set
            {
                parent = value;
            }
        }

        //public SerialPort ComPort { get; set; }

        /// <summary>
        /// Numer kanału kontrolera, do którego zostało podłączone serwo.
        /// </summary>
        public int Channel
        {
            get
            {
                return channel;
            }
            set
            {
                channel = value;
                servo.Channel = channel;
            }
        }

        #endregion

        #region metody


        /// <summary>
        /// Ustawia prędkość serwa.
        /// </summary>
        /// <param name="Speed">Wartość w zakresie 1-20</param>
        /// <returns></returns>
        public bool SetSpeed(int Speed)
        {
            
            if (servo.SetSpeed(Speed, Connection.serialPort))
            {
                speed = Speed;
                return true;
            }
            else return false;            
        }

        /// <summary>
        /// Ustawia pozycję (kąt) ramienia względem rodzica
        /// </summary>
        /// <param name="Position">Kąt wychylenia, uwzgledniając położenie początkowe np. -90 do 90</param>
        /// <returns>True, jeśli pomyślnie wysłano do portu com</returns>
        public virtual bool SetPosition(double Position)
        {

            if (Position < ArmAngle.MinAngle || Position > ArmAngle.MaxAngle)
                 throw new ArgumentOutOfRangeException($"Wartość {Position} spoza dozwoolonego zakresu {ArmAngle.MinAngle }-{ArmAngle.MaxAngle}"); ;
            double pos= ArmAngleToServoPosition(Position, zeroPositionAngle);

            if (servo.SetPosition(pos, Connection.serialPort))
            {
                servoPosition = pos;
                ArmAngle.Angle = Position;
                return true;
            }
            else
                return false;
            
        }
        /// <summary>
        /// Ustawia pozycję (kąt) ramienia i prędkość serva
        /// </summary>
        /// <param name="Position">Kąt wychylenia, liczba z zakresu 0-180 stopni</param>
        /// <param name="Speed">Prędkość, liczba z zakresu 1-20 </param>
        /// <returns>True, jeśli pomyślnie wysłano do portu com</returns>
        public bool SetPosition(double Position, int Speed)
        {
            if (Position < ArmAngle.MinAngle || Position > ArmAngle.MaxAngle)
                return false;
            double pos = ArmAngleToServoPosition(Position, zeroPositionAngle);
            if (servo.SetPosition(pos, Speed, Connection.serialPort))
            {
                speed = Speed;
                servoPosition = pos;
                ArmAngle.Angle = Position;
                return true;
            }
            else
                return false;

        }

        /// <summary>
        /// Przelicza kat ramienia na pozycję serwa ;
        /// </summary>
        /// <param name="position">Żądany kąt ramienia</param>
        /// <param name="zeroPositionAngle">Kąt ramienia, gdy pozycja serwa jest równa 0.</param>
        /// <returns></returns>
        private double ArmAngleToServoPosition(double position, double zeroPositionAngle)
        {
                return position - zeroPositionAngle;
        }

        /// <summary>
        /// Awaryjne zatrzymanie.
        /// </summary>
        /// <returns>True , jeśli poprawnie wysłano do portu com</returns>
        public bool EmergencyStop()
        {
            if (servo.EmergencyStop(Connection.serialPort))
            {
                servoPosition = -1;
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Wznowienie pracy.
        /// </summary>
        /// <returns>True , jeśli poprawnie wysłano do portu com</returns>
        public bool RecoveryStart()
        {

            return servo.RecoveryStart(Connection.serialPort);
        }

        /// <summary>
        /// Zaokrągla wartość do wielokrotności kroku silnika krokowego
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public static double RoundToStep(double Value)
        {
            
            return Math.Round((int)(Value / STEP) * STEP,2);
        }


        #endregion
    }
}
