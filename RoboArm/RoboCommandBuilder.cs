﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ginbot.VisionSystem;

namespace ginbot.RoboArm
{
    public class RoboCommandBuilder
    {
        /// <summary>
        /// Klasa zawiera metody do generowania listy kątów
        /// niezbędnych do przemieszczenia wykrytego objektu
        /// </summary>
        /// <param name="robo"></param>
        /// <param name="calibration"></param>
        public RoboCommandBuilder(RoboArm robo, Calibration calibration)
        {
            _robo = robo;
            _calibration = calibration;
            roboAngles = robo.roboAngles;
        }

        private RoboArm _robo;
        private Calibration _calibration;
        private RoboAngles roboAngles;

        //todo można dodać wykrywanie szerokości lub wysokości objektu
        //wysokość objektu
        private double objectHight = 30;
        //szerokość objektu
        private double objectWitdh = 30;
        //wysokość nad obiektem, na którym operuje ramie robota
        private double above = 100;       
        private Point3D startPosition = new Point3D(0, 150, 0);
        // wysokość, z której 
        private double DestinationHight = 50;
        public List<RoboAngles> Build(List<DetectedObject> detectedOjects,Point2D DestinationPoint)
        {
            var TmpRoboAngles = new RoboAngles();
            List<RoboAngles> RoboAnglesList = new List<RoboAngles>();
            if (detectedOjects == null) return RoboAnglesList;
            foreach (var detectedObject in detectedOjects)
            {
                // pominięcie punktów kalibracyjnych
                if (detectedObject.Name == Shape.Circle) continue;

                //obliczenie współrzędnych położenia objektu
                var objectPosition = _calibration.GetRealPoint(detectedObject.Center);
                // przejście nad punkt startowy
                TmpRoboAngles = AboveStartPosition(startPosition, roboAngles);
                RoboAnglesList.Add(TmpRoboAngles);
                // przejście nad obiekt
                TmpRoboAngles =AboveObject(objectPosition, TmpRoboAngles);
                RoboAnglesList.Add(TmpRoboAngles);
                // otwarcie szczęk
                TmpRoboAngles=OpenClamp(TmpRoboAngles);
                RoboAnglesList.Add(TmpRoboAngles);
                // przejście do pozycji w połowie wysokości obiektu
                TmpRoboAngles=HoldPosition(objectHight, objectPosition, TmpRoboAngles);
                RoboAnglesList.Add(TmpRoboAngles);
                // zamknięcie szczęk
                TmpRoboAngles=CloseClamp(objectWitdh, TmpRoboAngles);
                RoboAnglesList.Add(TmpRoboAngles);
                //przejście na pozycję nad obiektem
                TmpRoboAngles = AboveObject(objectPosition, TmpRoboAngles);
                RoboAnglesList.Add(TmpRoboAngles);
                // przejście na punkt docelowy
                TmpRoboAngles = AboveDestination(DestinationPoint, TmpRoboAngles);
                RoboAnglesList.Add(TmpRoboAngles);

                TmpRoboAngles=OpenClamp(TmpRoboAngles);
                RoboAnglesList.Add(TmpRoboAngles);
                roboAngles = TmpRoboAngles;
            }
            TmpRoboAngles=AboveStartPosition(startPosition, TmpRoboAngles);
            RoboAnglesList.Add(TmpRoboAngles);

            TmpRoboAngles=StartPosition(startPosition, TmpRoboAngles);
            RoboAnglesList.Add(TmpRoboAngles);

            return RoboAnglesList;
        }

        private RoboAngles OpenClamp(RoboAngles roboAngles)
        {
            var tmpRa = roboAngles.Copy();
            var openClampAngle = _robo.clampArm.clamp.ArmAngle.MinAngle;
            tmpRa.ClampAngle.Angle = openClampAngle;
            return tmpRa;
        }

        private RoboAngles AboveObject(Point2D objectPosition,RoboAngles roboAngles)
        {
            var tmpRa = roboAngles.Copy();
            var pointAboveObject = new Point3D(objectPosition.X, objectPosition.Y, above + objectHight);
            return tmpRa = Kinematics.InverseKinematics(pointAboveObject, roboAngles);
        }

        private RoboAngles CloseClamp(double objectWitdh, RoboAngles roboAngles)
        {
            var tmpRa = roboAngles.Copy();
            var holdClampAngle = _robo.clampArm.clamp.GetAngleFromWidth(objectWitdh - 2);
            tmpRa.ClampAngle.Angle = holdClampAngle;
            return tmpRa;
        }

        private RoboAngles HoldPosition(double objectHight,Point2D objectPosition, RoboAngles roboAngles)
        {
            var tmpRa = roboAngles.Copy();
            var holdPosition = new Point3D(objectPosition.X, objectPosition.Y, objectHight / 2+5);
            return tmpRa = Kinematics.InverseKinematics(holdPosition, roboAngles);
        }

        private RoboAngles AboveDestination(Point2D DestinationPosition, RoboAngles roboAngles)
        {
            var tmpRa = roboAngles.Copy();
            var pointAboveDestination = new Point3D(DestinationPosition.X, DestinationPosition.Y, above + DestinationHight);
            return tmpRa = Kinematics.InverseKinematics(pointAboveDestination, roboAngles);
        }

        private RoboAngles StartPosition(Point3D startPosition, RoboAngles roboAngles)
        {
            var tmpRa = roboAngles.Copy();
            return tmpRa = Kinematics.InverseKinematics(startPosition, roboAngles);
        }
        private RoboAngles AboveStartPosition(Point3D startPosition, RoboAngles roboAngles)
        {
            var tmpRa = roboAngles.Copy();
            var pointAboveStartPosition = new Point3D(startPosition.X, startPosition.Y, above + startPosition.Z);
            return tmpRa = Kinematics.InverseKinematics(pointAboveStartPosition, roboAngles);
        }
    }
}
