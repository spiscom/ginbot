﻿using ginbot.General;
using ginbot.VisionSystem;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ginbot.RoboArm
{
    public class Kinematics
    {
        /// <summary>
        /// Metoda oblicza pozycję ramienia.
        /// </summary>
        /// <param name="parentPosition3D">Pozycja ramienia rodzica.</param>
        /// <param name="armLength">Długość ramienia.</param>
        /// <param name="absoluteAngle">Całkowity kąt wychylenia (suma katów wychyleń wszystkich ramion rodziców). </param>
        /// <param name="basisAngle">Całkowity kąt wychylenia (obrotu) podstawy.</param>
        /// <returns>Punkt w przestrzeni 3D.</returns>
        public static Point3D GetPosition(Point3D parentPosition3D, double armLength, double absoluteAngle, double basisAngle)
        {

            
            
            //oś do przodu robota
            double y = armLength * Math.Cos(Geometry.DegreeToRadian(absoluteAngle)) * Math.Sin(Geometry.DegreeToRadian(basisAngle));
            //oś do góry
            double z = armLength * Math.Sin(Geometry.DegreeToRadian(absoluteAngle));
            //oś prostopadała do pozostałych
            double x = armLength * Math.Cos(Geometry.DegreeToRadian(absoluteAngle)) * Math.Cos(Geometry.DegreeToRadian(basisAngle));

            //dodanie wektora położenia rodzica
            x += parentPosition3D.X;
            y += parentPosition3D.Y;
            z += parentPosition3D.Z;

            return new Point3D(x, y, z);
        }


        /// <summary>
        /// Oblicza położenie "końcówki" robota
        /// </summary>
        /// <param name="basisAngle">Kąt obrotu podstawy</param>
        /// <param name="arm1Angle">Kąt wychylenia ramienia 1 względem rodzica</param>
        /// <param name="arm2Angle">Kąt wychylenia ramienia 2 względem rodzica</param>
        /// <param name="clampArmAngle">Kąt wychylenia ramienia chwytaka względem rodzica</param>
        /// <param name="clampAngle">Kąt wychylenia/otwarcia szczęki</param>
        /// <returns></returns>
        public static Point3D ForwardKinematics(double basisAngle, double arm1Angle, double arm2Angle, 
                                                                double clampArmAngle, double clampAngle)
        {
            //tworzymy tymczasowy obiekt RoboArm
            var tmpRoboArm = new RoboArm();

            //podstawiamy wartości kątów poszczególnych ramion
            tmpRoboArm.basis.ArmAngle.Angle = basisAngle;
            tmpRoboArm.arm1.ArmAngle.Angle = arm1Angle;
            tmpRoboArm.arm2.ArmAngle.Angle = arm2Angle;
            tmpRoboArm.clampArm.ArmAngle.Angle = clampArmAngle;
            tmpRoboArm.clampArm.clamp.ArmAngle.Angle = clampAngle;


            return tmpRoboArm.clampArm.Position3D;
        }

        /// <summary>
        /// Oblicza położenie końcówki robota
        /// </summary>
        /// <param name="roboAngles"></param>
        /// <returns></returns>
        public static Point3D ForwardKinematics(RoboAngles roboAngles)
        {
            return ForwardKinematics(roboAngles.BasisAngle.Angle, roboAngles.Arm1Angle.Angle, roboAngles.Arm2Angle.Angle, roboAngles.ClampArmAngle.Angle, roboAngles.ClampAngle.Angle);
        }

        /// <summary>
        /// Metoda wyznacza kąty ramion niezbędne do osiągnięcia punktu celu.
        /// </summary>
        /// <param name="target">Punkt celu</param>
        /// <param name="roboAngles">Kąty ramion robota</param>
        /// <returns>Wyznaczone  kąty ramion robota</returns>
        static public RoboAngles InverseKinematics(Point3D target,RoboAngles roboAngles)
        {
            var _roboAngles = roboAngles.Copy();

            //zmienne do warunków zatrzymania
            double min = double.MaxValue;
            bool next = true;           
            RoboAngles roboAnglesMin= new RoboAngles();
            int iterationCount = 0;

            //ustawienie kątów w połowie ich dozwolonego zakresu
            for (int i = 0; i < 4; i++)
            {
                _roboAngles.Array[i].Angle = _roboAngles.Array[i].MinAngle + 
                 (_roboAngles.Array[i].MaxAngle - _roboAngles.Array[i].MinAngle) / 2;
            }

            double LearningRate = Servo.STEP;
            int k = 1;
            
            // iteracja do póki nie osiągniemy minimalnej odległości do punktu docelowego
            do
            {
                for (int i = 0; i < 4; i++)
                {
                    // obliczamy odległość do punktu docelowego przed zmianą kąta
                    var distance = Kinematics.ForwardKinematics(_roboAngles).GetDistance(target);

                    // zmienna przechowująca wartość próbkowania
                    double SamplingDistance;

                    // określenie wartości próbkowania
                    if (distance > 5) SamplingDistance = LearningRate * 10;
                    else
                        SamplingDistance = LearningRate;                                       // 

                    //obliczenie gradientu dla wartości próbkowania
                    double gradient = Gradient(target, SamplingDistance, _roboAngles, i);

                    // obliczenie kroku (nadanie kierunku)
                    double step = Arm.RoundToStep(SamplingDistance * gradient);

                    // zmiana kata i-tego ramienia
                    _roboAngles.Array[i].Angle -= step;

                    // obliczenie pozycji końcówki robota po zmianie kąta
                    var newPoint = Kinematics.ForwardKinematics(_roboAngles);

                    //obliczenie odległości do punktu docelowego po zmianie kąta
                    double newDistance  = newPoint.GetDistance(target);

                    //warunek zatrzymania - algorytm kluczy wokól zdanego punktu, 
                    //to wybieramy rozwiązanie minimalne                    
                    if (min > newDistance)
                    {
                        min = newDistance;
                        roboAnglesMin = _roboAngles.Copy();
                        iterationCount = k;
                    }
                    else if (k-iterationCount>3)
                    {
                        next = false;
                        _roboAngles = roboAnglesMin;  
                        break;
                    }
                }
                k++;               
            } while (k<500 && next);
            
           //zwrócenie wartości wyznaczonych kątów
            return _roboAngles;
        }

        /// <summary>
        /// Metoda oblicza gradient dla i-tego ramienia
        /// </summary>
        /// <param name="target">Punkt docelowy</param>
        /// <param name="SamplingDistance">Wartość próbkowania</param>
        /// <param name="angles">Obecne położenie ramion (kąty)</param>
        /// <param name="i"> Numer połączenia</param>
        /// <returns></returns>
        static public double Gradient(Point3D target,double SamplingDistance, RoboAngles angles, int i)
        {            
            // zapisuje kąt, później zostanie przywrócony         
            double angle = angles.Array[i].Angle;

            // Gradient : [F(x+h) - F(x)] / h
            double f_x = Kinematics.ForwardKinematics(angles).GetDistance(target);

            angles.Array[i].Angle += SamplingDistance;
            double f_x_plus_h = Kinematics.ForwardKinematics(angles).GetDistance(target);

            double gradient = (f_x_plus_h - f_x) / SamplingDistance;

            // przywracany kąt
            angles.Array[i].Angle = angle;
            //zwracamy tylko kierunek lub zero
            if (gradient < 0) gradient = -1;
            if (gradient > 0) gradient = 1;

            return gradient;
        }



    }
}
