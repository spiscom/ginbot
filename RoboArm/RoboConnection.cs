﻿using ginbot.General;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace ginbot.RoboArm
{
    //Klasa reprezentuje połączenie z kontrolerem robota
    public class RoboConnection
    {
        public int TimeOut { get; set; } = 3000;
        private int autoReconnectTime =3000;
        private int autoConnectTimerElapsed = 0;
        private int countDown = 0;
        private System.Timers.Timer timer;
        private bool isConnected = false;
        private bool auto = true;
        private System.Timers.Timer autoConnectTimer;


        /// <summary>
        /// Zdarzenie wyzwalane przy zmianie statusu połaczenia
        /// </summary>
        public event RoboConnectionStatusChangedEvent roboConnectionStatusChangedEvent;
        public SerialPort serialPort { get; set; }


        /// <summary>
        /// tworzy instancję klasy RoboConnection
        /// </summary>
        public RoboConnection()
        {
            
            serialPort = new SerialPort();
            serialPort.BaudRate = 9600;
            serialPort.DtrEnable = true;
            serialPort.ReceivedBytesThreshold = 5;
            

            timer =new System.Timers.Timer(1000);
            timer.Elapsed += Timer_Elapsed;
            serialPort.DataReceived += new SerialDataReceivedEventHandler(SerialPort_DataReceived);
            serialPort.ErrorReceived += SerialPort_ErrorReceived;
            autoConnectTimer = new System.Timers.Timer(1000);
            autoConnectTimer.Elapsed += AutoConnectTimer_Elapsed;
        }
        /// <summary>
        /// Jeśli wartość auto=true co autoReconnectTime jest uruchamiana metoda AutoConnection w nowym wątku
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutoConnectTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            
            autoConnectTimerElapsed += 1000;
            if (auto && !isConnected && autoReconnectTime>= autoConnectTimerElapsed)
            {               
                AutoConnection();
                autoConnectTimerElapsed = 0;
            }
        }

        private void SerialPort_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            MessageBox.Show(e.EventType.ToString());
        }

        /// <summary>
        /// Zdarzenie wywołane pojawieniem się odpowiedzi na porcie
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            
            // jeżeli w buforze jest mniej niż 5 bajtów nie robimy nic
            if (serialPort.BytesToRead < 5)
                return;
            // odczyt bufora
            byte[] b2 = ReadAnswer();
            try
            {
                //porównanie bufora z prawidłową odpowiedzią sterownika na ping
                if (CompareBytes(Cmd.Answer, b2))
                {
                    IsConnected = true;
                    Thread.Sleep(1000);
                    SendPing();

                }
                // jesli odpowiedz nieprawidłowa połaczenie przerwane
                else
                {
                    IsConnected = false;
                    timer.Stop();
                    serialPort.Close();

                }
            }
            catch { }

            //wyzerowanie odliczania
            countDown = 0;
        }

        /// <summary>
        /// Sprawdzanie czy czas na odpowiedz nie przekroczy wartości TimeOut
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            countDown+=1000;
            if (countDown >= TimeOut)
            {
                Close();                
                countDown = 0;
            }
            
        }

        //Zwraca true gdy połaczenie jest zestawione, false w przeciwnym razie
        public bool IsConnected 
        { 
            get
            {
                return isConnected;
            }
            set
            {
                if (value!=isConnected)
                {
                    isConnected = value;
                    RoboConnectionStatusChangedEventArgs args = new RoboConnectionStatusChangedEventArgs()
                    {
                        IsConnected = isConnected

                    };
                    string msg;
                    if (isConnected)
                        msg = $"Połączono {serialPort.PortName}";
                    else
                        msg = $"Rozłączono {serialPort.PortName}";
                    Logger.Log("RoboConnection", msg);
                    roboConnectionStatusChangedEvent(this, args);
                }
                

                
            }
        }
       
        //TODO przenieść do klasy servo i zrobić kolejkowanie wysyłania
        /// <summary>
        /// Wysyła ping do sterownika
        /// </summary>
        /// <returns>Zwraca true gdy powiodło się, false w przeciwnym razie</returns>
        private bool SendPing()
        {
            if (!serialPort.IsOpen)
                return false;
            bool exception;
            do
            {
                try
                {
                    serialPort.Write(Cmd.Ping, 0, 5);
                    exception = false;
                }
                //wyjątek "zasób jest  użyciu"
                catch (System.IO.IOException e)
                {
                    exception = true;
                    Thread.Sleep(100);
                    var msg = $"{e.Source}: {e.Message}";
                    Logger.Log("SendPing",msg);
                }
                catch
                {
                    return false;

                }
            }
            while (exception);
            return true;
        }
        /// <summary>
        /// Porównuje identyczność dwóch macierzy bajtów
        /// </summary>
        /// <param name="b1">Pierwszy byte do porównania</param>
        /// <param name="b2">Drugi byte do porównania</param>
        /// <returns></returns>
        private bool CompareBytes(byte[] b1, byte[] b2)
        {            
            return b1.SequenceEqual<byte>(b2);
        }

        /// <summary>
        /// Inicjuje  połączenie ze sterownikiem
        /// </summary>
        public void Open()
        {
            if (isConnected)
                return;

            try
            {
                serialPort.Open();
                serialPort.RtsEnable = true;
                SendPing();
                timer.Start();
            }
            catch (Exception)
            {

                //throw;
            }
            
        }

        /// <summary>
        /// Zamyka połaczenie ze sterownikiem
        /// </summary>
        public void Close()
        {
            if (!isConnected)
                return;

            IsConnected = false;
            timer.Stop();
            serialPort.Close();
            
        }
        /// <summary>
        /// Metoda zatrzymuje automatyczne połaczenie do sterownika
        /// </summary>
        public void StopAutoConnect()
        {
            autoConnectTimer.Stop();
            auto = false;
            
            Close();
        }

        //odczytuje 5 bajtów z bufora portu szeregowego
        private byte[] ReadAnswer()
        {
            int intBuffer;
            
            
            byte[] byteBuffer;
            try
            {
                intBuffer = 5;
                byteBuffer = new byte[intBuffer];
                serialPort.Read(byteBuffer, 0, intBuffer);
            }
            catch
            {
                throw new Exception($"Nie można czytać z portu {serialPort.PortName}");
            }

            return byteBuffer;

        }
        /// <summary>
        /// Metoda startuje timer, który uruchamia metodę AutoConnection() w nowym wątku
        /// </summary>
        public void AutoConnect()
        {
            auto = true;
            autoConnectTimer.Start();

        }
        
        /// <summary>
        /// Metoda iteruje po wszystkich portach i inicjuje połaczenie do sterownika
        /// </summary>
        private void AutoConnection()
        {

            var ports = SerialPort.GetPortNames();
                foreach (var PortName in ports)
                {
                    if (isConnected) continue;

                    serialPort.Close();
                    serialPort.PortName = PortName;

                    Open();

                Thread.Sleep(TimeOut);


            }
            



        }

    }
}
