﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ginbot.RoboArm
{
    public class Wrist:Arm
    {
        /// <summary>
        /// Klasa reprezentuje obrotowy nadgarstek zacisku robota
        /// </summary>
        /// <param name="Length">Długość w mm</param>
        /// <param name="ZeroPositionAngle">Kąt ramienia względem rodzica przy zerowym położeniu serwa.</param>
        /// <param name="Channel">Kanał kontrolera, do którego podłączone jest serwo podstawy.</param>
        /// <param name="ComPort">Port com, do którego jest podłączony kontroler.</param>
        public Wrist(double ZeroPositionAngle, int Channel) : base(0, ZeroPositionAngle, Channel)
        {

        }
    }
}
