﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibUsbDotNet;
using LibUsbDotNet.LibUsb;
using LibUsbDotNet.Main;
using UsbLibrary;


namespace ginbot.RoboArm
{
    public class Usb
    {
        //Produkt ID
        private const int ProductId = 0x0100;

        //Vendor ID
        private const int VendorId = 0x1920;
        public UsbEndpointWriter writeEndpoint;
        public UsbEndpointReader readEnpoint;
        public UsbDevice usbDevice;

        public void Open()
        {
            
                UsbDeviceFinder selectedDevice = new UsbDeviceFinder(VendorId, ProductId);


            if (selectedDevice != null)
            {
                usbDevice = UsbDevice.OpenUsbDevice(selectedDevice);
                // If this is a "whole" usb device (libusb-win32, linux libusb-1.0)
                // it exposes an IUsbDevice interface. If not (WinUSB) the 
                // 'wholeUsbDevice' variable will be null indicating this is 
                // an interface of a device; it does not require or support 
                // configuration and interface selection.
                IUsbDevice wholeUsbDevice = usbDevice as IUsbDevice;
                if (!ReferenceEquals(wholeUsbDevice, null))
                {
                    // This is a "whole" USB device. Before it can be used, 
                    // the desired configuration and interface must be selected.

                    // Select config #1
                    wholeUsbDevice.SetConfiguration(1);

                    // Claim interface #0.
                    wholeUsbDevice.ClaimInterface(0);
                }
            }
            else
                new Exception("Nie odnaleziono portu usb");

            //Open up the endpoints
             writeEndpoint = usbDevice.OpenEndpointWriter(WriteEndpointID.Ep01);
             readEnpoint = usbDevice.OpenEndpointReader(ReadEndpointID.Ep02);
             readEnpoint.DataReceivedEnabled = true;


        }
        public int Write(byte[] buffer,int TimeOut)
        {
            //Write  bytes
            writeEndpoint.Write(buffer, TimeOut, out var bytesWritten);
            return bytesWritten;
        }

        public byte[] Read(byte[] buffer, int TimeOut)
        {
            var readBuffer = new byte[64];
            //Write  bytes
            readEnpoint.Read(readBuffer, TimeOut, out var readBytes);
            return readBuffer;
        }

        public void Close()
        {
            readEnpoint.DataReceivedEnabled = false;
            usbDevice.Close();
        }
    }
}
