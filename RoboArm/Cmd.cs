﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ginbot.RoboArm
{
    /// <summary>
    /// Klasa zawiera definicje zmiennych typu instrukcji
    /// wysyłanych do kontrolera
    /// </summary>
    static public class Cmd
    {
        public static byte StartCode = 0xff;
        public static byte SpeedControl = 0x01;
        public static byte PositionControl = 0x02; 
        public static byte ActionGroupStart = 0x09;
        public static byte EmergencyStopRecovery = 0x0b;
        public static byte[] Ping = { 0xFF, 0x00, 0x12, 0x00, 0x00 };
        public static byte[] Answer = { 0xFF, 0xF0, 0x12, 0x00, 0x00 };


        /// <summary>
        /// Metoda zwraca mniej znaczący byte z int
        /// </summary>
        /// <param name="Data">liczba int</param>
        /// <returns>zwraca mniej znaczący byte</returns>
        static public byte DataL(this int Data)
        {
            byte DL = (byte)(Data & 0xFF);
            return DL;
        }

        /// <summary>
        /// Metoda zwraca bardziej znaczący byte z int
        /// </summary>
        /// <param name="Data">liczba int</param>
        /// <returns>zwraca bardziej znaczący byte</returns>
        static public byte DataH(this int Data)
        {
            byte DH = (byte)(Data >> 8);
            return DH;
        }

        static public byte[] UsbPing()
        {
            byte[] usbPing = new byte[64];
            byte[] ping = { 0x05, 0x03, 0xff, 0x00, 0x12 };
        System.Buffer.BlockCopy(ping, 0, usbPing, 0, ping.Length);
            
            return usbPing;
    }
    }


}
