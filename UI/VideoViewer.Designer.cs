﻿namespace ginbot.UI
{
    partial class VideoViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pbViewer = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCameraProperty = new System.Windows.Forms.Button();
            this.btn_Calibration = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbViewer)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1267, 703);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.pbViewer);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 106);
            this.panel3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(997, 597);
            this.panel3.TabIndex = 1;
            // 
            // pbViewer
            // 
            this.pbViewer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbViewer.Location = new System.Drawing.Point(0, 0);
            this.pbViewer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pbViewer.Name = "pbViewer";
            this.pbViewer.Size = new System.Drawing.Size(997, 597);
            this.pbViewer.TabIndex = 0;
            this.pbViewer.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btn_Calibration);
            this.panel2.Controls.Add(this.btnCameraProperty);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1267, 106);
            this.panel2.TabIndex = 0;
            // 
            // btnCameraProperty
            // 
            this.btnCameraProperty.Location = new System.Drawing.Point(39, 26);
            this.btnCameraProperty.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCameraProperty.Name = "btnCameraProperty";
            this.btnCameraProperty.Size = new System.Drawing.Size(100, 28);
            this.btnCameraProperty.TabIndex = 0;
            this.btnCameraProperty.Text = "Settings";
            this.btnCameraProperty.UseVisualStyleBackColor = true;
            this.btnCameraProperty.Click += new System.EventHandler(this.btnCameraProperty_Click);
            // 
            // btn_Calibration
            // 
            this.btn_Calibration.Location = new System.Drawing.Point(181, 26);
            this.btn_Calibration.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Calibration.Name = "btn_Calibration";
            this.btn_Calibration.Size = new System.Drawing.Size(100, 28);
            this.btn_Calibration.TabIndex = 1;
            this.btn_Calibration.Text = "Kalibracja";
            this.btn_Calibration.UseVisualStyleBackColor = true;
            this.btn_Calibration.Click += new System.EventHandler(this.btn_Calibration_Click);
            // 
            // VideoViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1267, 703);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "VideoViewer";
            this.Text = "VideoViewer";
            this.Load += new System.EventHandler(this.VideoViewer_Load);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbViewer)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pbViewer;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnCameraProperty;
        private System.Windows.Forms.Button btn_Calibration;
    }
}