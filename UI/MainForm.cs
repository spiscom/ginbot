﻿using ginbot.VisionSystem;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ginbot.RoboArm;
using ginbot.General;

namespace ginbot.UI
{
    public partial class MainForm : Form
    {
        VisionSystem.VisionSystem vs;
        RoboArm.RoboArm roboArm;
        bool IsAutoOn = false;
        int timeBetweenAutoRun = 5; //seconds
        int timeElapsed = -1;
        bool IsRoboTaskRunning = false;
        
        public MainForm()
        {
            InitializeComponent();
            vs = new VisionSystem.VisionSystem();
            vs.NewFrameEvent += Vs_NewFrameEvent;
            vs.calibrationProgressChangedEvent += Vs_calibrationProgressChangedEvent;

            roboArm = new RoboArm.RoboArm();
            roboArm.Connection.roboConnectionStatusChangedEvent += Connection_roboConnectionStatusChangedEvent;
            roboArm.Connection.AutoConnect();
            roboArm.runFromListProgressChangedEvent += RoboArm_runFromListProgressChangedEvent;
        }



        private void Connection_roboConnectionStatusChangedEvent(object sender, RoboConnectionStatusChangedEventArgs e)
        {
            ConnectionStatusLabelUpdate(IsConnecdted:e.IsConnected);
        }

        private void ConnectionStatusLabelUpdate(bool IsConnecdted)
        {
            Color colorConneccted = Color.LightGreen;
            Color colorDisConnected = Color.Yellow;
            var textConnected = $"Połączony: {roboArm.Connection.serialPort.PortName}";
            var textDisConnected = "Nie połączony";

            if (IsConnecdted)
            {
                if (statusStrip.InvokeRequired)
                {
                    statusStrip.Invoke(new Action(() => lbConnectionStatus.Text = textConnected));
                    statusStrip.Invoke(new Action(() => lbConnectionStatus.BackColor = colorConneccted));
                }
                else
                {
                    lbConnectionStatus.Text = textConnected;
                    lbConnectionStatus.BackColor = colorConneccted;
                }
            }
            else
            {
                if (statusStrip.InvokeRequired)
                {
                    statusStrip.Invoke(new Action(() => lbConnectionStatus.Text = textDisConnected));
                    statusStrip.Invoke(new Action(() => lbConnectionStatus.BackColor = colorDisConnected));
                }
                else
                {
                    lbConnectionStatus.Text = textDisConnected;
                    lbConnectionStatus.BackColor = colorDisConnected;
                }
            }

        }

        private void Vs_calibrationProgressChangedEvent(object sender, CalibrationProgressChangedEventArgs args)
        {

            
            progressBarUpdate(args.ProgressPercentage,sender as string);

            
        }

        private void progressBarUpdate(int progressPercentage,string label="")
        {
            
            if (statusStrip.InvokeRequired)
            {
                statusStrip.Invoke(new Action(() => progressBar.Value = progressPercentage));
                statusStrip.Invoke(new Action(() => statusLabel.Text = label));
            }
            else
            {
                progressBar.Value = progressPercentage;
                statusLabel.Text = label;
            }
        }

        private void Vs_NewFrameEvent(object sender, VisionSystemNewFrameEventArgs e)
        {
            var bitmap = (Bitmap)e.NewFrame.Clone();
            if (vs.calibration.IsCalibrated)
            {
                foreach (var detectedObject in e.DetectedObjects)
                {
                    var realPosition = vs.calibration.GetRealPoint(detectedObject.Center);
                    var textPosition = $"{realPosition.X.ToString("N2")} ; {realPosition.Y.ToString("N2")}";
                    ObjectDrawer.DrawText(bitmap, textPosition, 15, detectedObject.Center);
                    ObjectDrawer.DrawArm(bitmap, vs.calibration, roboArm.clampArm);
                }
            }
            pictureBox.BackgroundImage = bitmap;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            ButtonUpdate(btnAutomat, IsAutoOn);
        }

        private void btnCamera_Click(object sender, EventArgs e)
        {
            vs.SelectVideoDevice();
            vs.Start();
        }

        private void btnVideoSettings_Click(object sender, EventArgs e)
        {
            vs.OpenPropertyWindow();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            RunningThreads.StopAllThreads();
            vs.Stop();
            roboArm.Connection.Close();
        }

        private void btnCalibration_Click(object sender, EventArgs e)
        {
            vs.StartCalibration();
        }

        private void btnRoboControls_Click(object sender, EventArgs e)
        {
            RoboControls roboControls = new RoboControls(roboArm);
            roboControls.Show();
        }

        private void btnAutomat_Click(object sender, EventArgs e)
        {
            if (vs.calibration.IsCalibrated)
            {
                IsAutoOn = !IsAutoOn;
                ButtonUpdate(sender as Button, IsAutoOn);

                    autoTimer.Enabled= IsAutoOn;

            }
            timeElapsed = 0;
        }

        private void ButtonUpdate(Button btn, bool state)
        {
            if (state)
            {
                btn.BackColor = Color.LightGreen;
            }
            else
            {
                btn.BackColor = Color.LightPink;
            }
        }

        private void autoTimer_Tick(object sender, EventArgs e)
        {
            if (IsRoboTaskRunning) return;
            timeElapsed++;
            var percentage = General.Percentage.Progress(timeElapsed, timeBetweenAutoRun);
            progressBarUpdate(percentage, "Następne zadanie za:");
            
            
            
            if (timeElapsed>=timeBetweenAutoRun)
            {
                DetectedObjectFilter df = new DetectedObjectFilter();
                df.OutFilter = new string[] { Shape.Circle, Shape.Unknown };
                var detectedobjects = df.GetFilteredObjects(vs.DetectedObjects);
                if (detectedobjects.Count>0)
                {
                    IsRoboTaskRunning = true;
                    timeElapsed = -1;
                    AutoTaskRun(vs.DetectedObjects, vs.calibration, roboArm);
                }
                else
                {
                    timeElapsed = -1;
                }
                
            }
        }
        private void RoboArm_runFromListProgressChangedEvent(object sender, RunFromListProgressChangedEventArgs e)
        {
            int percentage = General.Percentage.Progress(e.Done, e.Total);
            progressBarUpdate(percentage, "Praca robota");
            if (percentage==100)
                IsRoboTaskRunning = false;
        }

        private void AutoTaskRun(List<DetectedObject> DetectedObjects,Calibration calibration, RoboArm.RoboArm _roboArm)
        {
            RoboCommandBuilder roboCommandBuilder = new RoboCommandBuilder(_roboArm, calibration);
            var destPoint = new Point2D(380, 70);
            var logName = "Nowe Zadanie";
            var obiects = "";
            foreach (var item in DetectedObjects)
            {
                obiects += item.ToString();
            }
            Logger.Log(logName, obiects);
            var AngleList = roboCommandBuilder.Build(DetectedObjects, destPoint);
            AngleListRepo.Save(AngleList, "auto.csv");
            roboArm.Run(AngleList);
        }



        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            
            this.Cursor = new Cursor(Cursor.Current.Handle);
            if (Cursor.Position.Y < this.Size.Height && Cursor.Position.Y > this.Size.Height - flowLayoutPanel.Size.Height)
            {

                flowLayoutPanel.Visible = true;
                
            }
            else
            {
                
                    flowLayoutPanel.Visible = false;
            }
        }

        private void btnTakeBackground_Click(object sender, EventArgs e)
        {
           
        }
    }
}
