﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ginbot.RoboArm;
using ginbot.VisionSystem;
using UsbLibrary;

namespace ginbot.UI
{
    public partial class RoboControls : Form
    {
        RoboAngles roboAngles;
        RoboArm.RoboArm roboArm;
        
        bool isLoaded = false;
        //SerialPort serialPort = new SerialPort("COM4");
        List<RoboAngles> RoboAnglesList = new List<RoboAngles>();
        
        public RoboControls(RoboArm.RoboArm _roboArm)
        {
            InitializeComponent();

            this.roboArm = _roboArm;
            //roboArm.Connection = new RoboConnection();
            roboArm.Connection.roboConnectionStatusChangedEvent += RoboConnection_roboConnectionStatusChangedEvent;
            roboArm.Connection.AutoConnect();
            roboArm.runFromListProgressChangedEvent += RoboArm_runFromListProgressChangedEvent;




        }



        private void RoboArm_runFromListProgressChangedEvent(object sender, RunFromListProgressChangedEventArgs e)
        {
            if (pB_RunFromList.InvokeRequired)
            {
                pB_RunFromList.Invoke(new Action(() => pB_RunFromList.Maximum = e.Total));
                pB_RunFromList.Invoke(new Action(() => pB_RunFromList.Value = e.Done));
            }
            else
            {
                pB_RunFromList.Maximum = e.Total;
                pB_RunFromList.Value = e.Done;
            }
            
        }

        // Zmiana opisu statusu, gdy zmieni się status połączenia
        private void RoboConnection_roboConnectionStatusChangedEvent(object sender, RoboConnectionStatusChangedEventArgs e)
        {
            ConnectionStatusLabelUpdate(e.IsConnected);

        }


        private void ConnectionStatusLabelUpdate(bool IsConnecdted)
        {
            Color colorConneccted = Color.LightGreen;
            Color colorDisConnected = Color.Yellow;
            var textConnected = $"Połączony: {roboArm.Connection.serialPort.PortName}";
            var textDisConnected = "Nie połączony";

            if (IsConnecdted)
            {
                if (statusStrip.InvokeRequired)
                {
                    statusStrip.Invoke(new Action(() => lbConnectionStatus.Text = textConnected));
                    statusStrip.Invoke(new Action(() => lbConnectionStatus.BackColor = colorConneccted));
                }
                else
                {
                    lbConnectionStatus.Text = textConnected;
                    lbConnectionStatus.BackColor = colorConneccted;
                }
            }
            else
            {
                if (statusStrip.InvokeRequired)
                {
                    statusStrip.Invoke(new Action(() => lbConnectionStatus.Text = textDisConnected));
                    statusStrip.Invoke(new Action(() => lbConnectionStatus.BackColor = colorDisConnected));
                }
                else
                {
                    lbConnectionStatus.Text = textDisConnected;
                    lbConnectionStatus.BackColor = colorDisConnected;
                }
            }

        }

        //ustawienia poczatkowe
        private void RoboControls_Load(object sender, EventArgs e)
        {
            //inicjalizacja kontrolek
            ConnectionStatusLabelUpdate(roboArm.Connection.IsConnected);
            trb_Basis.Minimum = RoboAngleToTrbValue(roboArm.basis.ArmAngle.MinAngle);
            trb_Basis.Maximum = RoboAngleToTrbValue(roboArm.basis.ArmAngle.MaxAngle);
            
            tb_Basis.Text = TrbValueToServoAngle(trb_Basis.Value).ToString();

            trb_Arm1.Minimum = RoboAngleToTrbValue(roboArm.arm1.ArmAngle.MinAngle);
            trb_Arm1.Maximum = RoboAngleToTrbValue(roboArm.arm1.ArmAngle.MaxAngle);
            tb_Arm1.Text = TrbValueToServoAngle(trb_Arm1.Value).ToString();
            

            trb_Arm2.Minimum = RoboAngleToTrbValue(roboArm.arm2.ArmAngle.MinAngle);
            trb_Arm2.Maximum = RoboAngleToTrbValue(roboArm.arm2.ArmAngle.MaxAngle);
            tb_Arm2.Text = TrbValueToServoAngle(trb_Arm2.Value).ToString();
            

            trb_ClampArm.Minimum = RoboAngleToTrbValue(roboArm.clampArm.ArmAngle.MinAngle);
            trb_ClampArm.Maximum = RoboAngleToTrbValue(roboArm.clampArm.ArmAngle.MaxAngle);
            tb_ClampArm.Text = TrbValueToServoAngle(trb_ClampArm.Value).ToString();

            trb_Wirst.Minimum = RoboAngleToTrbValue(roboArm.clampArm.wrist.ArmAngle.MinAngle);
            trb_Wirst.Maximum = RoboAngleToTrbValue(roboArm.clampArm.wrist.ArmAngle.MaxAngle);
            tb_Wirst.Text = TrbValueToServoAngle(trb_Wirst.Value).ToString();
            

            trb_Clamp.Minimum = RoboAngleToTrbValue(roboArm.clampArm.clamp.ArmAngle.MinAngle);
            trb_Clamp.Maximum = RoboAngleToTrbValue(roboArm.clampArm.clamp.ArmAngle.MaxAngle);
            tb_Clamp.Text = TrbValueToServoAngle(trb_Clamp.Value).ToString();
            txb_ClampOpenWith.Text = roboArm.clampArm.clamp.GetWidth(TrbValueToServoAngle(trb_Clamp.Value)).ToString();
            positionLabelUpdate(roboArm);

            nudMinX.Value = (int)roboArm.MIN_X_POSITION;
            nudMinY.Value = (int)roboArm.MIN_Y_POSITION;
            nudMinZ.Value = (int)roboArm.MIN_Z_POSITION;
            nudMaxX.Value = (int)roboArm.MAX_X_POSITION;
            nudMaxY.Value = (int)roboArm.MAX_Y_POSITION;
            nudMaxZ.Value = (int)roboArm.MAX_Z_POSITION;
            //flaga ustawiana, gdy zostanie załadowany formularz
            isLoaded = true;
            
            

        }

        private void positionLabelUpdate(RoboArm.RoboArm roboArm)
        {
            lbBasisPos.Text = roboArm.basis.Position3D.ToString();
            lbArm2Pos.Text = roboArm.arm2.Position3D.ToString();
            lbArm1Pos.Text = roboArm.arm1.Position3D.ToString();
            lbClampArmPos.Text = roboArm.clampArm.Position3D.ToString();
            lbWirstPos.Text= roboArm.clampArm.wrist.Position3D.ToString();
            lbClampLength.Text = roboArm.clampArm.Length.ToString("N2");
        }

        private double TrbValueToServoAngle(int value)
        {
            return value * Servo.STEP;
        }

        private int RoboAngleToTrbValue(double angle)
        {
            return (int)(angle / Servo.STEP);
        }

        private void Trb_Scroll(object sender, EventArgs e)
        {
            //wyświetlenie wartości w textboxach
            var trb = sender as TrackBar;
            var trbValue = TrbValueToServoAngle(trb.Value).ToString();
            switch (trb.Tag.ToString())
            {
                case "0":
                    {
                        tb_Basis.Text = trbValue;
                        break;
                    }
                case "1":
                    {
                        tb_Arm1.Text = trbValue;
                        break;
                    }
                case "2":
                    {
                        tb_Arm2.Text = trbValue;
                        break;
                    }
                case "3":
                    {
                        tb_ClampArm.Text = trbValue;
                        break;
                    }
                case "4":
                    {
                        tb_Wirst.Text = trbValue;
                        break;
                    }
                case "5":
                    {
                        tb_Clamp.Text = trbValue;
                        txb_ClampOpenWith.Text = roboArm.clampArm.clamp.GetWidth(TrbValueToServoAngle(trb_Clamp.Value)).ToString();
                        break;
                    }
                default:
                    break;
            }

        }

        
        private void Trb_Value_Changed(object sender, EventArgs e)
        {

            //podstawienie wartości trackbarów do obiektu roboangles
            roboAngles = new RoboAngles();
            {
                roboAngles.BasisAngle.Angle = Convert.ToDouble(tb_Basis.Text);
                roboAngles.Arm1Angle.Angle = Convert.ToDouble(tb_Arm1.Text);
                roboAngles.Arm2Angle.Angle = Convert.ToDouble(tb_Arm2.Text);
                roboAngles.ClampArmAngle.Angle = Convert.ToDouble(tb_ClampArm.Text);
                roboAngles.WristAngle.Angle = Convert.ToDouble(tb_Wirst.Text);
                roboAngles.ClampAngle.Angle = Convert.ToDouble(tb_Clamp.Text);
            }
            
            //obliczenie i wyświetlenie pozycji końsówki robota
            Point3D pos = Kinematics.ForwardKinematics(roboAngles);
            lb_positionX.Text = $"{pos.X:0.00}";
            lb_positionY.Text = $"{pos.Y:0.00}";
            lb_positionZ.Text = $"{pos.Z:0.00}";
            
            //zmiana koloru gdy pozycja niedozwolona
            Color color = new Color();
            if (!roboArm.IsPositionAllowed(roboAngles))
            {
                btnStart.Enabled =  false && roboArm.Connection.IsConnected;
                color = Color.Red;
            }
            else
            {
                btnStart.Enabled = true; // && roboArm.Connection.IsConnected;
                color = SystemColors.Control;
            }

           lb_positionX.BackColor = lb_positionY.BackColor = lb_positionZ.BackColor = color;
            textBox1.Text = $"{pos.GetDistance(new Point3D((double)nudSetPosX.Value, (double)nudSetPosY.Value, (double)nudSetPosZ.Value))}";
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            // zmiana pozycji robota                  
            roboArm.SetArmsAngles(roboAngles);
            positionLabelUpdate(roboArm);
        }

        private void nud_Value_Changed(object sender, EventArgs e)
        {
            //ustawianie dozwolonego obszaru
            if (isLoaded)
            {
                roboArm.MIN_X_POSITION = (double)nudMinX.Value;
                roboArm.MIN_Y_POSITION = (double)nudMinY.Value;
                roboArm.MIN_Z_POSITION = (double)nudMinZ.Value;
                roboArm.MAX_X_POSITION = (double)nudMaxX.Value;
                roboArm.MAX_Y_POSITION = (double)nudMaxY.Value;
                roboArm.MAX_Z_POSITION = (double)nudMaxZ.Value;
            }
        }

        private void BtnConnect_Click(object sender, EventArgs e)
        {
            //połączenie / zakończenie połaczenia ze sterownikiem
            //if (roboConnection.IsConnected)
            //    roboArm.Connection.Close();
            //else
            //{
            //    roboConnection.serialPort.PortName = cb_SerialPorts.SelectedItem.ToString();
            //    try
            //    {
            //        roboConnection.Open();
            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show($"Nie można połaczyć ze sterownikiem\n{ex.Message}");
            //    }

            //}
            if (roboArm.Connection.IsConnected)
                roboArm.Connection.Close();
            else
            {
                //roboConnection.serialPort.PortName = cb_SerialPorts.SelectedItem.ToString();
                try
                {
                    roboArm.Connection.Open();
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Nie można połaczyć ze sterownikiem\n{ex.Message}");
                }

            }
            //serialPort.Open();
        }

        private void BtnSetPosition_Click(object sender, EventArgs e)
        {
            Point3D Target = new Point3D((double)nudSetPosX.Value, (double)nudSetPosY.Value, (double)nudSetPosZ.Value);
            var roboAngles = Kinematics.InverseKinematics(Target, roboArm.roboAngles);
            TbApdate(roboAngles);
            TrackBarUpdate(roboAngles);

        }

        private void TrackBarUpdate(RoboAngles roboAngles)
        {
            trb_Basis.Value = RoboAngleToTrbValue(roboAngles.BasisAngle.Angle);
            trb_Arm1.Value = RoboAngleToTrbValue(roboAngles.Arm1Angle.Angle);
            trb_Arm2.Value = RoboAngleToTrbValue(roboAngles.Arm2Angle.Angle);
            trb_ClampArm.Value = RoboAngleToTrbValue(roboAngles.ClampArmAngle.Angle);
            trb_Wirst.Value = RoboAngleToTrbValue(roboAngles.WristAngle.Angle);
            trb_Clamp.Value = RoboAngleToTrbValue(roboAngles.ClampAngle.Angle);
        }

        private void TbApdate(RoboAngles roboAngles)
        {
            tb_Basis.Text = roboAngles.BasisAngle.Angle.ToString("N2");
            tb_Arm1.Text = roboAngles.Arm1Angle.Angle.ToString("N2");
            tb_Arm2.Text = roboAngles.Arm2Angle.Angle.ToString("N2");
            tb_ClampArm.Text = roboAngles.ClampArmAngle.Angle.ToString("N2");
            tb_Wirst.Text = roboAngles.WristAngle.Angle.ToString("N2");
            tb_Clamp.Text = roboAngles.ClampAngle.Angle.ToString("N2");
        }

        private void btn_Remeber_Click(object sender, EventArgs e)
        {
            RoboAnglesList.Add(roboArm.roboAngles);
            UpdateTb_SaveAnglesCount();
        }

        private void btn_SaveToFile_Click(object sender, EventArgs e)
        {
            if (saveAnglesFile.ShowDialog()==DialogResult.OK)
            {
                try
                {
                    AngleListRepo.Save(RoboAnglesList, saveAnglesFile.FileName);
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void btn_OpenFile_Click(object sender, EventArgs e)
        {
            if (openAnglesFile.ShowDialog()==DialogResult.OK)
            {
                RoboAnglesList.Clear();
                RoboAnglesList = AngleListRepo.Open(openAnglesFile.FileName);
                UpdateTb_SaveAnglesCount();
            }
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            if (RoboAnglesList.Count>0)
            {
                RoboAnglesList.RemoveAt(RoboAnglesList.Count - 1);
                UpdateTb_SaveAnglesCount();
            }
        }

        private void UpdateTb_SaveAnglesCount()
        {
            tb_SaveAnglesCount.Text = $"{RoboAnglesList.Count}";
        }

        private void btn_Clear_Click(object sender, EventArgs e)
        {
            if (RoboAnglesList.Count > 0)
            {
                RoboAnglesList.Clear();
                UpdateTb_SaveAnglesCount();
            }
        }

        private void btn_StartFromFile_Click(object sender, EventArgs e)
        {
            if (RoboAnglesList.Count>0)
            roboArm.Run(RoboAnglesList);
            TrackBarUpdate(roboArm.roboAngles);
            positionLabelUpdate(roboArm);
        }

        private void btnVS_Click(object sender, EventArgs e)
        {
            VideoViewer vs = new VideoViewer();
            vs.Show();
        }
    }
}
