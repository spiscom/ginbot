﻿namespace ginbot.UI
{
    partial class RoboControls
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.trb_Basis = new System.Windows.Forms.TrackBar();
            this.tb_Basis = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbBasisPos = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbArm1Pos = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.trb_Arm1 = new System.Windows.Forms.TrackBar();
            this.tb_Arm1 = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lbArm2Pos = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.trb_Arm2 = new System.Windows.Forms.TrackBar();
            this.tb_Arm2 = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lbClampArmPos = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.trb_ClampArm = new System.Windows.Forms.TrackBar();
            this.tb_ClampArm = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lbWirstPos = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.trb_Wirst = new System.Windows.Forms.TrackBar();
            this.tb_Wirst = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lbClampLength = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txb_ClampOpenWith = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.trb_Clamp = new System.Windows.Forms.TrackBar();
            this.tb_Clamp = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.nudMaxZ = new System.Windows.Forms.NumericUpDown();
            this.nudMaxY = new System.Windows.Forms.NumericUpDown();
            this.nudMaxX = new System.Windows.Forms.NumericUpDown();
            this.nudMinZ = new System.Windows.Forms.NumericUpDown();
            this.nudMinY = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.nudMinX = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lb_positionX = new System.Windows.Forms.Label();
            this.lb_positionY = new System.Windows.Forms.Label();
            this.lb_positionZ = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.btnSetPosition = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.nudSetPosZ = new System.Windows.Forms.NumericUpDown();
            this.nudSetPosY = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.nudSetPosX = new System.Windows.Forms.NumericUpDown();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btn_Remeber = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pB_RunFromList = new System.Windows.Forms.ProgressBar();
            this.btn_StartFromFile = new System.Windows.Forms.Button();
            this.btn_Clear = new System.Windows.Forms.Button();
            this.btn_back = new System.Windows.Forms.Button();
            this.btn_OpenFile = new System.Windows.Forms.Button();
            this.tb_SaveAnglesCount = new System.Windows.Forms.TextBox();
            this.btn_SaveToFile = new System.Windows.Forms.Button();
            this.saveAnglesFile = new System.Windows.Forms.SaveFileDialog();
            this.openAnglesFile = new System.Windows.Forms.OpenFileDialog();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.lbConnectionStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.trb_Basis)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trb_Arm1)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trb_Arm2)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trb_ClampArm)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trb_Wirst)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trb_Clamp)).BeginInit();
            this.panel7.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinX)).BeginInit();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSetPosZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSetPosY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSetPosX)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.statusStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // trb_Basis
            // 
            this.trb_Basis.AutoSize = false;
            this.trb_Basis.Location = new System.Drawing.Point(3, 18);
            this.trb_Basis.Margin = new System.Windows.Forms.Padding(2);
            this.trb_Basis.Maximum = 180;
            this.trb_Basis.Name = "trb_Basis";
            this.trb_Basis.Size = new System.Drawing.Size(198, 45);
            this.trb_Basis.TabIndex = 1;
            this.trb_Basis.Tag = "0";
            this.trb_Basis.Value = 90;
            this.trb_Basis.Scroll += new System.EventHandler(this.Trb_Scroll);
            this.trb_Basis.ValueChanged += new System.EventHandler(this.Trb_Value_Changed);
            // 
            // tb_Basis
            // 
            this.tb_Basis.Location = new System.Drawing.Point(79, 65);
            this.tb_Basis.Margin = new System.Windows.Forms.Padding(2);
            this.tb_Basis.Name = "tb_Basis";
            this.tb_Basis.Size = new System.Drawing.Size(51, 20);
            this.tb_Basis.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lbBasisPos);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.trb_Basis);
            this.panel1.Controls.Add(this.tb_Basis);
            this.panel1.Location = new System.Drawing.Point(380, 321);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(207, 88);
            this.panel1.TabIndex = 3;
            // 
            // lbBasisPos
            // 
            this.lbBasisPos.AutoSize = true;
            this.lbBasisPos.Location = new System.Drawing.Point(13, 50);
            this.lbBasisPos.Name = "lbBasisPos";
            this.lbBasisPos.Size = new System.Drawing.Size(38, 13);
            this.lbBasisPos.TabIndex = 4;
            this.lbBasisPos.Text = "poz3D";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 3);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Basis (Podstawa)";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lbArm1Pos);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.trb_Arm1);
            this.panel2.Controls.Add(this.tb_Arm1);
            this.panel2.Location = new System.Drawing.Point(380, 230);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(207, 88);
            this.panel2.TabIndex = 4;
            // 
            // lbArm1Pos
            // 
            this.lbArm1Pos.AutoSize = true;
            this.lbArm1Pos.Location = new System.Drawing.Point(13, 49);
            this.lbArm1Pos.Name = "lbArm1Pos";
            this.lbArm1Pos.Size = new System.Drawing.Size(38, 13);
            this.lbArm1Pos.TabIndex = 5;
            this.lbArm1Pos.Text = "poz3D";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 3);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Arm1 (Ramie 1)";
            // 
            // trb_Arm1
            // 
            this.trb_Arm1.AutoSize = false;
            this.trb_Arm1.Location = new System.Drawing.Point(3, 18);
            this.trb_Arm1.Margin = new System.Windows.Forms.Padding(2);
            this.trb_Arm1.Maximum = 180;
            this.trb_Arm1.Name = "trb_Arm1";
            this.trb_Arm1.Size = new System.Drawing.Size(198, 45);
            this.trb_Arm1.TabIndex = 1;
            this.trb_Arm1.Tag = "1";
            this.trb_Arm1.Value = 100;
            this.trb_Arm1.Scroll += new System.EventHandler(this.Trb_Scroll);
            this.trb_Arm1.ValueChanged += new System.EventHandler(this.Trb_Value_Changed);
            // 
            // tb_Arm1
            // 
            this.tb_Arm1.Location = new System.Drawing.Point(79, 65);
            this.tb_Arm1.Margin = new System.Windows.Forms.Padding(2);
            this.tb_Arm1.Name = "tb_Arm1";
            this.tb_Arm1.Size = new System.Drawing.Size(51, 20);
            this.tb_Arm1.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.lbArm2Pos);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.trb_Arm2);
            this.panel3.Controls.Add(this.tb_Arm2);
            this.panel3.Location = new System.Drawing.Point(466, 138);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(207, 88);
            this.panel3.TabIndex = 5;
            // 
            // lbArm2Pos
            // 
            this.lbArm2Pos.AutoSize = true;
            this.lbArm2Pos.Location = new System.Drawing.Point(13, 50);
            this.lbArm2Pos.Name = "lbArm2Pos";
            this.lbArm2Pos.Size = new System.Drawing.Size(38, 13);
            this.lbArm2Pos.TabIndex = 6;
            this.lbArm2Pos.Text = "poz3D";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 3);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Arm2 (Ramie 2)";
            // 
            // trb_Arm2
            // 
            this.trb_Arm2.AutoSize = false;
            this.trb_Arm2.Location = new System.Drawing.Point(3, 18);
            this.trb_Arm2.Margin = new System.Windows.Forms.Padding(2);
            this.trb_Arm2.Maximum = 180;
            this.trb_Arm2.Minimum = -200;
            this.trb_Arm2.Name = "trb_Arm2";
            this.trb_Arm2.Size = new System.Drawing.Size(198, 45);
            this.trb_Arm2.TabIndex = 1;
            this.trb_Arm2.Tag = "2";
            this.trb_Arm2.Value = -90;
            this.trb_Arm2.Scroll += new System.EventHandler(this.Trb_Scroll);
            this.trb_Arm2.ValueChanged += new System.EventHandler(this.Trb_Value_Changed);
            // 
            // tb_Arm2
            // 
            this.tb_Arm2.Location = new System.Drawing.Point(79, 65);
            this.tb_Arm2.Margin = new System.Windows.Forms.Padding(2);
            this.tb_Arm2.Name = "tb_Arm2";
            this.tb_Arm2.Size = new System.Drawing.Size(51, 20);
            this.tb_Arm2.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.lbClampArmPos);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.trb_ClampArm);
            this.panel4.Controls.Add(this.tb_ClampArm);
            this.panel4.Location = new System.Drawing.Point(566, 48);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(207, 88);
            this.panel4.TabIndex = 6;
            // 
            // lbClampArmPos
            // 
            this.lbClampArmPos.AutoSize = true;
            this.lbClampArmPos.Location = new System.Drawing.Point(13, 50);
            this.lbClampArmPos.Name = "lbClampArmPos";
            this.lbClampArmPos.Size = new System.Drawing.Size(38, 13);
            this.lbClampArmPos.TabIndex = 7;
            this.lbClampArmPos.Text = "poz3D";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 3);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(142, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "ClampArm (Ramie chwytaka)";
            // 
            // trb_ClampArm
            // 
            this.trb_ClampArm.AutoSize = false;
            this.trb_ClampArm.Location = new System.Drawing.Point(3, 18);
            this.trb_ClampArm.Margin = new System.Windows.Forms.Padding(2);
            this.trb_ClampArm.Maximum = 180;
            this.trb_ClampArm.Name = "trb_ClampArm";
            this.trb_ClampArm.Size = new System.Drawing.Size(198, 45);
            this.trb_ClampArm.TabIndex = 1;
            this.trb_ClampArm.Tag = "3";
            this.trb_ClampArm.Scroll += new System.EventHandler(this.Trb_Scroll);
            this.trb_ClampArm.ValueChanged += new System.EventHandler(this.Trb_Value_Changed);
            // 
            // tb_ClampArm
            // 
            this.tb_ClampArm.Location = new System.Drawing.Point(79, 65);
            this.tb_ClampArm.Margin = new System.Windows.Forms.Padding(2);
            this.tb_ClampArm.Name = "tb_ClampArm";
            this.tb_ClampArm.Size = new System.Drawing.Size(51, 20);
            this.tb_ClampArm.TabIndex = 2;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.lbWirstPos);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.trb_Wirst);
            this.panel5.Controls.Add(this.tb_Wirst);
            this.panel5.Location = new System.Drawing.Point(776, 24);
            this.panel5.Margin = new System.Windows.Forms.Padding(2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(207, 88);
            this.panel5.TabIndex = 7;
            // 
            // lbWirstPos
            // 
            this.lbWirstPos.AutoSize = true;
            this.lbWirstPos.Location = new System.Drawing.Point(16, 50);
            this.lbWirstPos.Name = "lbWirstPos";
            this.lbWirstPos.Size = new System.Drawing.Size(38, 13);
            this.lbWirstPos.TabIndex = 7;
            this.lbWirstPos.Text = "poz3D";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 3);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Wirst (Nadgarstek)";
            // 
            // trb_Wirst
            // 
            this.trb_Wirst.AutoSize = false;
            this.trb_Wirst.Location = new System.Drawing.Point(3, 18);
            this.trb_Wirst.Margin = new System.Windows.Forms.Padding(2);
            this.trb_Wirst.Maximum = 180;
            this.trb_Wirst.Name = "trb_Wirst";
            this.trb_Wirst.Size = new System.Drawing.Size(198, 45);
            this.trb_Wirst.TabIndex = 1;
            this.trb_Wirst.Tag = "4";
            this.trb_Wirst.Value = 90;
            this.trb_Wirst.Scroll += new System.EventHandler(this.Trb_Scroll);
            this.trb_Wirst.ValueChanged += new System.EventHandler(this.Trb_Value_Changed);
            // 
            // tb_Wirst
            // 
            this.tb_Wirst.Location = new System.Drawing.Point(79, 65);
            this.tb_Wirst.Margin = new System.Windows.Forms.Padding(2);
            this.tb_Wirst.Name = "tb_Wirst";
            this.tb_Wirst.Size = new System.Drawing.Size(51, 20);
            this.tb_Wirst.TabIndex = 2;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.lbClampLength);
            this.panel6.Controls.Add(this.label23);
            this.panel6.Controls.Add(this.label14);
            this.panel6.Controls.Add(this.txb_ClampOpenWith);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Controls.Add(this.trb_Clamp);
            this.panel6.Controls.Add(this.tb_Clamp);
            this.panel6.Location = new System.Drawing.Point(776, 120);
            this.panel6.Margin = new System.Windows.Forms.Padding(2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(207, 88);
            this.panel6.TabIndex = 7;
            // 
            // lbClampLength
            // 
            this.lbClampLength.AutoSize = true;
            this.lbClampLength.Location = new System.Drawing.Point(16, 48);
            this.lbClampLength.Name = "lbClampLength";
            this.lbClampLength.Size = new System.Drawing.Size(36, 13);
            this.lbClampLength.TabIndex = 7;
            this.lbClampLength.Text = "length";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(178, 65);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(23, 13);
            this.label23.TabIndex = 6;
            this.label23.Text = "mm";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(58, 67);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(15, 13);
            this.label14.TabIndex = 5;
            this.label14.Text = "st";
            // 
            // txb_ClampOpenWith
            // 
            this.txb_ClampOpenWith.Location = new System.Drawing.Point(124, 63);
            this.txb_ClampOpenWith.Margin = new System.Windows.Forms.Padding(2);
            this.txb_ClampOpenWith.Name = "txb_ClampOpenWith";
            this.txb_ClampOpenWith.Size = new System.Drawing.Size(51, 20);
            this.txb_ClampOpenWith.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 3);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Clamp (Chwytak)";
            // 
            // trb_Clamp
            // 
            this.trb_Clamp.AutoSize = false;
            this.trb_Clamp.Location = new System.Drawing.Point(3, 18);
            this.trb_Clamp.Margin = new System.Windows.Forms.Padding(2);
            this.trb_Clamp.Maximum = 180;
            this.trb_Clamp.Name = "trb_Clamp";
            this.trb_Clamp.Size = new System.Drawing.Size(198, 45);
            this.trb_Clamp.TabIndex = 1;
            this.trb_Clamp.Tag = "5";
            this.trb_Clamp.Scroll += new System.EventHandler(this.Trb_Scroll);
            this.trb_Clamp.ValueChanged += new System.EventHandler(this.Trb_Value_Changed);
            // 
            // tb_Clamp
            // 
            this.tb_Clamp.Location = new System.Drawing.Point(3, 63);
            this.tb_Clamp.Margin = new System.Windows.Forms.Padding(2);
            this.tb_Clamp.Name = "tb_Clamp";
            this.tb_Clamp.Size = new System.Drawing.Size(51, 20);
            this.tb_Clamp.TabIndex = 2;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.tableLayoutPanel1);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(0, 31);
            this.panel7.Margin = new System.Windows.Forms.Padding(2);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(247, 90);
            this.panel7.TabIndex = 8;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.37165F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.54278F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.54279F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.54278F));
            this.tableLayoutPanel1.Controls.Add(this.nudMaxZ, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.nudMaxY, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.nudMaxX, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.nudMinZ, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.nudMinY, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label16, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label10, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label9, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label8, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.nudMinX, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(247, 90);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // nudMaxZ
            // 
            this.nudMaxZ.DecimalPlaces = 2;
            this.nudMaxZ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nudMaxZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nudMaxZ.Location = new System.Drawing.Point(184, 61);
            this.nudMaxZ.Margin = new System.Windows.Forms.Padding(2);
            this.nudMaxZ.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudMaxZ.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            -2147483648});
            this.nudMaxZ.Name = "nudMaxZ";
            this.nudMaxZ.Size = new System.Drawing.Size(60, 22);
            this.nudMaxZ.TabIndex = 16;
            this.nudMaxZ.ValueChanged += new System.EventHandler(this.nud_Value_Changed);
            // 
            // nudMaxY
            // 
            this.nudMaxY.DecimalPlaces = 2;
            this.nudMaxY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nudMaxY.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nudMaxY.Location = new System.Drawing.Point(122, 61);
            this.nudMaxY.Margin = new System.Windows.Forms.Padding(2);
            this.nudMaxY.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudMaxY.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            -2147483648});
            this.nudMaxY.Name = "nudMaxY";
            this.nudMaxY.Size = new System.Drawing.Size(57, 22);
            this.nudMaxY.TabIndex = 15;
            this.nudMaxY.ValueChanged += new System.EventHandler(this.nud_Value_Changed);
            // 
            // nudMaxX
            // 
            this.nudMaxX.DecimalPlaces = 2;
            this.nudMaxX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nudMaxX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nudMaxX.Location = new System.Drawing.Point(60, 61);
            this.nudMaxX.Margin = new System.Windows.Forms.Padding(2);
            this.nudMaxX.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudMaxX.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            -2147483648});
            this.nudMaxX.Name = "nudMaxX";
            this.nudMaxX.Size = new System.Drawing.Size(57, 22);
            this.nudMaxX.TabIndex = 14;
            this.nudMaxX.ValueChanged += new System.EventHandler(this.nud_Value_Changed);
            // 
            // nudMinZ
            // 
            this.nudMinZ.DecimalPlaces = 2;
            this.nudMinZ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nudMinZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nudMinZ.Location = new System.Drawing.Point(184, 32);
            this.nudMinZ.Margin = new System.Windows.Forms.Padding(2);
            this.nudMinZ.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudMinZ.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            -2147483648});
            this.nudMinZ.Name = "nudMinZ";
            this.nudMinZ.Size = new System.Drawing.Size(60, 22);
            this.nudMinZ.TabIndex = 13;
            this.nudMinZ.ValueChanged += new System.EventHandler(this.nud_Value_Changed);
            // 
            // nudMinY
            // 
            this.nudMinY.DecimalPlaces = 2;
            this.nudMinY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nudMinY.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nudMinY.Location = new System.Drawing.Point(122, 32);
            this.nudMinY.Margin = new System.Windows.Forms.Padding(2);
            this.nudMinY.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudMinY.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            -2147483648});
            this.nudMinY.Name = "nudMinY";
            this.nudMinY.Size = new System.Drawing.Size(57, 22);
            this.nudMinY.TabIndex = 12;
            this.nudMinY.ValueChanged += new System.EventHandler(this.nud_Value_Changed);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Location = new System.Drawing.Point(3, 61);
            this.label16.Margin = new System.Windows.Forms.Padding(2);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(52, 26);
            this.label16.TabIndex = 9;
            this.label16.Text = "Max";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Location = new System.Drawing.Point(3, 32);
            this.label12.Margin = new System.Windows.Forms.Padding(2);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 24);
            this.label12.TabIndex = 4;
            this.label12.Text = "Min";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Location = new System.Drawing.Point(184, 3);
            this.label10.Margin = new System.Windows.Forms.Padding(2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 24);
            this.label10.TabIndex = 2;
            this.label10.Text = "Z";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Location = new System.Drawing.Point(122, 3);
            this.label9.Margin = new System.Windows.Forms.Padding(2);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 24);
            this.label9.TabIndex = 1;
            this.label9.Text = "Y";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(60, 3);
            this.label8.Margin = new System.Windows.Forms.Padding(2);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 24);
            this.label8.TabIndex = 0;
            this.label8.Text = "X";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // nudMinX
            // 
            this.nudMinX.DecimalPlaces = 2;
            this.nudMinX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nudMinX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nudMinX.Location = new System.Drawing.Point(60, 32);
            this.nudMinX.Margin = new System.Windows.Forms.Padding(2);
            this.nudMinX.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudMinX.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            -2147483648});
            this.nudMinX.Name = "nudMinX";
            this.nudMinX.Size = new System.Drawing.Size(57, 22);
            this.nudMinX.TabIndex = 10;
            this.nudMinX.ValueChanged += new System.EventHandler(this.nud_Value_Changed);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 3);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Obszar dozwolony";
            // 
            // panel8
            // 
            this.panel8.AutoSize = true;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.label7);
            this.panel8.Controls.Add(this.panel7);
            this.panel8.Location = new System.Drawing.Point(24, 11);
            this.panel8.Margin = new System.Windows.Forms.Padding(2);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(249, 123);
            this.panel8.TabIndex = 9;
            // 
            // panel9
            // 
            this.panel9.AutoSize = true;
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.label11);
            this.panel9.Controls.Add(this.panel10);
            this.panel9.Location = new System.Drawing.Point(25, 157);
            this.panel9.Margin = new System.Windows.Forms.Padding(2);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(247, 90);
            this.panel9.TabIndex = 10;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(5, 3);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Pozycja 3D";
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.tableLayoutPanel2);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel10.Location = new System.Drawing.Point(0, 28);
            this.panel10.Margin = new System.Windows.Forms.Padding(2);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(245, 60);
            this.panel10.TabIndex = 8;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this.tableLayoutPanel2.Controls.Add(this.lb_positionX, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.lb_positionY, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.lb_positionZ, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label15, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label17, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label18, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 49.99999F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(245, 60);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // lb_positionX
            // 
            this.lb_positionX.AutoSize = true;
            this.lb_positionX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lb_positionX.Location = new System.Drawing.Point(3, 32);
            this.lb_positionX.Margin = new System.Windows.Forms.Padding(2);
            this.lb_positionX.Name = "lb_positionX";
            this.lb_positionX.Size = new System.Drawing.Size(76, 25);
            this.lb_positionX.TabIndex = 5;
            this.lb_positionX.Text = "X";
            this.lb_positionX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_positionY
            // 
            this.lb_positionY.AutoSize = true;
            this.lb_positionY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lb_positionY.Location = new System.Drawing.Point(84, 32);
            this.lb_positionY.Margin = new System.Windows.Forms.Padding(2);
            this.lb_positionY.Name = "lb_positionY";
            this.lb_positionY.Size = new System.Drawing.Size(76, 25);
            this.lb_positionY.TabIndex = 4;
            this.lb_positionY.Text = "X";
            this.lb_positionY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_positionZ
            // 
            this.lb_positionZ.AutoSize = true;
            this.lb_positionZ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lb_positionZ.Location = new System.Drawing.Point(165, 32);
            this.lb_positionZ.Margin = new System.Windows.Forms.Padding(2);
            this.lb_positionZ.Name = "lb_positionZ";
            this.lb_positionZ.Size = new System.Drawing.Size(77, 25);
            this.lb_positionZ.TabIndex = 3;
            this.lb_positionZ.Text = "X";
            this.lb_positionZ.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Location = new System.Drawing.Point(165, 3);
            this.label15.Margin = new System.Windows.Forms.Padding(2);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 24);
            this.label15.TabIndex = 2;
            this.label15.Text = "Z";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Location = new System.Drawing.Point(84, 3);
            this.label17.Margin = new System.Windows.Forms.Padding(2);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(76, 24);
            this.label17.TabIndex = 1;
            this.label17.Text = "Y";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label18.Location = new System.Drawing.Point(3, 3);
            this.label18.Margin = new System.Windows.Forms.Padding(2);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(76, 24);
            this.label18.TabIndex = 0;
            this.label18.Text = "X";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnStart
            // 
            this.btnStart.Enabled = false;
            this.btnStart.Location = new System.Drawing.Point(905, 234);
            this.btnStart.Margin = new System.Windows.Forms.Padding(2);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(73, 22);
            this.btnStart.TabIndex = 11;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // panel11
            // 
            this.panel11.AutoSize = true;
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.label13);
            this.panel11.Controls.Add(this.panel12);
            this.panel11.Location = new System.Drawing.Point(24, 280);
            this.panel11.Margin = new System.Windows.Forms.Padding(2);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(249, 148);
            this.panel11.TabIndex = 13;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(5, 3);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(167, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Oblicz kąty -Kinematyka odwrotna";
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.label24);
            this.panel12.Controls.Add(this.btnSetPosition);
            this.panel12.Controls.Add(this.tableLayoutPanel3);
            this.panel12.Controls.Add(this.textBox1);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel12.Location = new System.Drawing.Point(0, 26);
            this.panel12.Margin = new System.Windows.Forms.Padding(2);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(247, 120);
            this.panel12.TabIndex = 8;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(87, 99);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(119, 13);
            this.label24.TabIndex = 20;
            this.label24.Text = "Odległość do celu [mm]";
            // 
            // btnSetPosition
            // 
            this.btnSetPosition.Location = new System.Drawing.Point(169, 66);
            this.btnSetPosition.Margin = new System.Windows.Forms.Padding(2);
            this.btnSetPosition.Name = "btnSetPosition";
            this.btnSetPosition.Size = new System.Drawing.Size(73, 22);
            this.btnSetPosition.TabIndex = 12;
            this.btnSetPosition.Text = "Oblicz";
            this.btnSetPosition.UseVisualStyleBackColor = true;
            this.btnSetPosition.Click += new System.EventHandler(this.BtnSetPosition_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.80853F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.71142F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.74003F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.74002F));
            this.tableLayoutPanel3.Controls.Add(this.nudSetPosZ, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.nudSetPosY, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.label19, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label20, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.label21, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label22, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.nudSetPosX, 1, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 42.59259F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 57.40741F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(247, 42);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // nudSetPosZ
            // 
            this.nudSetPosZ.DecimalPlaces = 2;
            this.nudSetPosZ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nudSetPosZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nudSetPosZ.Location = new System.Drawing.Point(185, 20);
            this.nudSetPosZ.Margin = new System.Windows.Forms.Padding(2);
            this.nudSetPosZ.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudSetPosZ.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            -2147483648});
            this.nudSetPosZ.Name = "nudSetPosZ";
            this.nudSetPosZ.Size = new System.Drawing.Size(59, 22);
            this.nudSetPosZ.TabIndex = 13;
            this.nudSetPosZ.Value = new decimal(new int[] {
            150,
            0,
            0,
            0});
            // 
            // nudSetPosY
            // 
            this.nudSetPosY.DecimalPlaces = 2;
            this.nudSetPosY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nudSetPosY.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nudSetPosY.Location = new System.Drawing.Point(122, 20);
            this.nudSetPosY.Margin = new System.Windows.Forms.Padding(2);
            this.nudSetPosY.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudSetPosY.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            -2147483648});
            this.nudSetPosY.Name = "nudSetPosY";
            this.nudSetPosY.Size = new System.Drawing.Size(58, 22);
            this.nudSetPosY.TabIndex = 12;
            this.nudSetPosY.Value = new decimal(new int[] {
            150,
            0,
            0,
            0});
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.Location = new System.Drawing.Point(3, 20);
            this.label19.Margin = new System.Windows.Forms.Padding(2);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(51, 19);
            this.label19.TabIndex = 4;
            this.label19.Text = "Min";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label20.Location = new System.Drawing.Point(185, 3);
            this.label20.Margin = new System.Windows.Forms.Padding(2);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(59, 12);
            this.label20.TabIndex = 2;
            this.label20.Text = "Z";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Location = new System.Drawing.Point(122, 3);
            this.label21.Margin = new System.Windows.Forms.Padding(2);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(58, 12);
            this.label21.TabIndex = 1;
            this.label21.Text = "Y";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label22.Location = new System.Drawing.Point(59, 3);
            this.label22.Margin = new System.Windows.Forms.Padding(2);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(58, 12);
            this.label22.TabIndex = 0;
            this.label22.Text = "X";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // nudSetPosX
            // 
            this.nudSetPosX.DecimalPlaces = 2;
            this.nudSetPosX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nudSetPosX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nudSetPosX.Location = new System.Drawing.Point(59, 20);
            this.nudSetPosX.Margin = new System.Windows.Forms.Padding(2);
            this.nudSetPosX.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudSetPosX.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            -2147483648});
            this.nudSetPosX.Name = "nudSetPosX";
            this.nudSetPosX.Size = new System.Drawing.Size(58, 22);
            this.nudSetPosX.TabIndex = 10;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(6, 96);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(76, 20);
            this.textBox1.TabIndex = 14;
            // 
            // btn_Remeber
            // 
            this.btn_Remeber.Location = new System.Drawing.Point(9, 28);
            this.btn_Remeber.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Remeber.Name = "btn_Remeber";
            this.btn_Remeber.Size = new System.Drawing.Size(95, 22);
            this.btn_Remeber.TabIndex = 15;
            this.btn_Remeber.Text = "Zapamietaj";
            this.btn_Remeber.UseVisualStyleBackColor = true;
            this.btn_Remeber.Click += new System.EventHandler(this.btn_Remeber_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pB_RunFromList);
            this.groupBox1.Controls.Add(this.btn_StartFromFile);
            this.groupBox1.Controls.Add(this.btn_Clear);
            this.groupBox1.Controls.Add(this.btn_back);
            this.groupBox1.Controls.Add(this.btn_OpenFile);
            this.groupBox1.Controls.Add(this.tb_SaveAnglesCount);
            this.groupBox1.Controls.Add(this.btn_SaveToFile);
            this.groupBox1.Controls.Add(this.btn_Remeber);
            this.groupBox1.Location = new System.Drawing.Point(658, 249);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(192, 179);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Zapamiętaj pozycję";
            // 
            // pB_RunFromList
            // 
            this.pB_RunFromList.Location = new System.Drawing.Point(10, 155);
            this.pB_RunFromList.Margin = new System.Windows.Forms.Padding(2);
            this.pB_RunFromList.Name = "pB_RunFromList";
            this.pB_RunFromList.Size = new System.Drawing.Size(173, 19);
            this.pB_RunFromList.TabIndex = 22;
            // 
            // btn_StartFromFile
            // 
            this.btn_StartFromFile.Location = new System.Drawing.Point(10, 123);
            this.btn_StartFromFile.Margin = new System.Windows.Forms.Padding(2);
            this.btn_StartFromFile.Name = "btn_StartFromFile";
            this.btn_StartFromFile.Size = new System.Drawing.Size(96, 22);
            this.btn_StartFromFile.TabIndex = 21;
            this.btn_StartFromFile.Text = "Start";
            this.btn_StartFromFile.UseVisualStyleBackColor = true;
            this.btn_StartFromFile.Click += new System.EventHandler(this.btn_StartFromFile_Click);
            // 
            // btn_Clear
            // 
            this.btn_Clear.Location = new System.Drawing.Point(121, 89);
            this.btn_Clear.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Clear.Name = "btn_Clear";
            this.btn_Clear.Size = new System.Drawing.Size(54, 22);
            this.btn_Clear.TabIndex = 20;
            this.btn_Clear.Text = "czyść";
            this.btn_Clear.UseVisualStyleBackColor = true;
            this.btn_Clear.Click += new System.EventHandler(this.btn_Clear_Click);
            // 
            // btn_back
            // 
            this.btn_back.Location = new System.Drawing.Point(121, 58);
            this.btn_back.Margin = new System.Windows.Forms.Padding(2);
            this.btn_back.Name = "btn_back";
            this.btn_back.Size = new System.Drawing.Size(54, 22);
            this.btn_back.TabIndex = 19;
            this.btn_back.Text = "cofnij";
            this.btn_back.UseVisualStyleBackColor = true;
            this.btn_back.Click += new System.EventHandler(this.btn_back_Click);
            // 
            // btn_OpenFile
            // 
            this.btn_OpenFile.Location = new System.Drawing.Point(10, 58);
            this.btn_OpenFile.Margin = new System.Windows.Forms.Padding(2);
            this.btn_OpenFile.Name = "btn_OpenFile";
            this.btn_OpenFile.Size = new System.Drawing.Size(95, 22);
            this.btn_OpenFile.TabIndex = 18;
            this.btn_OpenFile.Text = "Otwórz z pliku";
            this.btn_OpenFile.UseVisualStyleBackColor = true;
            this.btn_OpenFile.Click += new System.EventHandler(this.btn_OpenFile_Click);
            // 
            // tb_SaveAnglesCount
            // 
            this.tb_SaveAnglesCount.Location = new System.Drawing.Point(121, 29);
            this.tb_SaveAnglesCount.Margin = new System.Windows.Forms.Padding(2);
            this.tb_SaveAnglesCount.Name = "tb_SaveAnglesCount";
            this.tb_SaveAnglesCount.Size = new System.Drawing.Size(55, 20);
            this.tb_SaveAnglesCount.TabIndex = 17;
            // 
            // btn_SaveToFile
            // 
            this.btn_SaveToFile.Location = new System.Drawing.Point(9, 89);
            this.btn_SaveToFile.Margin = new System.Windows.Forms.Padding(2);
            this.btn_SaveToFile.Name = "btn_SaveToFile";
            this.btn_SaveToFile.Size = new System.Drawing.Size(96, 22);
            this.btn_SaveToFile.TabIndex = 16;
            this.btn_SaveToFile.Text = "Zapisz do pliku";
            this.btn_SaveToFile.UseVisualStyleBackColor = true;
            this.btn_SaveToFile.Click += new System.EventHandler(this.btn_SaveToFile_Click);
            // 
            // saveAnglesFile
            // 
            this.saveAnglesFile.Filter = "plik RoboAngles|*.rba";
            // 
            // openAnglesFile
            // 
            this.openAnglesFile.Filter = "plik RoboAngles|*.rba";
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lbConnectionStatus});
            this.statusStrip.Location = new System.Drawing.Point(0, 466);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1036, 22);
            this.statusStrip.TabIndex = 18;
            this.statusStrip.Text = "statusStrip1";
            // 
            // lbConnectionStatus
            // 
            this.lbConnectionStatus.BackColor = System.Drawing.Color.Yellow;
            this.lbConnectionStatus.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbConnectionStatus.Name = "lbConnectionStatus";
            this.lbConnectionStatus.Size = new System.Drawing.Size(96, 17);
            this.lbConnectionStatus.Text = "Nie połączono";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // RoboControls
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1036, 488);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "RoboControls";
            this.Text = "RoboControls";
            this.Load += new System.EventHandler(this.RoboControls_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trb_Basis)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trb_Arm1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trb_Arm2)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trb_ClampArm)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trb_Wirst)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trb_Clamp)).EndInit();
            this.panel7.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinX)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSetPosZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSetPosY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSetPosX)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TrackBar trb_Basis;
        private System.Windows.Forms.TextBox tb_Basis;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TrackBar trb_Arm1;
        private System.Windows.Forms.TextBox tb_Arm1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TrackBar trb_Arm2;
        private System.Windows.Forms.TextBox tb_Arm2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TrackBar trb_ClampArm;
        private System.Windows.Forms.TextBox tb_ClampArm;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TrackBar trb_Wirst;
        private System.Windows.Forms.TextBox tb_Wirst;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TrackBar trb_Clamp;
        private System.Windows.Forms.TextBox tb_Clamp;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown nudMinX;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.NumericUpDown nudMaxZ;
        private System.Windows.Forms.NumericUpDown nudMaxY;
        private System.Windows.Forms.NumericUpDown nudMaxX;
        private System.Windows.Forms.NumericUpDown nudMinZ;
        private System.Windows.Forms.NumericUpDown nudMinY;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label lb_positionX;
        private System.Windows.Forms.Label lb_positionY;
        private System.Windows.Forms.Label lb_positionZ;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.NumericUpDown nudSetPosZ;
        private System.Windows.Forms.NumericUpDown nudSetPosY;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.NumericUpDown nudSetPosX;
        private System.Windows.Forms.Button btnSetPosition;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txb_ClampOpenWith;
        private System.Windows.Forms.Button btn_Remeber;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_SaveToFile;
        private System.Windows.Forms.TextBox tb_SaveAnglesCount;
        private System.Windows.Forms.SaveFileDialog saveAnglesFile;
        private System.Windows.Forms.Button btn_OpenFile;
        private System.Windows.Forms.OpenFileDialog openAnglesFile;
        private System.Windows.Forms.Button btn_Clear;
        private System.Windows.Forms.Button btn_back;
        private System.Windows.Forms.Button btn_StartFromFile;
        private System.Windows.Forms.ProgressBar pB_RunFromList;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lbConnectionStatus;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label lbBasisPos;
        private System.Windows.Forms.Label lbArm1Pos;
        private System.Windows.Forms.Label lbArm2Pos;
        private System.Windows.Forms.Label lbClampArmPos;
        private System.Windows.Forms.Label lbWirstPos;
        private System.Windows.Forms.Label lbClampLength;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}