﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ginbot.VisionSystem;

namespace ginbot.UI
{
    public partial class VideoViewer : Form
    {
        VisionSystem.VisionSystem visionSystem = new VisionSystem.VisionSystem();
         
        public VideoViewer()
        {
            InitializeComponent();
            visionSystem.NewFrameEvent += VisionSystem_NewFrameEvent;
        }

        private void VisionSystem_NewFrameEvent(object sender, VisionSystemNewFrameEventArgs e)
        {
            var bitmap = (Bitmap) e.NewFrame.Clone();
            if (visionSystem.calibration.IsCalibrated)
            {
                foreach (var detectedObject in e.DetectedObjects)
                {
                    var realPosition = visionSystem.calibration.GetRealPoint(detectedObject.Center);
                    var textPosition = $"{realPosition.X.ToString("N2")};{realPosition.Y.ToString("N2")}";
                    ObjectDrawer.DrawText(bitmap, textPosition, 15, detectedObject.Center);
                }
            }
            pbViewer.BackgroundImage =  bitmap;
            
        }

        private void VideoViewer_Load(object sender, EventArgs e)
        {
            
            if (visionSystem.SelectVideoDevice())
            {
                visionSystem.Start();
                visionSystem.FrameOneOf = 3;
            }
        }

        private void btnCameraProperty_Click(object sender, EventArgs e)
        {
            visionSystem.OpenPropertyWindow();
        }

        private void btn_Calibration_Click(object sender, EventArgs e)
        {
            visionSystem.StartCalibration();
        }
    }
}
